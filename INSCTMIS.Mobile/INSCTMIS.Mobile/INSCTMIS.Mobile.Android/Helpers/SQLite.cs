﻿using System;
using Xamarin.Forms;
using INSCTMIS.Mobile.Interface;
using global::SQLite;
using System.IO;

[assembly: Dependency(typeof(INSCTMIS.Mobile.Droid.Helpers.SQLite))]
namespace INSCTMIS.Mobile.Droid.Helpers
{
  


    public class SQLite : ISQLite
    {
        public SQLiteConnection GetConnection()
        {
            string sqliteFilename = "INSCTMISv2018.db3";
            string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string path = Path.Combine(documentsPath, sqliteFilename);
            return new SQLiteConnection(path);
        }
    }
}