﻿using System;
using SQLite;

namespace INSCTMIS.Mobile.Database
{
    public class ComplianceTDS
    {
        [PrimaryKey, Unique]
        public Int64 ColumnID { get; set; }
        public string RegionName { get; set; }
        public string KebeleName { get; set; }
        public string WoredaName { get; set; }
        public Int32 KebeleID { get; set; }
        public Int32 ServiceID { get; set; }
        public string ServiceName { get; set; }
        public string HouseholdType { get; set; }
    }

    public class ComplianceTDSMembers
    {
        [PrimaryKey, Unique]
        public Int64 ColumnID { get; set; }
        public Guid ProfileDSHeaderID { get; set; }
        public Guid ProfileDSDetailID { get; set; }
        public Int32 ReportingPeriodID { get; set; }
        public Int32 KebeleID { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public double Age { get; set; }
        public string Sex { get; set; }
        public string Pregnant { get; set; }
        public string Lactating { get; set; }
        public string NutritionalStatus { get; set; }
        public string EnrolledInSchool { get; set; }
        public string CBHIMembership { get; set; }
        public string ChildProtectionRisk { get; set; }
        public Int32 ServiceID { get; set; }
        public string ServiceName { get; set; }
        public string HouseholdType { get; set; }
        public string HasComplied { get; set; }
        public string Remarks { get; set; }
        public Int64 ComplianceBy { get; set; }
        public string Status { get; set; }
    }

    public class ComplianceTDSPLW
    {
        [PrimaryKey, Unique]
        public Int64 ColumnID { get; set; }
        public string RegionName { get; set; }
        public string KebeleName { get; set; }
        public string WoredaName { get; set; }
        public Int32 KebeleID { get; set; }
        public Int32 ServiceID { get; set; }
        public string ServiceName { get; set; }
        public string HouseholdType { get; set; }
    }

    public class ComplianceTDSPLWMembers
    {
        [PrimaryKey, Unique]
        public Int64 ColumnID { get; set; }
        public Int32 KebeleID { get; set; }
        public Guid ProfileTDSPLWID { get; set; }
        public Guid? ProfileTDSPLWDetailID { get; set; }
        public Int32 ReportingPeriodID { get; set; }
        public string NameOfPLW { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public Int32 PLWAge { get; set; }
        public string BabySex { get; set; }
        public string PLW { get; set; }
        public string BabyName { get; set; }
        public DateTime? BabyDateOfBirth { get; set; }
        public string NutritionalStatusInfant { get; set; }
        public string CBHIMembership { get; set; }
        public string ChildProtectionRisk { get; set; }
        public Int32 ServiceID { get; set; }
        public string ServiceName { get; set; }
        public string HouseholdType { get; set; }
        public string HasComplied { get; set; }
        public string Remarks { get; set; }
        public Int64 ComplianceBy { get; set; }
        public string Status { get; set; }
    }

    public class ComplianceTDSCMC
    {
        [PrimaryKey, Unique]
        public Int64 ColumnID { get; set; }
        public string RegionName { get; set; }
        public string KebeleName { get; set; }
        public string WoredaName { get; set; }
        public Int32 KebeleID { get; set; }
        public Int32 ServiceID { get; set; }
        public string ServiceName { get; set; }
        public string HouseholdType { get; set; }
    }

    public class ComplianceTDSCMCMembers
    {
        [PrimaryKey, Unique]
        public Int64 ColumnID { get; set; }
        public Int32 KebeleID { get; set; }
        public Guid ProfileTDSCMCID { get; set; }
        public Int32 ReportingPeriodID { get; set; }
        public string NameOfCareTaker { get; set; }
        public string MalnourishedChildName { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string MalnourishmentDegree { get; set; }
        public DateTime ChildDateOfBirth { get; set; }
        public string CBHIMembership { get; set; }
        public string ChildProtectionRisk { get; set; }
        public Int32 ServiceID { get; set; }
        public string ServiceName { get; set; }
        public string HouseholdType { get; set; }
        public string HasComplied { get; set; }
        public string Remarks { get; set; }
        public Int64 ComplianceBy { get; set; }
        public string Status { get; set; }
    }

    public class ComplianceCPHousehold 
    {
        [PrimaryKey, Unique]
        public Int64 ColumnID { get; set; }
        public string RegionName { get; set; }
        public string KebeleName { get; set; }
        public string WoredaName { get; set; }
        public Int32 KebeleID { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string NameOfHouseHoldHead { get; set; }
        public string FiscalYear { get; set; }
        public Guid ProfileDSHeaderID { get; set; }
        public string ClientType { get; set; }
    }

    public class ComplianceCPHouseholdMembers
    {
        [PrimaryKey, Unique]
        public Int64 ColumnID { get; set; }
        public Int32 KebeleID { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string HouseHoldMemberSex { get; set; }
        public Int64 HouseHoldMemberAge { get; set; }
        public Guid ProfileDSHeaderID { get; set; }
        public Guid ProfileDSDetailID { get; set; }
        public string ClientType { get; set; }
    }

    public class ComplianceCPCase
    {
        [PrimaryKey, Unique]
        public Int64 UniqueID { get; set; }
        public Guid ChildProtectionVisitHeaderId { get; set; }
        public Guid ChildProtectionHeaderID { get; set; }
        public Guid ChildProtectionDetailID { get; set; }
        public Guid ChildDetailID { get; set; }
        public int RiskId { get; set; }
        public string RiskName { get; set; }
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public int ProviderId { get; set; }
        public string ProviderName { get; set; }
        public string Status { get; set; }
    }

    public class ComplianceCPCaseHeader
    {
        [PrimaryKey]
        public Guid ChildProtectionHeaderID { get; set; }
        public string ChildProtectionId { get; set; }
        public string CaseManagementReferral { get; set; }
        public string CaseStatusId { get; set; }
        public string ClientTypeID { get; set; }
        public DateTime CollectionDate { get; set; }
        public string SocialWorker { get; set; }
        public string Remarks { get; set; }
        public string FiscalYear { get; set; }
        public Guid ChildDetailID { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Status { get; set; }
    }

    public class ComplianceCPCaseDetail
    {
        [PrimaryKey]
        public Guid ChildProtectionDetailID { get; set; }
        public Guid ChildProtectionHeaderID { get; set; }
        public string ChildName { get; set; }
        public string RiskId { get; set; }
        public string RiskName { get; set; }
        public string ServiceId { get; set; }
        public string ServiceName { get; set; }
        public string ProviderId { get; set; }
        public string ProviderName { get; set; }
        public string Status { get; set; }
    }

    public class ComplianceCPCaseVisitHeader
    {
        [PrimaryKey]
        public Guid ChildProtectionVisitHeaderId { get; set; }
        public Guid ChildProtectionHeaderID { get; set; }
        public DateTime VisitDate { get; set; }
        public string FiscalYear { get; set; }
        public string SocialWorker { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Status { get; set; }
    }

    public class ComplianceCPCaseVisitDetail
    {
        [PrimaryKey]
        public Guid ChildProtectionVisitDetailId { get; set; }
        public Guid ChildProtectionVisitHeaderId { get; set; }
        public Guid ChildProtectionDetailID { get; set; }
        public Guid ChildDetailID { get; set; }
        public string IsServiceAccessed { get; set; }
        public DateTime? DateServiceAccessed { get; set; }
        public string ServiceNotAccessedReason { get; set; }
        public string ChildName { get; set; }
        public string RiskName { get; set; }
        public string ServiceName { get; set; }
        public string ProviderName { get; set; }
        public string Status { get; set; }
    }
}
