﻿using INSCTMIS.Mobile.Interface;
using SQLite;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.Database
{
    public class DataStore
    {
        private readonly SQLiteConnection _database;
        private readonly string nameSpace = "INSCTMIS.Mobile.Database.";

        public DataStore()
        {
            _database = DependencyService.Get<ISQLite>().GetConnection();

            this._database.CreateTable<HouseholdMember>();
            this._database.CreateTable<Form1A>();
            this._database.CreateTable<Infant>();
            this._database.CreateTable<Form1B>();
            this._database.CreateTable<Form1C>();

            this._database.CreateTable<SocialWorker>();
            this._database.CreateTable<Region>();
            this._database.CreateTable<Woreda>();
            this._database.CreateTable<Kebele>();
            this._database.CreateTable<IntegratedService>();
            this._database.CreateTable<ServiceProvider>();
            this._database.CreateTable<MonitoringService>();
            this._database.CreateTable<SystemCodeDetail>();
            this._database.CreateTable<Setting>();

            this._database.CreateTable<ComplianceTDS>();
            this._database.CreateTable<ComplianceTDSMembers>();
            this._database.CreateTable<ComplianceTDSPLW>();
            this._database.CreateTable<ComplianceTDSPLWMembers>();
            this._database.CreateTable<ComplianceTDSCMC>();
            this._database.CreateTable<ComplianceTDSCMCMembers>();
            this._database.CreateTable<ComplianceCPHousehold>();
            this._database.CreateTable<ComplianceCPHouseholdMembers>();
            this._database.CreateTable<ComplianceCPCase>();
            this._database.CreateTable<ComplianceCPCaseHeader>();
            this._database.CreateTable<ComplianceCPCaseDetail>();
            this._database.CreateTable<ComplianceCPCaseVisitHeader>();
            this._database.CreateTable<ComplianceCPCaseVisitDetail>();

            this._database.CreateTable<Monitoring5A1Household>();
            this._database.CreateTable<Monitoring5A1Members>();
            this._database.CreateTable<Monitoring5A2Household>();
            this._database.CreateTable<Monitoring5A2Members>();
            this._database.CreateTable<Monitoring5BHousehold>();
            this._database.CreateTable<Monitoring5BMembers>();
            this._database.CreateTable<Monitoring5CHousehold>();
            this._database.CreateTable<Monitoring5CMembers>();
            this._database.CreateTable<Monitoring>();

            this._database.CreateTable<RetargetingHousehold>();
            this._database.CreateTable<RetargetingMembers>();
            this._database.CreateTable<Retargeting>();

        }

        public virtual void AddOrUpdate<TEntity>(TEntity entity) where TEntity : class
        {
            this._database.InsertOrReplace(entity);
        }

        public virtual void Create<TEntity>(TEntity entity) where TEntity : class
        {
            this._database.Insert(entity);
        }

        public virtual void Delete<TEntity>(TEntity entity) where TEntity : class
        {
            this._database.Delete(entity);
        }

        public virtual void Delete<TEntity>(int primarykey) where TEntity : class
        {
            this._database.Delete<TEntity>(primarykey);
        }

        public virtual void Manage<TEntity>(TEntity entity) where TEntity : class
        {
        }

        public virtual void Update<TEntity>(TEntity entity) where TEntity : class
        {
            this._database.Update(entity);
        }

        #region SystemCodeDetail

        public List<SystemCodeDetail> SystemCodeDetailGetAll()
        {
            return this._database.Query<SystemCodeDetail>(
                "SELECT * FROM [SystemCodeDetail]");
        }

        public SystemCodeDetail SystemCodeDetailGetByCode(string childCode)
        {

            var sql2 =
                $"SELECT * FROM [SystemCodeDetail] WHERE [SystemCode] = '{childCode}' ";
            Debug.WriteLine(sql2);
            return this._database.Query<SystemCodeDetail>(sql2).Single();
        }

        public SystemCodeDetail SystemCodeDetailGetById(int id)
        {
            return this._database.Query<SystemCodeDetail>($"SELECT * FROM [SystemCodeDetail] WHERE [Id] = {id}").Single();
        }

        public List<SystemCodeDetail> SystemCodeDetailsGetByCode(string code)
        {
            var systemcodetails = this._database.Query<SystemCodeDetail>($"SELECT * FROM [SystemCodeDetail] WHERE SystemCode ='{code}' ");
            return systemcodetails;
        }

        public List<Region> GetRegionDetails()
        {
            var regioncodetails = this._database.Query<Region>($"SELECT * FROM [Region]");
            return regioncodetails;
        }

        public List<Woreda> GetWoredaDetails()
        {
            var Woredacodetails = this._database.Query<Woreda>($"SELECT * FROM [Woreda]");
            return Woredacodetails;
        }

        public List<Kebele> GetKebeleDetails()
        {
            var kebelecodetails = this._database.Query<Kebele>($"SELECT * FROM [Kebele]");
            return kebelecodetails;
        }

        public List<SocialWorker> GetSocialWorkerDetails()
        {
            var socialworkercodetails = this._database.Query<SocialWorker>($"SELECT * FROM [SocialWorker]");
            return socialworkercodetails;
        }

        public List<MonitoringService> MitigationActionsByForm(string form)
        {
            var actions = this._database.Query<MonitoringService>($"SELECT * FROM [MonitoringService] WHERE FormName ='{form}' ");
            return actions;
        }

        #endregion SystemCodeDetail

        #region Abstract Get
        public List<object> GetTable(string tableName)
        {
            // tableName =  + tableName;
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM [" + tableName + "]";
            return _database.Query(map, query, obj).ToList();
        }

        public T GetTableRow<T>(string tableName, string column, string value)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + " WHERE " + column + " = '" + value + "'";
            return _database.Query(map, query, obj).Cast<T>().Single();
        }

        public object GetTableRow(string tableName, string column, string value)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + " WHERE " + column + " = '" + value + "'";
            return _database.Query(map, query, obj).Single();
        }

        public object GetTableRow(string tableName, string column1, string value1, string column2, string value2)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + " WHERE " + column1 + " = '" + value1 + "' AND " + column2 + " = '" + value2 + "'";
            return _database.Query(map, query, obj).Single();
        }

        public List<object> GetTableRows(string tableName, string column, string value)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + " WHERE " + column + " = '" + value + "'";
            return _database.Query(map, query, obj).ToList();
        }

        public List<object> GetTableRows(string tableName, string column, int value)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + " WHERE " + column + " = " + value + "";
            return _database.Query(map, query, obj).ToList();
        }

        public List<object> GetTableRows(string tableName, string column1, string value1, string column2, string value2, string column3, string value3)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + " WHERE " + column1 + " = '" + value1 + "' AND " + column2 + " = '" + value2 + "' AND " + column3 + " = '" + value3 + "'";
            return _database.Query(map, query, obj).ToList();
        }

        public List<object> GetTableRows(string tableName, string column, Guid value)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + " WHERE " + column + " = '" + value + "'";
            //string query = "SELECT * FROM " + tableName + "";
            return _database.Query(map, query, obj).ToList();
        }

        public List<T> GetTableRows<T>(string tableName, string column, string value)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + " WHERE " + column + " = '" + value + "'";
            return _database.Query(map, query, obj).Cast<T>().ToList();
        }

        public List<T> GetTableRows<T>(string tableName)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName;
            return _database.Query(map, query, obj).Cast<T>().ToList();
        }

        /* custom IRetrieve */
        public List<object> GetTableRowsPendingSyncRows(string tableName, string column1, string value1, string column2, string value2)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + " WHERE " + column1 + " = '" + value1 + "' AND HasComplied <> '' ORDER BY " + column2 + "";
            return _database.Query(map, query, obj).ToList();
        }

        public List<object> GetTableRowsPendingSyncRows(string tableName, string column1, string value1)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + " WHERE Status <> ''";
            return _database.Query(map, query, obj).ToList();
        }

        public List<T> GetISNPTableRowsPendingSyncRows<T>(string tableName)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + " WHERE Status <> 'SYNCED'";
            return _database.Query(map, query, obj).Cast<T>().ToList();
        }

        public List<T> GetRegistrationTableRowsPendingSyncRows<T>(string tableName)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            //string query = "SELECT * FROM " + tableName + " WHERE Status <> 'SYNCED'";
            string query = "SELECT * FROM " + tableName + "";
            return _database.Query(map, query, obj).Cast<T>().ToList();
        }

        public List<T> GetComplianceTableRowsPendingSyncRows<T>(string tableName)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + " WHERE HasComplied <> '' AND Status <> 'SYNCED'";
            return _database.Query(map, query, obj).Cast<T>().ToList();
        }

        public List<T> GetMonitoringTableRowsPendingSyncRows<T>(string tableName)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + " WHERE ActionResponse <> '' AND Status <> 'SYNCED'";
            return _database.Query(map, query, obj).Cast<T>().ToList();
        }

        public List<T> GetRetargetingTableRowsPendingSyncRows<T>(string tableName)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + " WHERE Status <> '' AND RecordStatus <> 'SYNCED'";
            return _database.Query(map, query, obj).Cast<T>().ToList();
        }

        public List<object> DeleteComplianceTableRowsPendingSync(string tableName)
        {
            // tableName =  + tableName;
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "DELETE FROM [" + tableName + "] WHERE HasComplied <> ''";
            return _database.Query(map, query, obj).ToList();
        }

        public List<object> DeleteMonitoringTableRowsPendingSync(string tableName)
        {
            // tableName =  + tableName;
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "DELETE FROM [" + tableName + "] WHERE HasComplied <> ''";
            return _database.Query(map, query, obj).ToList();
        }

        #endregion Abstract Get
    }
}