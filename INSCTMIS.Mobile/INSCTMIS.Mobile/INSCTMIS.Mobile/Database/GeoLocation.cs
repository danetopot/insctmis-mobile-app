﻿using System;
using SQLite;

namespace INSCTMIS.Mobile.Database
{
    public class Region
    {
        [PrimaryKey, Unique]
        public int RegionID { get; set; }
        public string RegionCode { get; set; }
        public string RegionName { get; set; }

    }

    public class Woreda
    {
        [PrimaryKey, Unique]
        public int WoredaID { get; set; }
        public int RegionID { get; set; }
        public string WoredaCode { get; set; }
        public string WoredaName { get; set; }
    }

    public class Kebele
    {
        [PrimaryKey, Unique]
        public int KebeleID { get; set; }
        public int WoredaID { get; set; }
        public string KebeleCode { get; set; }
        public string KebeleName { get; set; }
    }

}