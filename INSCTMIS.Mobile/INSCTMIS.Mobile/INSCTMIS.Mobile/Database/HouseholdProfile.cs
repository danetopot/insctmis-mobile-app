﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace INSCTMIS.Mobile.Database
{
    public class Form1A
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        [JsonIgnore]
        public int KebeleID { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
        public string HouseholdIDNumber { get; set; } //AKA PSNP HH Number
        public string NameOfHouseHoldHead { get; set; }
        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }
        public DateTime CollectionDate { get; set; }
        public string SocialWorker { get; set; }
        public string Remarks { get; set; }
        public string CCCCBSPCMember { get; set; } // AKA CCC Member
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Status { get; set; }
        [Ignore]
        public virtual List<HouseholdMember> HouseholdDetails { get; set; }
        [Ignore]
        public virtual KebeleId KebeleId { get; set; }
        [Ignore]
        public virtual string FiscalYear { get => "FY"; }

    }

    public class HouseholdMember
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int Form1AId { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string IndividualID { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int Age { get; set; }
        public string Sex { get; set; }
        public string Pregnant { get; set; }
        public string Lactating { get; set; }
        public string Handicapped { get; set; }
        public string ChronicallyIll { get; set; }
        public string NutritionalStatus { get; set; }
        public string childUnderTSForCMAM { get; set; }
        public string EnrolledInSchool { get; set; }
        public string Grade { get; set; }
        public string SchoolName { get; set; }
        public string ChildProtectionRisk { get; set; }
    }

    public class Form1B
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        [JsonIgnore]
        public int KebeleID { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
        public string PLW { get; set; }
        public string NameOfPLW { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string SocialWorker { get; set; }
        public string CCCCBSPCMember { get; set; } 
        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }
        public DateTime CollectionDate { get; set; }
        public string Remarks { get; set; }
        public string MedicalRecordNumber { get; set; }
        public int PLWAge { get; set; } 
        public DateTime StartDateTDS { get; set; } 
        public DateTime EndDateTDS { get; set; }
        public string NutritionalStatusPLW { get; set; }
        public string ChildProtectionRisk { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Status { get; set; }
        [Ignore]
        public virtual List<Infant> HouseholdDetails { get; set; }
        [Ignore]
        public virtual KebeleId KebeleId { get; set; }
        [Ignore]
        public virtual string FiscalYear { get => "FY"; }
    }

    public class Infant
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int Form1BId { get; set; }
        public DateTime BabyDateOfBirth { get; set; }
        public string BabyName { get; set; }
        public string BabySex { get; set; }
        public string NutritionalStatusInfant { get; set; }
    }

    public class Form1C
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        [JsonIgnore]
        public int KebeleID { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }
        public DateTime CollectionDate { get; set; }
        public string SocialWorker { get; set; }
        public string CCCCBSPCMember { get; set; } 
        public string NameOfCareTaker { get; set; }
        public string HouseHoldIDNumber { get; set; } 
        public string CaretakerID { get; set; }
        public string MalnourishedChildName { get; set; }
        public string ChildID { get; set; }
        public string MalnourishedChildSex { get; set; }
        public DateTime ChildDateOfBirth { get; set; }
        public int Age { get; set; }
        public string MalnourishmentDegree { get; set; }
        public DateTime StartDateTDS { get; set; }
        public DateTime NextCNStatusDate { get; set; }
        public DateTime EndDateTDS { get; set; }
        public DateTime DateTypeCertificate { get; set; }
        public string ChildProtectionRisk { get; set; }
        public string Remarks { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Status { get; set; }
        [Ignore]
        public virtual KebeleId KebeleId { get; set; }
        [Ignore]
        public virtual string FiscalYear { get => "FY"; }
    }

    public class KebeleId
    {
        public int KebeleID { get; set; }
    }

    public class Gender
    {
        public string GenderName { get; set; }
        public string GenderID { get; set; }
    }

    public class YesNo
    {
        public string Name { get; set; }
        public string ID { get; set; }
    }

    public class NutritionalStatus
    {
        public string Name { get; set; }
        public string ID { get; set; }
    }

    public class SchoolGrade
    {
        public string Name { get; set; }
        public string ID { get; set; }
    }

    public class GeneralOptions
    {
        public string Name { get; set; }
        public string ID { get; set; }
    }
}
