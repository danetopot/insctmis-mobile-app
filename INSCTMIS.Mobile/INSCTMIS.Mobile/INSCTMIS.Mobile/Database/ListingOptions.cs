﻿using System;
using SQLite;

namespace INSCTMIS.Mobile.Database
{
    public class SystemCodeDetail
    {
        [PrimaryKey, Unique]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string SystemCode { get; set; }
    }

    public class ServiceProvider
    {
        [PrimaryKey, Unique]
        public int ServiceProviderId { get; set; }
        public string ServiceProviderName { get; set; }
    }

    public class IntegratedService
    {
        [PrimaryKey, Unique]
        public int ServiceId { get; set; }
        public int ServiceProviderId { get; set; }
        public string ServiceName { get; set; }
    }

    public class MonitoringService
    {
        public int Id { get; set; }
        public string FormName { get; set; }
        public string Reason { get; set; }
        public string Action { get; set; }
    }

}