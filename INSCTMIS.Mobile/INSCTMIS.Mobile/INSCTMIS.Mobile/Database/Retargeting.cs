﻿using System;
using SQLite;

namespace INSCTMIS.Mobile.Database
{
    public class RetargetingHousehold
    {
        [PrimaryKey, Unique]
        public string RetargetingHeaderID { get; set; }
        public Int64 ColumnID { get; set; }
        public string FormID { get; set; }
        public int KebeleID { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
        public string NameOfHouseholdHead { get; set; }
        public string HouseholdIDNumber { get; set; }
        public int ClientCategory { get; set; }
        public int FiscalYear { get; set; }
        public int Members { get; set; }
        public bool ApprovalStatus { get; set; }
    }

    public class RetargetingMembers
    {
        [PrimaryKey, Unique]
        public string RetargetingDetailID { get; set; }
        public string RetargetingHeaderID { get; set; }
        public Int64 ColumnID { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string IndividualID { get; set; }
        public DateTime DateOfBirth { get; set; }
        public double Age { get; set; }
        public string Sex { get; set; }
        public int ClientCategory { get; set; }
        public string StartDateTDS { get; set; }
        public DateTime? NextCNStatusDate { get; set; }
        public string EndDateTDS { get; set; }
        public string DateTypeCertificate { get; set; }
        public string CaretakerID { get; set; }
        public string ChildID { get; set; }
        public string CCCMember { get; set; }
        public string Status { get; set; }
    }

    public class Retargeting
    {
        [PrimaryKey, Unique]
        public string RetargetingDetailID { get; set; }
        public string RetargetingHeaderID { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string CCCMember { get; set; }
        public string Status { get; set; }
        public string StatusId { get; set; }
        public string Pregnant { get; set; }
        public string Lactating { get; set; }
        public string Handicapped { get; set; }
        public string ChronicallyIll { get; set; }
        public string NutritionalStatus { get; set; }
        public string ChildUnderTSForCMAM { get; set; }
        public string EnrolledInSchool { get; set; }
        public string SchoolName { get; set; }
        public string Grade { get; set; }
        public string SocialWorker { get; set; }
        public DateTime CreatedOn { get; set; }
        public Int64 RetargetingBy { get; set; }
        public string RecordStatus { get; set; }
    }
}
