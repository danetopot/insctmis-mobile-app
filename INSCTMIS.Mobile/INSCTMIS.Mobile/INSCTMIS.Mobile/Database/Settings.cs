﻿using System;
using SQLite;

namespace INSCTMIS.Mobile.Database
{
    class Setting
    {
        [PrimaryKey]
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
