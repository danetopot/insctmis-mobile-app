﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
namespace INSCTMIS.Mobile.Helpers
{
    public class Settings
    {
        private const string AccessTokenExpirationDateKey = "token_expiry_key";
        private const string AccessTokenIssuedDateKey = "token_issued_key";
        private const string AccessTokenTypeKey = "token_type_key";
        private const string AccessTokenKey = "token_key";

        private const string KebeleIdKey = "kebele_id_key";
        private const string KebeleNameKey = "kebele_name_key";

        private const string ReportingPeriodIdKey = "reporting_period_id_key";
        private const string ReportingPeriodNameKey = "reporting_period_name_key";

        private const string FiscalYearIdKey = "fiscal_year_id_key";
        private const string FiscalYearNameKey = "fiscal_year_name_key";

        private const string UserIdKey = "user_id_key";

        private const string UserRoleIdKey = "user_roleid_key";

        private const string EnumeratorIdKey = "enumerator_id_key";

        private const string AttemptedPushKey = "attempted_push";

        private const string DatabaseIdKey = "azure_database";

        private const string EmailKey = "email_key";

        private const string FavoriteModeKey = "favorites_only";

        private const string UserNameKey = "firstname_key";

        private const string FirstRunKey = "first_run";

        private const string GooglePlayCheckedKey = "play_checked";

        private const string HasSetReminderKey = "set_a_reminder";

        private const bool HasSyncedDataDefault = false;

        private const string HasSyncedDataKey = "has_synced_down";
        private const string HasSyncedDataUpwardsKey = "has_synced_upwards";
        private const string HasSyncedDataDownwardsKey = "has_synced_downwards";

        private const string LastFavoriteTimeKey = "last_favorite_time";

        private const string LastNameKey = "lastname_key";

        private const string LastSyncKey = "last_sync";
        private const string LastSyncDownKey = "last_sync_down";
        private const string LastSyncUpKey = "last_sync_up";

        private const string LoggedInKey = "logged_in";

        private const int LoginAttemptsDefault = 0;

        private const string LoginAttemptsKey = "login_attempts";

        private const bool NeedsSyncDefault = true;

        private const string NeedsSyncKey = "needs_sync";

        private const string PasswordKey = "password_key";

        private const string PositionKey = "position_key";

        private const string PushNotificationsEnabledKey = "push_enabled";

        private const string PushRegisteredKey = "push_registered";

        private static readonly bool AttemptedPushDefault = false;

        private static readonly int DatabaseIdDefault = 0;
        private static readonly int EnumeratorIdDefault = 0;

        private static readonly bool FavoriteModeDefault = false;

        private static readonly bool FirstRunDefault = true;

        private static readonly bool GooglePlayCheckedDefault = false;

        private static readonly bool HasSetReminderDefault = false;

        private static readonly DateTime LastSyncDefault = DateTime.Now.AddDays(-30);

        private static readonly bool LoggedInDefault = true;

        private static readonly bool PushNotificationsEnabledDefault = false;

        private static readonly bool PushRegisteredDefault = false;

        private static Settings settings;

        private readonly string emailDefault = "demo@system.com";
        private readonly string usernameDefault = "Admin";
        private readonly string passwordDefault = "";

        private readonly string firstNameDefault = string.Empty;

        private readonly string lastNameDefault = string.Empty;

        private readonly string positionDefault = string.Empty;

        private readonly string tokenDefault = string.Empty;

        private bool isConnected;

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the current settings. This should always be used
        /// </summary>
        /// <value>The current.</value>
        public static Settings Current
        {
            get
            {
                return settings ?? (settings = new Settings());
            }
        }

        public string KebeleId
        {
            get
            {
                return AppSettings.GetValueOrDefault(KebeleIdKey, KebeleId);
            }

            set
            {
                AppSettings.AddOrUpdateValue(KebeleIdKey, value);
            }
        }
        public string KebeleName
        {
            get
            {
                return AppSettings.GetValueOrDefault(KebeleNameKey, KebeleName);
            }

            set
            {
                AppSettings.AddOrUpdateValue(KebeleIdKey, value);
            }
        }

        public string ReportingId
        {
            get
            {
                return AppSettings.GetValueOrDefault(ReportingPeriodIdKey, ReportingId);
            }

            set
            {
                AppSettings.AddOrUpdateValue(ReportingPeriodIdKey, value);
            }
        }
        public string ReportingName
        {
            get
            {
                return AppSettings.GetValueOrDefault(ReportingPeriodNameKey, ReportingName);
            }

            set
            {
                AppSettings.AddOrUpdateValue(ReportingPeriodIdKey, value);
            }
        }

        public string FiscalYearId
        {
            get
            {
                return AppSettings.GetValueOrDefault(FiscalYearIdKey, FiscalYearId);
            }

            set
            {
                AppSettings.AddOrUpdateValue(FiscalYearIdKey, value);
            }
        }
        public string FiscalYearName
        {
            get
            {
                return AppSettings.GetValueOrDefault(FiscalYearNameKey, FiscalYearName);
            }

            set
            {
                AppSettings.AddOrUpdateValue(FiscalYearIdKey, value);
            }
        }

        public static int DatabaseId
        {
            get
            {
                return AppSettings.GetValueOrDefault(DatabaseIdKey, DatabaseIdDefault);
            }

            set
            {
                AppSettings.AddOrUpdateValue(DatabaseIdKey, value);
            }
        }

        public int EnumeratorId
        {
            get
            {
                return AppSettings.GetValueOrDefault(EnumeratorIdKey, EnumeratorIdDefault);
            }

            set
            {
                AppSettings.AddOrUpdateValue(EnumeratorIdKey, value);
            }
        }

        public Int64 UserId
        {
            get
            {
                return AppSettings.GetValueOrDefault(UserIdKey, UserId);
            }

            set
            {
                AppSettings.AddOrUpdateValue(UserIdKey, value);
            }
        }

        public int UserRoleId
        {
            get
            {
                return AppSettings.GetValueOrDefault(UserRoleIdKey, UserRoleId);
            }

            set
            {
                AppSettings.AddOrUpdateValue(UserRoleIdKey, value);
            }
        }

        public DateTime AccessTokenIssuedDate
        {
            get => AppSettings.GetValueOrDefault(AccessTokenIssuedDateKey, DateTime.Now);
            set
            {
                if (AppSettings.AddOrUpdateValue(AccessTokenIssuedDateKey, value))
                {
                    OnPropertyChanged();
                }
            }
        }

        public DateTime AccessTokenExpirationDate
        {
            get => AppSettings.GetValueOrDefault(AccessTokenExpirationDateKey, DateTime.Now);
            set
            {
                if (AppSettings.AddOrUpdateValue(AccessTokenExpirationDateKey, value))
                {
                    OnPropertyChanged();
                }
            }
        }

        public string AccessToken
        {
            get => AppSettings.GetValueOrDefault(AccessTokenKey, this.tokenDefault);

            set
            {
                if (AppSettings.AddOrUpdateValue(AccessTokenKey, value))
                {
                    OnPropertyChanged();
                }
            }
        }

        public string AccessTokenType
        {
            get => AppSettings.GetValueOrDefault(AccessTokenTypeKey, this.tokenDefault);

            set
            {
                if (AppSettings.AddOrUpdateValue(AccessTokenTypeKey, value))
                {
                    OnPropertyChanged();
                }
            }
        }

        public bool AttemptedPush
        {
            get
            {
                return AppSettings.GetValueOrDefault(AttemptedPushKey, AttemptedPushDefault);
            }

            set
            {
                AppSettings.AddOrUpdateValue(AttemptedPushKey, value);
            }
        }

        public string Email
        {
            get
            {
                return AppSettings.GetValueOrDefault(EmailKey, this.emailDefault);
            }

            set
            {
                if (AppSettings.AddOrUpdateValue(EmailKey, this.emailDefault))
                {
                }
            }
        }

        
        public string UserName
        {
            get
            {
                return AppSettings.GetValueOrDefault(UserNameKey, this.usernameDefault);
            }

            set
            {
                if (AppSettings.AddOrUpdateValue(UserNameKey, value))
                {
                    OnPropertyChanged();
                    OnPropertyChanged(nameof(UserName));
                }
            }
        }



        /// <summary>
        /// Gets or sets a value indicating whether the user wants to see favorites only.
        /// </summary>
        /// <value><c>true</c> if favorites only; otherwise, <c>false</c>.</value>
        public bool FirstRun
        {
            get
            {
                return AppSettings.GetValueOrDefault(FirstRunKey, FirstRunDefault);
            }

            set
            {
                if (AppSettings.AddOrUpdateValue(FirstRunKey, value))
                    OnPropertyChanged();
            }
        }

        public bool GooglePlayChecked
        {
            get
            {
                return AppSettings.GetValueOrDefault(GooglePlayCheckedKey, GooglePlayCheckedDefault);
            }

            set
            {
                AppSettings.AddOrUpdateValue(GooglePlayCheckedKey, value);
            }
        }

        public bool HasSetReminder
        {
            get
            {
                return AppSettings.GetValueOrDefault(HasSetReminderKey, HasSetReminderDefault);
            }

            set
            {
                AppSettings.AddOrUpdateValue(HasSetReminderKey, value);
            }
        }

        public bool HasSyncedDataDownwards
        {
            get
            {
                return AppSettings.GetValueOrDefault(HasSyncedDataDownwardsKey, HasSyncedDataDefault);
            }

            set
            {
                AppSettings.AddOrUpdateValue(HasSyncedDataDownwardsKey, value);
            }
        }

        public bool HasSyncedDataUpwards
        {
            get
            {
                return AppSettings.GetValueOrDefault(HasSyncedDataUpwardsKey, HasSyncedDataDefault);
            }

            set
            {
                AppSettings.AddOrUpdateValue(HasSyncedDataUpwardsKey, value);
            }
        }

        public bool IsConnected
        {
            get
            {
                return isConnected;
            }

            set
            {
                if (isConnected == value)
                    return;
                isConnected = value;
                OnPropertyChanged();
            }
        }

        public bool IsLoggedIn => !string.IsNullOrWhiteSpace(Email);

        public DateTime LastFavoriteTime
        {
            get => AppSettings.GetValueOrDefault(LastFavoriteTimeKey, DateTime.UtcNow);
            set
            {
                AppSettings.AddOrUpdateValue(LastFavoriteTimeKey, value);
            }
        }

        public string LastName
        {
            get
            {
                return AppSettings.GetValueOrDefault(LastNameKey, this.lastNameDefault);
            }

            set
            {
                if (AppSettings.AddOrUpdateValue(LastNameKey, value))
                {
                    OnPropertyChanged();
                    OnPropertyChanged(nameof(UserName));
                }
            }
        }

        public DateTime LastSyncDown
        {
            get
            {
                return AppSettings.GetValueOrDefault(LastSyncDownKey, LastSyncDefault);
            }

            set
            {
                if (AppSettings.AddOrUpdateValue(LastSyncDownKey, value))
                    OnPropertyChanged();
            }
        }

        public DateTime LastSync
        {
            get
            {
                return AppSettings.GetValueOrDefault(LastSyncKey, LastSyncDefault);
            }

            set
            {
                if (AppSettings.AddOrUpdateValue(LastSyncKey, value))
                    OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user wants to see favorites only.
        /// </summary>
        /// <value><c>true</c> if favorites only; otherwise, <c>false</c>.</value>
        public bool LoggedIn
        {
            get
            {
                return AppSettings.GetValueOrDefault(LoggedInKey, LoggedInDefault);
            }

            set
            {
                if (AppSettings.AddOrUpdateValue(LoggedInKey, value))
                    OnPropertyChanged();
            }
        }

        public int LoginAttempts
        {
            get
            {
                return AppSettings.GetValueOrDefault(LoginAttemptsKey, LoginAttemptsDefault);
            }

            set
            {
                AppSettings.AddOrUpdateValue(LoginAttemptsKey, value);
            }
        }

        public bool NeedsSync
        {
            get
            {
                return AppSettings.GetValueOrDefault(NeedsSyncKey, NeedsSyncDefault)
                       || LastSync < DateTime.Now.AddDays(-1);
            }

            set
            {
                AppSettings.AddOrUpdateValue(NeedsSyncKey, value);
            }
        }

        public string Password
        {
            get
            {
                return AppSettings.GetValueOrDefault(PasswordKey, this.passwordDefault);
            }

            set
            {
                if (AppSettings.AddOrUpdateValue(PasswordKey, value))
                {
                    // OnPropertyChanged();
                    // OnPropertyChanged(nameof(UserAvatar));
                }
            }
        }

        public string Position
        {
            get
            {
                return AppSettings.GetValueOrDefault(PositionKey, this.positionDefault);
            }

            set
            {
                if (AppSettings.AddOrUpdateValue(PositionKey, value))
                {
                    OnPropertyChanged();
                }
            }
        }

        public bool PushNotificationsEnabled
        {
            get
            {
                return AppSettings.GetValueOrDefault(PushNotificationsEnabledKey, PushNotificationsEnabledDefault);
            }

            set
            {
                if (AppSettings.AddOrUpdateValue(PushNotificationsEnabledKey, value))
                    OnPropertyChanged();
            }
        }

        public bool PushRegistered
        {
            get
            {
                return AppSettings.GetValueOrDefault(PushRegisteredKey, PushRegisteredDefault);
            }

            set
            {
                AppSettings.AddOrUpdateValue(PushRegisteredKey, value);
            }
        }

        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        public static int UpdateDatabaseId()
        {
            return DatabaseId++;
        }

        public void FinishHack(string id)
        {
            AppSettings.AddOrUpdateValue("minihack_" + id, true);
        }

        public bool IsHackFinished(string id)
        {
            return AppSettings.GetValueOrDefault("minihack_" + id, false);
        }

        private void OnPropertyChanged([CallerMemberName] string name = "") =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
    }
}
