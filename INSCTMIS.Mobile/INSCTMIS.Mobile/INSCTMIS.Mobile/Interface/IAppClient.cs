﻿using System.Collections.Generic;
using System.Threading.Tasks;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Models;

namespace INSCTMIS.Mobile.Interface
{
    public interface IAppClient
    {
        Task<AccountResponse> ForgotPasswordAsync(string username);

        Task<AccountResponse> LoginAsync(string username, string password);

        Task<AccountResponse> LoginSocialWorker(string username, string password);
         
        Task<ListingOptionsResponse> GetListingSettings(AccountResponse accountResponse);

        Task<ApiStatus> ForgotPin(string nationalIdNo, string emailAddress, string id);

        Task<ApiStatus> ChangePin(string currentPin, string newPin, string id);

        Task LogoutAsync();

        Task<ComplianceResponse> GetHouseholdsReadyForCompliance(QueryParameter queryParameter);

        Task<ISNPResponse> GetHouseholdsReadyForChildProtection(QueryParameter queryParameter);

        Task<MonitoringResponse> GetHouseholdsReadyForMonitoring(QueryParameter queryParameter);

        Task<RetargetingResponse> GetHouseholdsReadyForRetargeting(QueryParameter2 queryParameter);

        Task<ApiStatus> PostHouseholdsReadyForCompliance(ComplianceCaptureVm compliance);

        Task<ApiStatus> PostHouseholdsReadyForMonitoring(MonitoringCaptureVm monitoring);

        Task<ApiStatus> PostHouseholdsReadyForRetargeting(RetargetingCaptureVm retargeting);

        Task<ApiStatus> PostChildProtectionHousehold(ISNPCaseVm isnp);

        Task<ApiStatus> PostHouseholdProfiles(HouseholdProfileVm householdprofile);
    }
}