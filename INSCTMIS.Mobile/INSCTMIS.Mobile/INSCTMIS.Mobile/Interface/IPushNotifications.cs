﻿using System.Threading.Tasks;

namespace INSCTMIS.Mobile.Interface
{
    public interface IPushNotifications
    {
        bool IsRegistered { get; }

        void OpenSettings();

        Task<bool> RegisterForNotifications();
    }
}