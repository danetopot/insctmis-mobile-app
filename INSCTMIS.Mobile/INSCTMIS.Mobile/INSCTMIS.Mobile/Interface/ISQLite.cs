﻿using SQLite;

namespace INSCTMIS.Mobile.Interface
{

    public interface ISQLite
    {
        SQLiteConnection GetConnection();
    }
}
