﻿using INSCTMIS.Mobile.ViewModels;

namespace INSCTMIS.Mobile.Interface
{
    public interface IToast
    {
        void SendToast(string message);
        object MakeText(SyncViewModel syncViewModel, string v, object @long);
    }

}
