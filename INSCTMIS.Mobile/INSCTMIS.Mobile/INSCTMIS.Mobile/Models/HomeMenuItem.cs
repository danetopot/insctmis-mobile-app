﻿namespace INSCTMIS.Mobile.Models
{
    public enum MenuItemType
    {
        HomePage,
        HouseholdProfilePage,
        CompliancePage,
        MonitoringPage,
        RetargetingPage,
        SyncPage,
        SettingsPage,
        LogoutPage
    }
    public class HomeMenuItem
    {
        public MenuItemType Id { get; set; }

        public string Title { get; set; }
    }
}
