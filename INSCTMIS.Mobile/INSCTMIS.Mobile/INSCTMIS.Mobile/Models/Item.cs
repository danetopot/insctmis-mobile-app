﻿using INSCTMIS.Mobile.Database;
using Plugin.DeviceInfo.Abstractions;
using System;
using System.Collections.Generic;

namespace INSCTMIS.Mobile.Models
{
    public class Item
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
    }

    public class ApiStatus
    {
        public string Description { get; set; }
        public int? StatusId { get; set; }
        public int? Id { get; set; }
    }

    public class UserDevice
    {
        public Guid DataId { get; set; }
        public string ModuleName { get; set; }
        public string AppVersion { get; set; }
        public string AppBuild { get; set; }
        public string DeviceId { get; set; }
        public string DeviceModel { get; set; }
        public string DeviceManufacturer { get; set; }
        public string DeviceName { get; set; }
        public string Version { get; set; }
        public string VersionNumber { get; set; }
        public bool IsDevice { get; set; }
    }

    public class AuthKeyResponse
    {
        public int Success { get; set; }
    }

    public class AccountResponse
    {
        public Int64 UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Message { get; set; }
        public string IsAuthenticated { get; set; }
    }

    public class ListingOptionsResponse : SocialWorkerLoginResponse
    {
        public SocialWorker SocialWorker { get; set; }
        public List<Region> Region { get; set; }
        public List<Woreda> Woreda { get; set; }
        public List<Kebele> Kebele { get; set; }
        public List<ServiceProvider> ServiceProvider { get; set; }
        public List<IntegratedService> IntegratedService { get; set; }
        public List<MonitoringService> MonitoringService { get; set; }
        public List<SystemCodeDetail> SystemCodeDetail { get; set; }
    }

    public class SocialWorkerLoginResponse
    {
        public string Error { get; set; }
    }

    public class ComplianceVm
    {
        public List<ComplianceCaptureVm> Compliance { get; set; }
    }

    public class ComplianceCaptureVm
    {
        public int KebeleID { get; set; }
        public int ReportingPeriodID { get; set; }
        public Guid IndividualProfileID { get; set; }
        public int ServiceID { get; set; }
        public string HouseholdType { get; set; }
        public string HasComplied { get; set; }
        public string Remarks { get; set; }
        public int CreatedById { get; set; }
        public UserDevice UserDevice { get; set; }
    }

    public class ComplianceResponse
    {
        public List<ComplianceTDS> TDSHouseholds { get; set; }
        public List<ComplianceTDSMembers> TDSHouseholdMembers { get; set; }
        public List<ComplianceTDSPLW> TDSPLWHouseholds { get; set; }
        public List<ComplianceTDSPLWMembers> TDSPLWHouseholdMembers { get; set; }
        public List<ComplianceTDSCMC> TDSCMCHouseholds { get; set; }
        public List<ComplianceTDSCMCMembers> TDSCMCHouseholdMembers { get; set; }

    }

    public class MonitoringVm
    {
        public List<MonitoringCaptureVm> Monitoring { get; set; }
    }

    public class MonitoringCaptureVm
    {
        public Guid IndividualProfileID { get; set; }
        public int KebeleID { get; set; }
        public int ReportingPeriodID { get; set; }
        public DateTime CompletedDate { get; set; }
        public int NonComplianceReason { get; set; }
        public int ActionResponse { get; set; }
        public string HouseholdType { get; set; }
        public string SocialWorker { get; set; }
        public Int64 CreatedById { get; set; }
        public UserDevice UserDevice { get; set; }
    }


    public class MonitoringResponse
    {
        public List<Monitoring5A1Household> TDS1Households { get; set; }
        public List<Monitoring5A1Members> TDS1HouseholdMembers { get; set; }
        public List<Monitoring5A2Household> TDS2Households { get; set; }
        public List<Monitoring5A2Members> TDS2HouseholdMembers { get; set; }
        public List<Monitoring5BHousehold> PLWHouseholds { get; set; }
        public List<Monitoring5BMembers> PLWHouseholdMembers { get; set; }
        public List<Monitoring5CHousehold> CMCHouseholds { get; set; }
        public List<Monitoring5CMembers> CMCHouseholdMembers { get; set; }

    }

    public class HouseholdProfileVm
    {
        public HouseholdProfileCapturePDSVm PDSHousehold { get; set; }
        public List<HouseholdProfileCapturePDSMembersVm> PDSHouseholdMembers { get; set; }
        public HouseholdProfileCapturePLWVm PLWHousehold { get; set; }
        public List<HouseholdProfileCapturePLWMembersVm> PLWHouseholdMembers { get; set; }
        public HouseholdProfileCaptureCMCVm CMCHousehold { get; set; }

    }

    public class HouseholdProfileCapturePDSVm
    {
        public Guid ProfileDSHeaderID { get; set; }
        public Guid ProfileDSDetailID { get; set; }
        public string HouseHold { get; set; }
        public int KebeleID { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
        public string NameOfHouseHoldHead { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string Remarks { get; set; }
        public DateTime CollectionDate { get; set; }
        public string SocialWorker { get; set; }
        public string CCCCBSPCMember { get; set; }
        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public UserDevice UserDevice { get; set; }
    }

    public class HouseholdProfileCapturePDSMembersVm
    {
        public Guid ProfileDSHeaderID { get; set; }
        public Guid ProfileDSDetailID { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string IndividualID { get; set; }
        public string Pregnant { get; set; }
        public string Lactating { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int Age { get; set; }
        public string Sex { get; set; }
        public string PWL { get; set; }
        public string Handicapped { get; set; }
        public string ChronicallyIll { get; set; }
        public string NutritionalStatus { get; set; }
        public string ChildUnderTSForCMAM { get; set; }
        public string EnrolledInSchool { get; set; }
        public string Grade { get; set; }
        public string SchoolName { get; set; }
        public string ChildProtectionRisk { get; set; }
    }

    public class HouseholdProfileCapturePLWVm
    {
        public Guid ProfileDSHeaderID { get; set; }
        public Guid ProfileDSDetailID { get; set; }
        public string HouseHold { get; set; }
        public int KebeleID { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
        public DateTime CollectionDate { get; set; }
        public string SocialWorker { get; set; }
        public string CCCCBSPCMember { get; set; }
        public string PLW { get; set; }
        public string NameOfPLW { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string MedicalRecordNumber { get; set; }
        public int PLWAge { get; set; }
        public string NutritionalStatusPLW { get; set; }
        public DateTime StartDateTDS { get; set; }
        public DateTime EndDateTDS { get; set; }
        public string Remarks { get; set; }
        public string ChildProtectionRisk { get; set; }
        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public UserDevice UserDevice { get; set; }
    }

    public class HouseholdProfileCapturePLWMembersVm
    {
        public Guid ProfileDSHeaderID { get; set; }
        public Guid ProfileDSDetailID { get; set; }
        public DateTime BabyDateOfBirth { get; set; }
        public string BabyName { get; set; }
        public string BabySex { get; set; }
        public string NutritionalStatusInfant { get; set; }
    }

    public class HouseholdProfileCaptureCMCVm
    {
        public Guid ProfileDSHeaderID { get; set; }
        public string HouseHold { get; set; }
        public int KebeleID { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
        public DateTime CollectionDate { get; set; }
        public string SocialWorker { get; set; }
        public string CCCCBSPCMember { get; set; }
        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }
        public string CreatedBy { get; set; }
        public string NameOfCareTaker { get; set; }
        public string CaretakerID { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string ChildID { get; set; }
        public string MalnourishedChildName { get; set; }
        public string MalnourishedChildSex { get; set; }
        public DateTime ChildDateOfBirth { get; set; }
        public DateTime DateTypeCertificate { get; set; }
        public string MalnourishmentDegree { get; set; }
        public DateTime StartDateTDS { get; set; }
        public DateTime NextCNStatusDate { get; set; }
        public DateTime EndDateTDS { get; set; }
        public string Remarks { get; set; }
        public string ChildProtectionRisk { get; set; }
        public DateTime CreatedOn { get; set; }
        public UserDevice UserDevice { get; set; }
    }

    public class RetargetingVm
    {
        public List<RetargetingCaptureVm> Retargeting { get; set; }
    }

    public class RetargetingCaptureVm
    {
        public string RetargetingDetailID { get; set; }
        public string RetargetingHeaderID { get; set; }
        public string CCCMember { get; set; }
        public string Status { get; set; }
        public string Pregnant { get; set; }
        public string Lactating { get; set; }
        public string Handicapped { get; set; }
        public string ChronicallyIll { get; set; }
        public string NutritionalStatus { get; set; }
        public string ChildUnderTSForCMAM { get; set; }
        public string EnrolledInSchool { get; set; }
        public string SchoolName { get; set; }
        public string Grade { get; set; }
        public string SocialWorker { get; set; }
        public DateTime DateUpdated { get; set; }
        public bool IsUpdated { get; set; }
        public Int64 CreatedBy { get; set; }
        public UserDevice UserDevice { get; set; }
    }

    public class RetargetingResponse
    {
        public List<RetargetingHousehold> RetargetingHouseholds { get; set; }
        public List<RetargetingMembers> RetargetingHouseholdMembers { get; set; }
    }

    public class ISNPResponse
    {
        public List<ComplianceCPHousehold> CPHouseholds { get; set; }
        public List<ComplianceCPHouseholdMembers> CPHouseholdMembers { get; set; }
        public List<ComplianceCPCase> CPCases { get; set; }
    }

    public class ISNPCaseVm
    {
        public ComplianceCPCaseHeaderCaptureVm CPCaseHeaderInfo { get; set; }
        public List<ComplianceCPCaseDetailCaptureVm> CPCaseDetailInfo { get; set; }
        public ComplianceCPCaseVisitHeaderCaptureVm CPCaseVisitHeaderInfo { get; set; }
        public List<ComplianceCPCaseVisitDetailCaptureVm> CPCaseVisitDetailInfo { get; set; }
        public UserDevice UserDevice { get; set; }
    }

    public class ComplianceCPCaseHeaderCaptureVm
    {
        public Guid ChildProtectionHeaderID { get; set; }
        public string ChildProtectionId { get; set; }
        public string CaseManagementReferral { get; set; }
        public string CaseStatusId { get; set; }
        public string ClientTypeID { get; set; }
        public DateTime CollectionDate { get; set; }
        public string SocialWorker { get; set; }
        public string Remarks { get; set; }
        public string FiscalYear { get; set; }
        public Guid ChildDetailID { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
    }

    public class ComplianceCPCaseDetailCaptureVm
    {
        public Guid ChildProtectionDetailID { get; set; }
        public Guid ChildProtectionHeaderID { get; set; }
        public string RiskId { get; set; }
        public string ServiceId { get; set; }
        public string ProviderId { get; set; }
    }

    public class ComplianceCPCaseVisitHeaderCaptureVm
    {
        public Guid ChildProtectionVisitHeaderId { get; set; }
        public Guid ChildProtectionHeaderID { get; set; }
        public DateTime VisitDate { get; set; }
        public string FiscalYear { get; set; }
        public string SocialWorker { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
    }

    public class ComplianceCPCaseVisitDetailCaptureVm
    {
        public Guid ChildProtectionVisitHeaderId { get; set; }
        public Guid ChildProtectionDetailID { get; set; }
        public string ServiceStatusID { get; set; }
        public DateTime? ServiceAccessDate { get; set; }
        public string ServiceNotAccessedReasonID { get; set; }
    }

    public class SelectableItemWrapper<T>
    {
        public bool IsSelected { get; set; }
        public T Item { get; set; }
    }

    public class QueryParameter
    {
        public string KebeleID { get; set; }
        public string ReportingPeriodID { get; set; }
    }

    public class QueryParameter2
    {
        public string KebeleID { get; set; }
        public string FiscalYear { get; set; }
    }
}