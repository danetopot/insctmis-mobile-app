﻿using INSCTMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INSCTMIS.Mobile.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ComplianceCaptureForm4ABCPage : ContentPage
	{
        public ComplianceCaptureForm4ABCPage(int kebeleid, int serviceid, string household, string id)
        {
            InitializeComponent();
            BindingContext = new ComplianceCaptureForm4ABCViewModel(Navigation, kebeleid, serviceid, household, id);
        }
    }
}