﻿using INSCTMIS.Mobile.Converters;
using INSCTMIS.Mobile.ViewModels;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INSCTMIS.Mobile.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ComplianceCaptureForm4DFollowUpPage : TabbedPage
	{

        public ComplianceCaptureForm4DFollowUpPage(string headerid, string isnpdataid, string childid, string risk, string service, string provider)
        {
            InitializeComponent();
            BindingContext = new ComplianceCaptureForm4DFollowUpViewModel(Navigation, headerid, isnpdataid, childid, risk, service, provider);

            DateServiceAccessed.MinimumDate = Convert.ToDateTime("01/01/1900");
        }
    }
}