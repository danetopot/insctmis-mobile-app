﻿using System;
using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INSCTMIS.Mobile.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ComplianceCaptureForm4DPage : TabbedPage
    {
        private ComplianceCaptureForm4DViewModel vm;
        private int KebeleId;
        private int ServiceId;
        private string Household;

        ContentPage contentISNPCaseFollowUpsReadyForSyncContentPage;
        TabbedPage tabbedPage;
        Button addNewButton;

        public ComplianceCaptureForm4DPage (string id, bool edit)
		{
			InitializeComponent ();
            BindingContext = vm = new ComplianceCaptureForm4DViewModel(Navigation, id);

            ServiceProvided.SelectedIndex = 0;
            ServiceProvider.SelectedIndex = 0;
            Risk.SelectedIndex = 0;

            addNewButton = this.AddNewButton;
            addNewButton.Clicked += async (object sender, EventArgs e) =>
            {
                await vm.ExecuteSaveRiskCommand();

                if(!vm.Error)
                {
                    this.Risk.SelectedIndex = 0;
                    this.ServiceProvided.SelectedIndex = 0;
                    this.ServiceProvider.SelectedIndex = 0;
                }

                vm.PendingISNPRisksReadyForSync.Clear();
                vm.PendingISNPRisksReadyForSync.AddRange(vm.GetPendingISNPRisksReadyForSync(vm.ChildProtectionHeaderId.ToString()));
            };

            #region ManageTabs
            /*
            tabbedPage = this.ISNPDataListTab;
            contentISNPCaseFollowUpsReadyForSyncContentPage = this.ISNPCaseFollowUpsReadyForSyncContentPage;
            tabbedPage.Children.Remove(contentISNPCaseFollowUpsReadyForSyncContentPage);
            tabbedPage.Children.Add(contentISNPCaseFollowUpsReadyForSyncContentPage);

            vm.PendingISNPCasesFollowUpReadyForSync.Clear();
            vm.PendingISNPCasesFollowUpReadyForSync.AddRange(vm.GetPendingISNPCasesFollowUpReadyForSync(vm.ChildDetailID.ToString()));
            */
            #endregion
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            //tabbedPage.Children.Remove(contentISNPCaseFollowUpsReadyForSyncContentPage);
            //tabbedPage.Children.Add(contentISNPCaseFollowUpsReadyForSyncContentPage);
            //vm.PendingISNPCasesFollowUpReadyForSync.Clear();
            //vm.PendingISNPCasesFollowUpReadyForSync.AddRange(vm.GetPendingISNPCasesFollowUpReadyForSync(vm.ChildDetailID.ToString()));
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        
    }
}