﻿using INSCTMIS.Mobile.ViewModels;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace INSCTMIS.Mobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ComplianceHouseholdMembersListPage : TabbedPage
    {
        private ComplianceHouseholdMembersListViewModel vm;
        private int KebeleId;
        private int ServiceId;
        private string Household;
        private string Id;

        ContentPage contentTDSCompliancePage, contentPLWCompliancePage, contentCMCCompliancePage, contentISNPCompliancePage, contentTDSPendingPage, contentPLWPendingPage, contentCMCPendingPage, contentISNPPendingPage;
        TabbedPage tabbedPage;

        public ComplianceHouseholdMembersListPage(int kebeleid, int serviceid, string household, string id)
        {
            InitializeComponent();
            BindingContext = vm = new ComplianceHouseholdMembersListViewModel(Navigation, kebeleid, serviceid, household, id);
            KebeleId = kebeleid;
            ServiceId = serviceid;
            Household = household;
            Id = id;

            #region ManageTabs
            tabbedPage = this.HouseholdMembersListTab;
            contentTDSCompliancePage = this.TDSComplianceMembersContentPage;
            contentPLWCompliancePage = this.PLWComplianceMembersContentPage;
            contentCMCCompliancePage = this.CMCComplianceMembersContentPage;
            contentISNPCompliancePage = this.ISNPComplianceMembersContentPage;
            contentTDSPendingPage = this.TDSPendingSyncComplianceMembersContentPage;
            contentPLWPendingPage = this.PLWPendingSyncComplianceMembersContentPage;
            contentCMCPendingPage = this.CMCPendingSyncComplianceMembersContentPage;
            contentISNPPendingPage = this.ISNPPendingSyncComplianceMembersContentPage;
            tabbedPage.Children.Remove(contentTDSCompliancePage);
            tabbedPage.Children.Remove(contentPLWCompliancePage);
            tabbedPage.Children.Remove(contentCMCCompliancePage);
            tabbedPage.Children.Remove(contentISNPCompliancePage);
            tabbedPage.Children.Remove(contentTDSPendingPage);
            tabbedPage.Children.Remove(contentPLWPendingPage);
            tabbedPage.Children.Remove(contentCMCPendingPage);
            tabbedPage.Children.Remove(contentISNPPendingPage); 
            #endregion

            if (Household.Equals("PDS"))
            {
                tabbedPage.Children.Add(contentTDSCompliancePage);
                tabbedPage.Children.Add(contentTDSPendingPage);
                DownloadedTDSComplianceHouseholdMembersGrid.VerticalOptions = LayoutOptions.FillAndExpand;
                PendingSyncTDSComplianceHouseholdMembersGrid.VerticalOptions = LayoutOptions.FillAndExpand;
                vm.PendingSyncTDSComplianceHouseholdMembers.Clear();
                vm.PendingSyncTDSComplianceHouseholdMembers.AddRange(vm.GetTDSHouseholdMembersReadyForSync(KebeleId, ServiceId, Household));
            }

            if (Household.Equals("PLW"))
            {
                tabbedPage.Children.Add(contentPLWCompliancePage);
                tabbedPage.Children.Add(contentPLWPendingPage);
                DownloadedPLWComplianceHouseholdMembersGrid.VerticalOptions = LayoutOptions.FillAndExpand;
                PendingSyncPLWComplianceHouseholdMembersGrid.VerticalOptions = LayoutOptions.FillAndExpand;
                vm.DownloadedPLWComplianceHouseholdMembers.Clear();
                vm.DownloadedPLWComplianceHouseholdMembers.AddRange(vm.GetPLWHouseholdMembersReadyForCompliance(KebeleId, ServiceId, Household));
            }

            if (Household.Equals("CMC"))
            {
                tabbedPage.Children.Add(contentCMCCompliancePage);
                tabbedPage.Children.Add(contentCMCPendingPage);
                DownloadedCMCComplianceHouseholdMembersGrid.VerticalOptions = LayoutOptions.FillAndExpand;
                PendingSyncCMCComplianceHouseholdMembersGrid.VerticalOptions = LayoutOptions.FillAndExpand;
                vm.PendingSyncCMCComplianceHouseholdMembers.Clear();
                vm.PendingSyncCMCComplianceHouseholdMembers.AddRange(vm.GetCMCHouseholdMembersReadyForSync(KebeleId, ServiceId, Household));
            }

            if (Household.Equals("ISNP"))
            {
                tabbedPage.Children.Add(contentISNPCompliancePage);
                tabbedPage.Children.Add(contentISNPPendingPage);
                DownloadedISNPComplianceHouseholdMembersGrid.VerticalOptions = LayoutOptions.FillAndExpand;
                PendingSyncISNPComplianceHouseholdMembersGrid.VerticalOptions = LayoutOptions.FillAndExpand;
                vm.PendingSyncISNPComplianceHouseholdMembers.Clear();
                vm.PendingSyncISNPComplianceHouseholdMembers.AddRange(vm.GetISNPHouseholdMembersReadyForSync(KebeleId, ServiceId, Household));
                vm.DownloadedISNPComplianceHouseholdMembers.Clear();
                vm.DownloadedISNPComplianceHouseholdMembers.AddRange(vm.GetISNPHouseholdMembersReadyForCompliance(KebeleId, ServiceId, Household, Id));
            }

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (Household.Equals("PDS"))
            {
                vm.DownloadedTDSComplianceHouseholdMembers.Clear();
                vm.DownloadedTDSComplianceHouseholdMembers.AddRange(vm.GetTDSHouseholdMembersReadyForCompliance(KebeleId, ServiceId, Household));

                vm.PendingSyncTDSComplianceHouseholdMembers.Clear();
                vm.PendingSyncTDSComplianceHouseholdMembers.AddRange(vm.GetTDSHouseholdMembersReadyForSync(KebeleId, ServiceId, Household));
            }

            if (Household.Equals("PLW"))
            {
                vm.DownloadedPLWComplianceHouseholdMembers.Clear();
                vm.DownloadedPLWComplianceHouseholdMembers.AddRange(vm.GetPLWHouseholdMembersReadyForCompliance(KebeleId, ServiceId, Household));

                vm.PendingSyncPLWComplianceHouseholdMembers.Clear();
                vm.PendingSyncPLWComplianceHouseholdMembers.AddRange(vm.GetPLWHouseholdMembersReadyForSync(KebeleId, ServiceId, Household));
            }

            if (Household.Equals("CMC"))
            {

                vm.DownloadedCMCComplianceHouseholdMembers.Clear();
                vm.DownloadedCMCComplianceHouseholdMembers.AddRange(vm.GetCMCHouseholdMembersReadyForCompliance(KebeleId, ServiceId, Household));

                vm.PendingSyncCMCComplianceHouseholdMembers.Clear();
                vm.PendingSyncCMCComplianceHouseholdMembers.AddRange(vm.GetCMCHouseholdMembersReadyForSync(KebeleId, ServiceId, Household));
            }

            if (Household.Equals("ISNP"))
            {

                vm.DownloadedISNPComplianceHouseholdMembers.Clear();
                vm.DownloadedISNPComplianceHouseholdMembers.AddRange(vm.GetISNPHouseholdMembersReadyForCompliance(KebeleId, ServiceId, Household, Id));

                vm.PendingSyncISNPComplianceHouseholdMembers.Clear();
                vm.PendingSyncISNPComplianceHouseholdMembers.AddRange(vm.GetISNPHouseholdMembersReadyForSync(KebeleId, ServiceId, Household));
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            if (Household.Equals("PDS"))
            {

                vm.DownloadedTDSComplianceHouseholdMembers.Clear();
                vm.PendingSyncTDSComplianceHouseholdMembers.Clear();
            }

            if (Household.Equals("PLW"))
            {
                vm.DownloadedPLWComplianceHouseholdMembers.Clear();
                vm.PendingSyncPLWComplianceHouseholdMembers.Clear();

            }

            if (Household.Equals("CMC"))
            {
                vm.DownloadedCMCComplianceHouseholdMembers.Clear();
                vm.PendingSyncCMCComplianceHouseholdMembers.Clear();
            }

            if (Household.Equals("ISNP"))
            {
                vm.DownloadedISNPComplianceHouseholdMembers.Clear();
                vm.PendingSyncISNPComplianceHouseholdMembers.Clear();
            }
        }

    }
}