﻿using INSCTMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INSCTMIS.Mobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ComplianceHouseholdsListPage : TabbedPage
	{
        private ComplianceHouseholdsListViewModel vm;

        public ComplianceHouseholdsListPage ()
		{
			InitializeComponent ();
            BindingContext = vm = new ComplianceHouseholdsListViewModel(Navigation);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            vm.DownloadedTDSComplianceHouseholds.ReplaceRange(vm.GetTDSHouseholdsReadyForCompliance());
            vm.DownloadedPLWComplianceHouseholds.ReplaceRange(vm.GetPLWHouseholdsReadyForCompliance());
            vm.DownloadedCMCComplianceHouseholds.ReplaceRange(vm.GetCMCHouseholdsReadyForCompliance());
            vm.DownloadedISNPComplianceHouseholds.ReplaceRange(vm.GetISNPHouseholdsReadyForCompliance());
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            vm.DownloadedTDSComplianceHouseholds.Clear();
            vm.DownloadedPLWComplianceHouseholds.Clear();
            vm.DownloadedCMCComplianceHouseholds.Clear();
            vm.DownloadedISNPComplianceHouseholds.Clear();

        }

    }
}