﻿using INSCTMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INSCTMIS.Mobile.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CompliancePage : ContentPage
	{
        private ComplianceViewModel vm;

        public CompliancePage()
        {
            InitializeComponent();
            BindingContext = vm = new ComplianceViewModel(Navigation);
        }
    }
}