﻿using INSCTMIS.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INSCTMIS.Mobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ManagePLWInfantPage : ContentPage
    {
        public ManagePLWInfantPage(int PLWId)
        {
            InitializeComponent();
            BindingContext = new ManagePLWInfantViewModel(Navigation, PLWId);
        }
    }
}