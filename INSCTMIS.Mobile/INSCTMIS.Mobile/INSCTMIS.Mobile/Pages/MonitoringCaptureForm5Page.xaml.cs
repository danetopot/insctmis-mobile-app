﻿using INSCTMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INSCTMIS.Mobile.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MonitoringCaptureForm5Page : ContentPage
	{
        private MonitoringCaptureForm5ViewModel vm;
        public MonitoringCaptureForm5Page (string household, string profiledetailid, int kebeleid, int reportingperiodid, string nocomplyreason, string formname)
		{
			InitializeComponent ();
            BindingContext = vm = new MonitoringCaptureForm5ViewModel(Navigation, household, profiledetailid, kebeleid, reportingperiodid, nocomplyreason, formname);
        }
	}
}