﻿using System;
using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INSCTMIS.Mobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MonitoringForm5Page : TabbedPage
    {
        private MonitoringForm5ViewModel vm;
        ContentPage contentNonComplianceReasonsContentPage, contentNonComplianceReasonsPendingSyncContentPage;
        TabbedPage tabbedPage;

        public MonitoringForm5Page (string household, int kebeleid, string profiledetailid, string reportingperiod)
        {
            InitializeComponent();
            BindingContext = vm = new MonitoringForm5ViewModel(Navigation, household, kebeleid, profiledetailid, reportingperiod);

            #region ManageTabs
            tabbedPage = this.MonitoringListTab;
            contentNonComplianceReasonsContentPage = this.NonComplianceReasonsContentPage;
            contentNonComplianceReasonsPendingSyncContentPage = this.NonComplianceReasonsPendingSyncContentPage;
            tabbedPage.Children.Remove(contentNonComplianceReasonsContentPage);
            tabbedPage.Children.Remove(contentNonComplianceReasonsPendingSyncContentPage);
            #endregion
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            tabbedPage.Children.Add(contentNonComplianceReasonsContentPage);
            tabbedPage.Children.Add(contentNonComplianceReasonsPendingSyncContentPage);

            vm.DownloadedHouseholdsMonitoringServices.Clear();
            vm.DownloadedHouseholdsMonitoringServices.AddRange(vm.GetHouseholdsMonitoringServices(vm.Household));

            vm.PendingSyncMonitoringHouseholds.Clear();
            vm.PendingSyncMonitoringHouseholds.AddRange(vm.GetPendingSyncMonitoringHouseholds(vm.ProfileDetailID));
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            vm.PendingSyncMonitoringHouseholds.Clear();
            vm.DownloadedHouseholdsMonitoringServices.Clear();
        }
    }
}