﻿using INSCTMIS.Mobile.ViewModels;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INSCTMIS.Mobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MonitoringHouseholdMembersListPage : TabbedPage
    {
        private MonitoringHouseholdMembersListViewModel vm;
        private string Household;
        private string HeaderId;

        ContentPage contentTDS1MonitoringMembersContentPage, contentTDS2MonitoringMembersContentPage, contentPLWMonitoringMembersContentPage, contentCMCMonitoringMembersContentPage;
        TabbedPage tabbedPage;

        public MonitoringHouseholdMembersListPage (string household, string headerid)
        {
            InitializeComponent();
            BindingContext = vm = new MonitoringHouseholdMembersListViewModel(Navigation, household, headerid);

            Household = household;
            HeaderId = headerid;

            #region ManageTabs
            tabbedPage = this.HouseholdMembersListTab;
            contentTDS1MonitoringMembersContentPage = this.TDS1MonitoringMembersContentPage;
            contentTDS2MonitoringMembersContentPage = this.TDS2MonitoringMembersContentPage;
            contentPLWMonitoringMembersContentPage = this.PLWMonitoringMembersContentPage;
            contentCMCMonitoringMembersContentPage = this.CMCMonitoringMembersContentPage;
            tabbedPage.Children.Remove(contentTDS1MonitoringMembersContentPage);
            tabbedPage.Children.Remove(contentTDS2MonitoringMembersContentPage);
            tabbedPage.Children.Remove(contentPLWMonitoringMembersContentPage);
            tabbedPage.Children.Remove(contentCMCMonitoringMembersContentPage);
            #endregion
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (Household.Equals("PDS1"))
            {
                tabbedPage.Children.Add(contentTDS1MonitoringMembersContentPage);
                vm.DownloadedTDS1MonitoringHouseholdMembers.Clear();
                vm.DownloadedTDS1MonitoringHouseholdMembers.AddRange(vm.GetTDS1HouseholdMembersReadyForMonitoring(HeaderId));
            }

            if (Household.Equals("PDS2"))
            {
                tabbedPage.Children.Add(contentTDS2MonitoringMembersContentPage);
                vm.DownloadedTDS2MonitoringHouseholdMembers.Clear();
                vm.DownloadedTDS2MonitoringHouseholdMembers.AddRange(vm.GetTDS2HouseholdMembersReadyForMonitoring(HeaderId));
            }

            if (Household.Equals("PLW"))
            {
                tabbedPage.Children.Add(contentPLWMonitoringMembersContentPage);
                vm.DownloadedPLWMonitoringHouseholdMembers.Clear();
                vm.DownloadedPLWMonitoringHouseholdMembers.AddRange(vm.GetPLWHouseholdMembersReadyForMonitoring(HeaderId));
            }

            if (Household.Equals("CMC"))
            {
                tabbedPage.Children.Add(contentCMCMonitoringMembersContentPage);
                vm.DownloadedCMCMonitoringHouseholdMembers.Clear();
                vm.DownloadedCMCMonitoringHouseholdMembers.AddRange(vm.GetCMCHouseholdMembersReadyForMonitoring(HeaderId));
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            vm.DownloadedTDS1MonitoringHouseholdMembers.Clear();
            vm.DownloadedTDS2MonitoringHouseholdMembers.Clear();
            vm.DownloadedPLWMonitoringHouseholdMembers.Clear();
            vm.DownloadedCMCMonitoringHouseholdMembers.Clear();
        }
    }
}