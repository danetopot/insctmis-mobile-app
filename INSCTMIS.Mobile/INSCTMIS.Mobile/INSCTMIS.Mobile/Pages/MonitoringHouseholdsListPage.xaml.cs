﻿using INSCTMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INSCTMIS.Mobile.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MonitoringHouseholdsListPage : TabbedPage
    {
        private MonitoringHouseholdsListViewModel vm;

        public MonitoringHouseholdsListPage ()
		{
			InitializeComponent ();
            BindingContext = vm = new MonitoringHouseholdsListViewModel(Navigation);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }
    }
}