﻿using INSCTMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INSCTMIS.Mobile.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MonitoringPage : ContentPage
	{
        private MonitoringViewModel vm;

        public MonitoringPage()
        {
            InitializeComponent();
            BindingContext = vm = new MonitoringViewModel(Navigation);
        }
    }
}