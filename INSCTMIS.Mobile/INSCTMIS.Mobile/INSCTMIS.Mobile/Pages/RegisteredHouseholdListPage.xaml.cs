﻿using INSCTMIS.Mobile.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INSCTMIS.Mobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RegisteredHouseholdListPage : ContentPage
	{
		public RegisteredHouseholdListPage ()
		{
			InitializeComponent ();
            BindingContext = new RegisteredHouseholdListViewModel(Navigation);
        }
	}
}