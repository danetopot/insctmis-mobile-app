﻿using INSCTMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INSCTMIS.Mobile.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RetargetingCaptureForm7Page : ContentPage
	{
        private RetargetingCaptureForm7ViewModel vm;

        public RetargetingCaptureForm7Page (string retargetingdetailid, bool edit)
		{
			InitializeComponent ();
            BindingContext = vm = new RetargetingCaptureForm7ViewModel(Navigation, retargetingdetailid, edit);

            Status.SelectedIndex = 0;
            Disabled.SelectedIndex = 0;
            ChronicallyIll.SelectedIndex = 0;
            Pregnant.SelectedIndex = 0;
            Lactating.SelectedIndex = 0;
            NutritionalStatus.SelectedIndex = 0;
            ChildUnderTSForCMAM.SelectedIndex = 0;
            EnrolledInSchool.SelectedIndex = 0;

           
        }
    }
}