﻿using INSCTMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INSCTMIS.Mobile.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RetargetingHouseholdMembersListPage : TabbedPage
	{
        private RetargetingHouseholdMembersListViewModel vm;

        public RetargetingHouseholdMembersListPage (string retargetingheaderid)
		{
			InitializeComponent ();
            BindingContext = vm = new RetargetingHouseholdMembersListViewModel(Navigation, retargetingheaderid);
        }
	}
}