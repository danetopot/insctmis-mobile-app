﻿using INSCTMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INSCTMIS.Mobile.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RetargetingHouseholdsListPage : TabbedPage
	{
        private RetargetingHouseholdsListViewModel vm;
        ContentPage contentTDSRetargetingPage, contentPLWRetargetingPage, contentCMCRetargetingPage, contentPendingRetargetingPage;
        TabbedPage tabbedPage;

        public RetargetingHouseholdsListPage ()
		{
			InitializeComponent ();
            BindingContext = vm = new RetargetingHouseholdsListViewModel(Navigation);

            #region ManageTabs
            tabbedPage = this.RetargetingMembersListTab;
            contentTDSRetargetingPage = this.TDSRetargetingMembersContentPage;
            contentPLWRetargetingPage = this.PLWRetargetingMembersContentPage;
            contentCMCRetargetingPage = this.CMCRetargetingMembersContentPage;
            contentPendingRetargetingPage = this.PendingRetargetingMembersContentPage;
            tabbedPage.Children.Remove(contentTDSRetargetingPage);
            tabbedPage.Children.Remove(contentPLWRetargetingPage);
            tabbedPage.Children.Remove(contentCMCRetargetingPage);
            tabbedPage.Children.Remove(contentPendingRetargetingPage);
            #endregion
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            tabbedPage.Children.Add(contentTDSRetargetingPage);
            vm.DownloadedTDSHouseholdsReadyForRetargeting.Clear();
            vm.DownloadedTDSHouseholdsReadyForRetargeting.AddRange(vm.GetTDSHouseholdsReadyForRetargeting(1));

            tabbedPage.Children.Add(contentPLWRetargetingPage);
            vm.DownloadedPLWHouseholdsReadyForRetargeting.Clear();
            vm.DownloadedPLWHouseholdsReadyForRetargeting.AddRange(vm.GetPLWHouseholdsReadyForRetargeting(2));

            tabbedPage.Children.Add(contentCMCRetargetingPage);
            vm.DownloadedCMCHouseholdsReadyForRetargeting.Clear();
            vm.DownloadedCMCHouseholdsReadyForRetargeting.AddRange(vm.GetCMCHouseholdsReadyForRetargeting(3));

            tabbedPage.Children.Add(contentPendingRetargetingPage);
            vm.PendingRetargetingMembersReadyForSync.Clear();
            vm.PendingRetargetingMembersReadyForSync.AddRange(vm.GetPendingRetargetingMembersReadyForSync());
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            vm.DownloadedTDSHouseholdsReadyForRetargeting.Clear();
            vm.DownloadedPLWHouseholdsReadyForRetargeting.Clear();
            vm.DownloadedCMCHouseholdsReadyForRetargeting.Clear();
            vm.PendingRetargetingMembersReadyForSync.Clear();
        }
    }
}