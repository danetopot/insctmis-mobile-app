﻿using INSCTMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INSCTMIS.Mobile.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RetargetingPage : ContentPage
	{
        private RetargetingViewModel vm;

        public RetargetingPage ()
		{
			InitializeComponent ();
            BindingContext = vm = new RetargetingViewModel(Navigation);
        }
	}
}