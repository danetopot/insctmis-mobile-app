﻿using INSCTMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INSCTMIS.Mobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SettingsPage : TabbedPage
    {
        private SettingsViewModel vm;

        public SettingsPage ()
        {
            InitializeComponent();
            BindingContext = vm = new SettingsViewModel(Navigation);
        }
    }
}