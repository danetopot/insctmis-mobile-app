﻿using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Interface;
using INSCTMIS.Mobile.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Threading.Tasks;
using Xamarin.Forms.PlatformConfiguration;

namespace INSCTMIS.Mobile.Services
{
    public class AppClient : IAppClient
    {
        private const string CctpSSOApiKey = "0c833t3w37jq58dj249dt675a465k6b0rz090zl3jpoa9jw8vz7y6awpj5ox0qmb";

        private readonly HttpClient client;

        public AppClient()
            : this(CctpSSOApiKey)
        {
        }

        public AppClient(string apiKey)
        {
            this.client = new HttpClient { BaseAddress = new Uri(Constants.BaseApiAddress) };
            this.client.DefaultRequestHeaders.Clear();
            this.client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<AccountResponse> CreateToken(string username, string password)
        {
            var keyValues = new List<KeyValuePair<string, string>>
                                {
                                    new KeyValuePair<string, string>(
                                        "username",
                                        username),
                                    new KeyValuePair<string, string>(
                                        "password",
                                        password),
                                    new KeyValuePair<string, string>(
                                        "grant_type",
                                        "password"),
                                };

            var json = await this.PostForm("Token", keyValues);
            return JsonConvert.DeserializeObject<AccountResponse>(json);
        }
        
        public async Task<AccountResponse> ForgotPassAsync(string username)
        {
            var form = new List<KeyValuePair<string, string>>
                           {
                               new KeyValuePair<string, string>(
                                   "username",
                                   username),
                           };

            var json = await this.PostForm("api/account/forgotpassword/", form).ConfigureAwait(true);
            return json == "" ? null : JsonConvert.DeserializeObject<AccountResponse>(json);
        }

        public async Task<AccountResponse> ForgotPasswordAsync(string username)
        {
            return await ForgotPassAsync(username);
        }

        public async Task<AccountResponse> LoginAsync(string username, string password) =>
            await this.CreateToken(username, password);

        public async Task<AccountResponse> LoginSocialWorker(string username, string password)
        {

            try
            {
                var form = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("UserName",username),
                    new KeyValuePair<string, string>("Password",password),
                };

                var json = await this.PostForm("api/SocialWorker/Login/", form).ConfigureAwait(true);

                return json == "" ? new AccountResponse() : JsonConvert.DeserializeObject<AccountResponse>(json);
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public async Task<ListingOptionsResponse> GetListingSettings(AccountResponse accountResponse)
        {
            var form = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("UserName",accountResponse.UserName),
                    new KeyValuePair<string, string>("Password",accountResponse.Password),
                };

            var json = await this.PostForm("api/SocialWorker/GetListingSettings/", form).ConfigureAwait(true);

            return json == "" ? new ListingOptionsResponse() : JsonConvert.DeserializeObject<ListingOptionsResponse>(json);
        }

        public async Task<ApiStatus> PostHouseholdsReadyForCompliance(ComplianceCaptureVm compliance)
        {
            var data = JsonConvert.SerializeObject(compliance, Formatting.None);

            var form = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("ComplianceInfo", data)

            };
            var json = await this.PostForm("api/Compliance/PostHouseholdComplianceData/", form).ConfigureAwait(true);

            return json == "" ? new ApiStatus() : JsonConvert.DeserializeObject<ApiStatus>(json);
        }

        public async Task<ApiStatus> PostHouseholdsReadyForMonitoring(MonitoringCaptureVm monitoring)
        {
            var data = JsonConvert.SerializeObject(monitoring, Formatting.None);

            var form = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("MonitoringInfo", data)

            };
            var json = await this.PostForm("api/Monitoring/PostHouseholdsReadyForMonitoring/", form).ConfigureAwait(true);

            return json == "" ? new ApiStatus() : JsonConvert.DeserializeObject<ApiStatus>(json);
        }

        public async Task<ApiStatus> PostHouseholdsReadyForRetargeting(RetargetingCaptureVm retargeting)
        {
            var data = JsonConvert.SerializeObject(retargeting, Formatting.None);

            var form = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("RetargetingInfo", data)

            };
            var json = await this.PostForm("api/Retargeting/PostHouseholdsReadyForRetargeting/", form).ConfigureAwait(true);

            return json == "" ? new ApiStatus() : JsonConvert.DeserializeObject<ApiStatus>(json);
        }

        public async Task<ApiStatus> PostChildProtectionHousehold(ISNPCaseVm isnp)
        {
            var data = JsonConvert.SerializeObject(isnp, Formatting.None);

            var form = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("ISNPInfo", data)

            };
            var json = await this.PostForm("api/Compliance/PostHouseholdChildProtectionData/", form).ConfigureAwait(true);

            return json == "" ? new ApiStatus() : JsonConvert.DeserializeObject<ApiStatus>(json);
        }

        public async Task<ApiStatus> PostHouseholdProfiles(HouseholdProfileVm householdprofile)
        {
            var data = JsonConvert.SerializeObject(householdprofile, Formatting.None);

            var form = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("HouseholdProfileInfo", data)

            };
            var json = await this.PostForm("api/HouseholdProfile/PostHouseholdProfiles/", form).ConfigureAwait(true);

            return json == "" ? new ApiStatus() : JsonConvert.DeserializeObject<ApiStatus>(json);
        }

        public async Task<ComplianceResponse> GetHouseholdsReadyForCompliance(QueryParameter queryParameter)
        {
            var form = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("KebeleID",queryParameter.KebeleID),
                    new KeyValuePair<string, string>("ReportingPeriodID",queryParameter.ReportingPeriodID),
                };

            var json = await this.PostForm("api/Compliance/GetHouseholdsReadyForCompliance/", form).ConfigureAwait(true);

            return json == "" ? new ComplianceResponse() : JsonConvert.DeserializeObject<ComplianceResponse>(json);
        }

        public async Task<MonitoringResponse> GetHouseholdsReadyForMonitoring(QueryParameter queryParameter)
        {
            var form = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("KebeleID",queryParameter.KebeleID),
                    new KeyValuePair<string, string>("ReportingPeriodID",queryParameter.ReportingPeriodID),
                };

            var json = await this.PostForm("api/Monitoring/GetHouseholdsReadyForMonitoring/", form).ConfigureAwait(true);

            return json == "" ? new MonitoringResponse() : JsonConvert.DeserializeObject<MonitoringResponse>(json);
        }

        public async Task<RetargetingResponse> GetHouseholdsReadyForRetargeting(QueryParameter2 queryParameter)
        {
            var form = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("KebeleID",queryParameter.KebeleID),
                    new KeyValuePair<string, string>("FiscalYear",queryParameter.FiscalYear),
                };

            var json = await this.PostForm("api/Retargeting/GetHouseholdsReadyForRetargeting/", form).ConfigureAwait(true);

            return json == "" ? new RetargetingResponse() : JsonConvert.DeserializeObject<RetargetingResponse>(json);
        }

        public async Task<ISNPResponse> GetHouseholdsReadyForChildProtection(QueryParameter queryParameter)
        {
            var form = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("KebeleID",queryParameter.KebeleID),
                    new KeyValuePair<string, string>("ReportingPeriodID",queryParameter.ReportingPeriodID),
                };

            var json = await this.PostForm("api/Compliance/GetHouseholdsReadyForChildProtection/", form).ConfigureAwait(true);

            return json == "" ? new ISNPResponse() : JsonConvert.DeserializeObject<ISNPResponse>(json);
        }

        public async Task<ApiStatus> ForgotPin(string nationalIdNo, string emailAddress, string id)
        {
            var form = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("nationalId",nationalIdNo),
                new KeyValuePair<string, string>("emailAddress",emailAddress),
                new KeyValuePair<string, string>("id",id),
            };
            var json = await this.PostForm("api/Account/ForgotPin", form).ConfigureAwait(true);
            return json == "" ? new ApiStatus() : JsonConvert.DeserializeObject<ApiStatus>(json);
        }

        public async Task<ApiStatus> ChangePin(string currentPin, string newPin, string id)
        {
            var form = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("nationalId",currentPin),
                new KeyValuePair<string, string>("emailAddress",newPin),
                new KeyValuePair<string, string>("id",id),
            };
            var json = await this.PostForm("api/Account/ChangePin", form).ConfigureAwait(true);
            return json == "" ? new ApiStatus() : JsonConvert.DeserializeObject<ApiStatus>(json);
        }

        public async Task LogoutAsync()
        {
            await this.PostForm("api/Account/Logout/", null).ConfigureAwait(true);

            await Task.FromResult(0);
        }

        private async Task<string> PostForm(string endpoint, List<KeyValuePair<string, string>> keyValues)
        {

            try
            {
                var response = await this.client.PostAsync(
                                           this.client.BaseAddress + endpoint,
                                           new FormUrlEncodedContent(keyValues));

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    return data;
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public static Dictionary<string, string> ObjectToDictionary(object obj)
        {
            Dictionary<string, string> ret = new Dictionary<string, string>();

            foreach (PropertyInfo prop in obj.GetType().GetProperties())
            {
                string propName = prop.Name;
                var val = obj.GetType().GetProperty(propName).GetValue(obj, null);
                if (val != null)
                {
                    ret.Add(propName, val.ToString());
                }
                else
                {
                    ret.Add(propName, null);
                }
            }

            return ret;
        }

        public static void Merge<TKey, TValue>(IDictionary<TKey, TValue> first, IDictionary<TKey, TValue> second)
        {
            if (second == null || first == null)
            {
                return;
            }

            foreach (var item in second)
            {
                if (!first.ContainsKey(item.Key))
                {
                    first.Add(item.Key, item.Value);
                }
            }
        }

        //public void ToastMessage(string message)
        //{
        //    Context context = Application.Context;
        //    string text = "Hello toast!";
        //    ToastLength duration = ToastLength.Short;

        //    var toast = Toast.MakeText(context, text, duration);
        //    toast.Show();
        //}
    }

}
