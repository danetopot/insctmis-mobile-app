﻿using INSCTMIS.Mobile.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace INSCTMIS.Mobile.Services
{
    public class GeneralServices
    {

        public GeneralServices()
        {

        }
        public List<Gender> GetGenders()
        {
            List<Gender> genderList = new List<Gender> {
                                             new Gender{ GenderName="Male",   GenderID = "M"},
                                             new Gender{ GenderName="Female", GenderID = "F"}
                                             };

            return genderList;
        }

        public List<YesNo> GetYesNo()
        {
            List<YesNo> yesno = new List<YesNo> {
                                             new YesNo{ Name="N/A", ID = "-"},
                                             new YesNo{ Name="NO", ID = "NO"},
                                             new YesNo{ Name="YES",   ID = "YES"}
                                             };

            return yesno;
        }

        public List<NutritionalStatus> GetNutritionalStatus()
        {
            List<NutritionalStatus> nutrStatus = new List<NutritionalStatus> {
                                             new NutritionalStatus{ Name="N/A", ID = "-"},
                                             new NutritionalStatus{ Name="Normal", ID = "N"},
                                             new NutritionalStatus{ Name="Mod.", ID = "M"},
                                             new NutritionalStatus{ Name="Sev. Malnourished", ID = "SM"}
                                             };

            return nutrStatus;
        }

        public List<SchoolGrade> GetSchoolGrade()
        {
            List<SchoolGrade> classgrade = new List<SchoolGrade> {
                                             new SchoolGrade{ Name="Select Grade",   ID = ""},
                                             new SchoolGrade{ Name="Grade 1",   ID = "1"},
                                             new SchoolGrade{ Name="Grade 2",   ID = "2"},
                                             new SchoolGrade{ Name="Grade 3",   ID = "3"},
                                             new SchoolGrade{ Name="Grade 4", ID = "4"},
                                             new SchoolGrade{ Name="Grade 5",   ID = "5"},
                                             new SchoolGrade{ Name="Grade 6",   ID = "6"},
                                             new SchoolGrade{ Name="Grade 7",   ID = "7"},
                                             new SchoolGrade{ Name="Grade 8", ID = "8"},
                                             new SchoolGrade{ Name="Grade 9",   ID = "9"},
                                             new SchoolGrade{ Name="Grade 10",   ID = "10"},
                                             new SchoolGrade{ Name="Grade 11",   ID = "11"},
                                             new SchoolGrade{ Name="Grade 12", ID = "12"},
                                             new SchoolGrade{ Name="Grade 13",   ID = "13"},
                                             new SchoolGrade{ Name="Grade 14",   ID = "14"}
                                             };

            return classgrade;
        }

        public List<GeneralOptions> GetPregnantOrLactating()
        {
            List<GeneralOptions> optionList = new List<GeneralOptions> {
                                             new GeneralOptions{ Name="Lactating",   ID = "L"},
                                             new GeneralOptions{ Name="Pregnant", ID = "P"}
                                             };

            return optionList;
        }
    }
}
