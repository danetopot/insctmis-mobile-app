﻿using FluentValidation;
using INSCTMIS.Mobile.ViewModels;

namespace INSCTMIS.Mobile.Validators
{
    public class HouseholdValidator : AbstractValidator<ManageHouseholdViewModel>
    {
        public HouseholdValidator()
        {
            RuleFor(x => x.RegionID).NotEmpty().GreaterThanOrEqualTo(0).WithMessage("Region is required");
            RuleFor(x => x.WoredaID).NotEmpty().GreaterThanOrEqualTo(0).WithMessage("Woreda is required");
            RuleFor(x => x.KebeleID).NotEmpty().GreaterThanOrEqualTo(0).WithMessage("Kebele is required");
            RuleFor(x => x.CBHIMembership).NotEmpty().WithMessage("CBHIMembership is required");
            RuleFor(x => x.HouseholdIDNumber).NotNull().NotEmpty().WithMessage("PSNP HH Number is required");
            RuleFor(x => x.NameOfHouseHoldHead).NotNull().NotEmpty().WithMessage("Name of Household Head is required");
        }
    }

    public class HouseholdMemberValidator : AbstractValidator<ManageHouseholdMemberViewModel>
    {
        public HouseholdMemberValidator()
        {
            RuleFor(x => x.HouseHoldMemberName).NotNull().NotEmpty().WithMessage("Name of household member is required");
            RuleFor(x => x.DateOfBirth).NotNull().NotEmpty().WithMessage("Date of birth is required");
            RuleFor(x => x.Age).NotNull().NotEmpty().WithMessage("Age is required");
            RuleFor(x => x.Sex).NotNull().NotEmpty().WithMessage("Sex/Gender is required");
            RuleFor(x => x.Handicapped).NotNull().NotEmpty().WithMessage("Disability status is required");
            RuleFor(x => x.ChronicallyIll).NotNull().NotEmpty().When(x => x.Age <= 5).WithMessage("Chronic illness status is required");
        }
    }

    public class PLWValidator : AbstractValidator<ManagePLWViewModel>
    {
        public PLWValidator()
        {
            RuleFor(x => x.RegionID).NotNull().NotEmpty().WithMessage("Region is required");
            RuleFor(x => x.WoredaID).NotNull().NotEmpty().WithMessage("Woreda is required");
            RuleFor(x => x.KebeleID).NotNull().NotEmpty().WithMessage("Kebele is required");
            RuleFor(x => x.NameOfPLW).NotNull().NotEmpty().WithMessage("Name of PLW is required");
            RuleFor(x => x.PLW).NotNull().NotEmpty().WithMessage("Pregant/Lactating status is required");
            RuleFor(x => x.HouseHoldIDNumber).NotNull().NotEmpty().WithMessage("PSNP Number is required");
            RuleFor(x => x.CBHIMembership).NotNull().NotEmpty().WithMessage("CBHIMembership is required");
            RuleFor(x => x.CBHINumber).NotNull().NotEmpty().When(x => x.CBHIMembership.Equals("YES")).WithMessage("CBHINumber is required");
            RuleFor(x => x.CCCCBSPCMember).NotNull().NotEmpty().WithMessage("CCC Membership status is required");
            RuleFor(x => x.MedicalRecordNumber).NotNull().NotEmpty().WithMessage("Individual ID is required");
            RuleFor(x => x.PLWAge).NotNull().NotEmpty().WithMessage("Age is required");
            RuleFor(x => x.StartDateTDS).NotNull().NotEmpty().WithMessage("Transitioning date is required");
            RuleFor(x => x.EndDateTDS).NotNull().NotEmpty().GreaterThan(x => x.StartDateTDS).WithMessage("Expected temporary end date is required");
            RuleFor(x => x.NutritionalStatusPLW).NotNull().NotEmpty().WithMessage("Nutrition status is required");
            RuleFor(x => x.ChildProtectionRisk).NotNull().NotEmpty().WithMessage("Child protection status is required");
        }
    }

    public class InfantValidator : AbstractValidator<ManagePLWInfantViewModel>
    {
        public InfantValidator()
        {
            RuleFor(x => x.BabyDateOfBirth).NotNull().NotEmpty().WithMessage("D.O.B of infant is required");
            RuleFor(x => x.BabyName).NotNull().NotEmpty().WithMessage("Infant name is required");
            RuleFor(x => x.BabySex).NotNull().NotEmpty().WithMessage("Infant sex is required");
            RuleFor(x => x.NutritionalStatusInfant).NotNull().NotEmpty().WithMessage("Nutritional status of the infant is required");
        }
    }

    public class CaretakerValidator : AbstractValidator<ManageCaretakerViewModel>
    {
        public CaretakerValidator()
        {
            RuleFor(x => x.RegionID).NotNull().NotEmpty().WithMessage("Region is required");
            RuleFor(x => x.WoredaID).NotNull().NotEmpty().WithMessage("Woreda is required");
            RuleFor(x => x.KebeleID).NotNull().NotEmpty().WithMessage("Kebele is required");
            RuleFor(x => x.NameOfCareTaker).NotNull().NotEmpty().WithMessage("Name of caretaker is required");
            RuleFor(x => x.CaretakerID).NotNull().NotEmpty().WithMessage("Caretaker ID is required");
            RuleFor(x => x.HouseHoldIDNumber).NotNull().NotEmpty().WithMessage("PSNP Number is required");
            RuleFor(x => x.CBHIMembership).NotNull().NotEmpty().WithMessage("CBHIMembership is required");
            RuleFor(x => x.CCCCBSPCMember).NotNull().NotEmpty().WithMessage("CCC/CBSPCMember is required");
            RuleFor(x => x.CollectionDate).NotNull().NotEmpty().WithMessage("Collection Date is required");
            RuleFor(x => x.MalnourishedChildName).NotNull().NotEmpty().WithMessage("Child name is required");
            RuleFor(x => x.ChildID).NotNull().NotEmpty().WithMessage("Child ID is required");
            RuleFor(x => x.MalnourishedChildSex).NotNull().NotEmpty().WithMessage("Child gender is required");
            RuleFor(x => x.ChildDateOfBirth).NotNull().NotEmpty().WithMessage("Child D.O.B is required");
        }
    }
}
