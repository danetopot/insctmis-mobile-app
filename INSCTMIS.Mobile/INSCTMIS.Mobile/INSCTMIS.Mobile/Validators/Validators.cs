﻿using System;
using INSCTMIS.Mobile.Database;
using FluentValidation;
using INSCTMIS.Mobile.ViewModels;

namespace INSCTMIS.Mobile.Validators
{
    public class LoginValidator : AbstractValidator<LoginViewModel>
    {
        public LoginValidator()
        {
            RuleFor(x => x.Username).NotEmpty().Matches(@"^[A-Za-z0-9_'--, ]{1,100}$").WithMessage("Username is required.");
            RuleFor(x => x.Password).NotEmpty().Matches(@"^[A-Za-z0-9_'--, ]{1,100}$").WithMessage("Password is required.");
        }
    }
}