﻿using FluentValidation;
using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Interface;
using INSCTMIS.Mobile.Pages;
using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    class ComplianceCaptureForm4ABCViewModel : LocalBaseViewModel
    {
        private IAppClient client;
        public INavigation Navigation;
        Dictionary<string, string> AppSettings = null;
        private readonly IValidator _validator;
        private ICommand _editComplianceCommand;
        private ICommand _saveComplianceCommand;

        private SystemCodeDetail _selectedHasComplied;
        private string _remarks;
        public ComplianceTDSMembers ComplianceTDS { get; set; }
        public ComplianceTDSPLWMembers CompliancePLW { get; set; }
        public ComplianceTDSCMCMembers ComplianceCMC { get; set; }


        public ObservableRangeCollection<SystemCodeDetail> YesNoOptions = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> LoadedYesNoOptions
        {
            get => YesNoOptions;
            set => SetProperty(ref YesNoOptions, value);
        }

        public SystemCodeDetail SelectedHasCompliedOption
        {
            get => _selectedHasComplied;
            set
            {
                if (this._selectedHasComplied == value)
                {
                    return;
                }

                this._selectedHasComplied = value;

                this.OnPropertyChanged();
            }
        }

        public string Remarks
        {
            get { return _remarks; }
            set
            {
                SetProperty(ref _remarks, value);
                OnPropertyChanged();

            }
        }

        public ICommand SaveComplianceCommand => _saveComplianceCommand ?? (_saveComplianceCommand = new Command(async () => await ExecuteSaveCompliance()));
        private async Task ExecuteSaveCompliance()
        {
            try
            {
                IsBusy = true;
                Message = "Validating .. ";
                var errorMessage = "";
                var complianceTDS = this.ComplianceTDS;
                var compliancePLW = this.CompliancePLW;
                var complianceCMC = this.ComplianceCMC;

                if(Household.Equals("PDS"))
                {
                    complianceTDS.ComplianceBy = Convert.ToInt64(AppSettings["UserId"]);
                    complianceTDS.HouseholdType = Household;
                    if (SelectedHasCompliedOption == null)
                    {
                        errorMessage += "Please Provide Has Complied";
                    }
                    else
                    {
                        complianceTDS.HasComplied = SelectedHasCompliedOption.Value;
                    }
                    complianceTDS.Remarks = Remarks;
                    complianceTDS.Status = "PENDING SYNC";
                }
                if (Household.Equals("PLW"))
                {
                    compliancePLW.ComplianceBy = Convert.ToInt64(AppSettings["UserId"]);
                    compliancePLW.HouseholdType = Household;
                    if (SelectedHasCompliedOption == null)
                    {
                        errorMessage += "Please Provide Has Complied";
                    }
                    else
                    {
                        compliancePLW.HasComplied = SelectedHasCompliedOption.Value;
                    }
                    compliancePLW.Remarks = Remarks;
                    compliancePLW.Status = "PENDING SYNC";
                }
                if (Household.Equals("CMC"))
                {
                    complianceCMC.ComplianceBy = Convert.ToInt64(AppSettings["UserId"]);
                    complianceCMC.HouseholdType = Household;
                    if (SelectedHasCompliedOption == null)
                    {
                        errorMessage += "Please Provide Has Complied";
                    }
                    else
                    {
                        complianceCMC.HasComplied = SelectedHasCompliedOption.Value;
                    }
                    complianceCMC.Remarks = Remarks;
                    complianceCMC.Status = "PENDING SYNC";
                }



                if (errorMessage == "")
                {
                    Message = "Updating Database .. ";
                    if (Household.Equals("PDS"))
                    {
                        App.Database.AddOrUpdate(complianceTDS);
                    }
                    if (Household.Equals("PLW"))
                    {
                        App.Database.AddOrUpdate(compliancePLW);
                    }
                    if (Household.Equals("CMC"))
                    {
                        App.Database.AddOrUpdate(complianceCMC);
                    }

                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Success",
                        Message = "Save successful.",
                        Cancel = "OK"
                    });
                    await Navigation.PopAsync(true);
                    await Navigation.PushAsync(new ComplianceHouseholdMembersListPage(KebeleId, ServiceId, Household, null));

                }
                else
                {

                    if (errorMessage.Length > 0)
                    {
                        ValidateMessage = $"{ValidateMessage}\n{errorMessage}";
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(
                            MessageKeys.Error,
                            new MessagingServiceAlert
                            {
                                Title = "Please Check the Data and Try Again!!",
                                Message = ValidateMessage,
                                Cancel = "OK"
                            });
                        IsBusy = false;
                    }
                }



            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                           MessageKeys.Error,
                           new MessagingServiceAlert
                           {
                               Title = "Please Correct the Data and Try Again!!",
                               Message = e.Message,
                               Cancel = "OK"
                           });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            return;
        }

        public ComplianceCaptureForm4ABCViewModel(INavigation navigation, int kebeleid, int serviceid, string household, string id) : base(navigation)
        {
            client = DependencyService.Get<IAppClient>();
            Navigation = navigation;
            KebeleId = kebeleid;
            ServiceId = serviceid;
            Household = household;

            Title = "Compliance Capture";
            HasCompliedLabel = "Has Complied";
            RemarksCompliedLabel = "Remarks";
            Button = "Save";

            if (household.Equals("PDS"))
            {
                Header = "Capture Form 4A";
                var data = App.Database.GetTableRow("ComplianceTDSMembers", "ProfileDSDetailID", id.ToString(), "ServiceID", serviceid.ToString());
                ComplianceTDS = (ComplianceTDSMembers)data;
                MemberNameLabel = ComplianceTDS.HouseHoldMemberName;
                ServiceNameLabel = ComplianceTDS.ServiceName;
            }

            if (household.Equals("PLW"))
            {
                Header = "Capture Form 4B";
                var data = App.Database.GetTableRow("ComplianceTDSPLWMembers", "ProfileTDSPLWID", id.ToString(), "ServiceID", serviceid.ToString());
                CompliancePLW = (ComplianceTDSPLWMembers)data;
                MemberNameLabel = CompliancePLW.NameOfPLW;
                ServiceNameLabel = CompliancePLW.ServiceName;
            }

            if (household.Equals("CMC"))
            {
                Header = "Capture Form 4C";
                var data = App.Database.GetTableRow("ComplianceTDSCMCMembers", "ProfileTDSCMCID", id.ToString(), "ServiceID", serviceid.ToString());
                ComplianceCMC = (ComplianceTDSCMCMembers)data;
                MemberNameLabel = ComplianceCMC.NameOfCareTaker;
                ServiceNameLabel = ComplianceCMC.ServiceName;
            }

            LoadedYesNoOptions.AddRange(App.Database.SystemCodeDetailsGetByCode("YesNoOptions"));

            AppSettings = GetAppSettings();
        }

        private string message;
        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private string messageSync;
        public string MessageSync
        {
            get { return messageSync; }
            set { SetProperty(ref messageSync, value); }
        }

        private string header;
        public string Header
        {
            get { return header; }
            set { SetProperty(ref header, value); }
        }

        private string title;
        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }

        private string button;
        public string Button
        {
            get { return button; }
            set { SetProperty(ref button, value); }
        }

        private string hasCompliedLabel;
        public string HasCompliedLabel
        {
            get { return hasCompliedLabel; }
            set { SetProperty(ref hasCompliedLabel, value); }
        }

        private string remarksCompliedLabel;
        public string RemarksCompliedLabel
        {
            get { return remarksCompliedLabel; }
            set { SetProperty(ref remarksCompliedLabel, value); }
        }

        private string serviceNameLabel;
        public string ServiceNameLabel
        {
            get { return serviceNameLabel; }
            set { SetProperty(ref serviceNameLabel, value); }
        }

        private string memberNameLabel;
        public string MemberNameLabel
        {
            get { return memberNameLabel; }
            set { SetProperty(ref memberNameLabel, value); }
        }

        private int kebeleId;
        public int KebeleId
        {
            get { return kebeleId; }
            set { SetProperty(ref kebeleId, value); }
        }

        private int serviceId;
        public int ServiceId
        {
            get { return serviceId; }
            set { SetProperty(ref serviceId, value); }
        }

        private string household;
        public string Household
        {
            get { return household; }
            set { SetProperty(ref household, value); }
        }


    }
}
