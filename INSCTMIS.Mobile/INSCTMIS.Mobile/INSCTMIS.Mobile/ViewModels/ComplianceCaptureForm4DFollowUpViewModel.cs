﻿using FluentValidation;
using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Interface;
using INSCTMIS.Mobile.Pages;
using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;


namespace INSCTMIS.Mobile.ViewModels
{
    class ComplianceCaptureForm4DFollowUpViewModel : LocalBaseViewModel
    {
        private IAppClient client;
        public INavigation Navigation;
        Dictionary<string, string> AppSettings = null;
        private ICommand _saveFollowUpCommand;
        private ICommand _saveFollowUpDetailCommand;
        public ComplianceCPHouseholdMembers ComplianceISNP { get; set; }
        public ComplianceCPCaseVisitHeader ComplianceCPCaseVisitHeader { get; set; }
        public ComplianceCPCaseVisitDetail ComplianceCPCaseVisitDetail { get; set; }

        public ComplianceCaptureForm4DFollowUpViewModel(INavigation navigation, string headerid, string isnpdataid, string childid, string risk, string service, string provider) : base(navigation)
        {
            client = DependencyService.Get<IAppClient>();
            Navigation = navigation;

            var data = App.Database.GetTableRow("ComplianceCPHouseholdMembers", "ProfileDSDetailID", childid);
            ComplianceISNP = (ComplianceCPHouseholdMembers)data;
            ChildName = ComplianceISNP.HouseHoldMemberName;
            ChildProtectionVisitHeaderId = Guid.NewGuid();
            ChildProtectionHeaderId = Guid.Parse(headerid);
            ChildProtectionDetailID = Guid.Parse(isnpdataid);
            ChildDetailId = Guid.Parse(childid);

            IsServiceAccessed = false;
            IsServiceNotAccessed = false;
            Button = "Save";
            RiskName = risk;
            ServiceProvidedName = service;
            ServiceProviderName = provider;
            RiskNameLabel = $"Risk  : " + risk;
            ServiceProvidedNameLabel = $"Service  : " + service;
            ServiceProviderNameLabel = $"Provider  : " + provider;
            TitleFollowUp = "Follow Up";
            TitlePendingSync = "Pending Upload";
            WasServiceAccessedLabel = "Was Service Accessed";
            DateServiceAccessedLabel = "Date Service Accessed";
            ReasonServiceNotAccessedLabel = "Reason Service Not Accessed";

            LoadedYesNoOptions.AddRange(App.Database.SystemCodeDetailsGetByCode("YesNoOptions"));
            LoadedReasonsServiceNotAccessibleOptions.AddRange(App.Database.SystemCodeDetailsGetByCode("ReasonsServiceNotAccessibleOptions"));

            AppSettings = GetAppSettings();
        }

        private SystemCodeDetail _selectedWasServiceAccessed;
        public SystemCodeDetail SelectedWasServiceAccessed
        {
            get => _selectedWasServiceAccessed;
            set
            {
                if (this._selectedWasServiceAccessed == value)
                {
                    return;
                }

                this._selectedWasServiceAccessed = value;

                if(this._selectedWasServiceAccessed.Value.Equals("YES"))
                {
                    IsServiceAccessed = true;
                    IsServiceNotAccessed = false;
                } else {

                    IsServiceAccessed = false;
                    IsServiceNotAccessed = true;
                }


                this.OnPropertyChanged();
            }
        }

        private SystemCodeDetail _selectedReasonServiceNotAccessed;
        public SystemCodeDetail SelectedReasonServiceNotAccessed
        {
            get => _selectedReasonServiceNotAccessed;
            set
            {
                if (this._selectedReasonServiceNotAccessed == value)
                {
                    return;
                }

                this._selectedReasonServiceNotAccessed = value;

                this.OnPropertyChanged();
            }
        }

        private Guid childProtectionVisitHeaderId;
        public Guid ChildProtectionVisitHeaderId
        {
            get { return childProtectionVisitHeaderId; }
            set { SetProperty(ref childProtectionVisitHeaderId, value); }
        }

        private Guid childProtectionHeaderId;
        public Guid ChildProtectionHeaderId
        {
            get { return childProtectionHeaderId; }
            set { SetProperty(ref childProtectionHeaderId, value); }
        }

        private Guid childProtectionDetailID;
        public Guid ChildProtectionDetailID
        {
            get { return childProtectionDetailID; }
            set { SetProperty(ref childProtectionDetailID, value); }
        }

        private Guid childDetailId;
        public Guid ChildDetailId
        {
            get { return childDetailId; }
            set { SetProperty(ref childDetailId, value); }
        }

        private string button;
        public string Button
        {
            get { return button; }
            set { SetProperty(ref button, value); }
        }

        private bool isServiceAccessed;
        public bool IsServiceAccessed
        {
            get { return isServiceAccessed; }
            set { SetProperty(ref isServiceAccessed, value); }
        }

        private bool isServiceNotAccessed;
        public bool IsServiceNotAccessed
        {
            get { return isServiceNotAccessed; }
            set { SetProperty(ref isServiceNotAccessed, value); }
        }

        private string wasServiceAccessedLabel;
        public string WasServiceAccessedLabel
        {
            get { return wasServiceAccessedLabel; }
            set { SetProperty(ref wasServiceAccessedLabel, value); }
        }

        private string wasServiceAccessed;
        public string WasServiceAccessed
        {
            get { return wasServiceAccessed; }
            set { SetProperty(ref wasServiceAccessed, value); }
        }

        private DateTime dateServiceAccessed;
        public DateTime DateServiceAccessed
        {
            get { return dateServiceAccessed; }
            set { SetProperty(ref dateServiceAccessed, value); }
        }

        private string dateServiceAccessedLabel;
        public string DateServiceAccessedLabel
        {
            get { return dateServiceAccessedLabel; }
            set { SetProperty(ref dateServiceAccessedLabel, value); }
        }

        private string reasonServiceNotAccessed;
        public string ReasonServiceNotAccessed
        {
            get { return reasonServiceNotAccessed; }
            set { SetProperty(ref reasonServiceNotAccessed, value); }
        }

        private string reasonServiceNotAccessedLabel;
        public string ReasonServiceNotAccessedLabel
        {
            get { return reasonServiceNotAccessedLabel; }
            set { SetProperty(ref reasonServiceNotAccessedLabel, value); }
        }

        private string childName;
        public string ChildName
        {
            get { return childName; }
            set { SetProperty(ref childName, value); }
        }

        private string riskName;
        public string RiskName
        {
            get { return riskName; }
            set { SetProperty(ref riskName, value); }
        }

        private string serviceProvidedName;
        public string ServiceProvidedName
        {
            get { return serviceProvidedName; }
            set { SetProperty(ref serviceProvidedName, value); }
        }

        private string serviceProviderName;
        public string ServiceProviderName
        {
            get { return serviceProviderName; }
            set { SetProperty(ref serviceProviderName, value); }
        }

        private string riskNameLabel;
        public string RiskNameLabel
        {
            get { return riskNameLabel; }
            set { SetProperty(ref riskNameLabel, value); }
        }

        private string serviceProvidedNameLabel;
        public string ServiceProvidedNameLabel
        {
            get { return serviceProvidedNameLabel; }
            set { SetProperty(ref serviceProvidedNameLabel, value); }
        }

        private string serviceProviderNameLabel;
        public string ServiceProviderNameLabel
        {
            get { return serviceProviderNameLabel; }
            set { SetProperty(ref serviceProviderNameLabel, value); }
        }

        private string titleFollowUp;
        public string TitleFollowUp
        {
            get { return titleFollowUp; }
            set { SetProperty(ref titleFollowUp, value); }
        }

        private string titlePendingSync;
        public string TitlePendingSync
        {
            get { return titlePendingSync; }
            set { SetProperty(ref titlePendingSync, value); }
        }

        public ICommand SaveFollowUpCommand => _saveFollowUpCommand ?? (_saveFollowUpCommand = new Command(async () => await ExecuteSaveFollowUp()));
        private async Task ExecuteSaveFollowUp()
        {
            try
            {
                IsBusy = true;
                Message = "Validating .. ";
                var errorMessage = "";

                var complianceCPCaseVisitHeader = new ComplianceCPCaseVisitHeader();
                complianceCPCaseVisitHeader.ChildProtectionVisitHeaderId = ChildProtectionVisitHeaderId;
                complianceCPCaseVisitHeader.ChildProtectionHeaderID = ChildProtectionHeaderId;
                complianceCPCaseVisitHeader.VisitDate = DateTime.Now;
                complianceCPCaseVisitHeader.SocialWorker = Settings.UserName;
                complianceCPCaseVisitHeader.CreatedOn = DateTime.Now;
                complianceCPCaseVisitHeader.Status = "PENDING SYNC";
                complianceCPCaseVisitHeader.CreatedBy = AppSettings["UserName"];

                var complianceCPCaseVisitDetail = new ComplianceCPCaseVisitDetail();
                complianceCPCaseVisitDetail.ChildProtectionVisitDetailId = Guid.NewGuid();
                complianceCPCaseVisitDetail.ChildProtectionVisitHeaderId = ChildProtectionVisitHeaderId;
                complianceCPCaseVisitDetail.ChildProtectionDetailID = ChildProtectionDetailID;
                complianceCPCaseVisitDetail.ChildDetailID = ChildDetailId;
                complianceCPCaseVisitDetail.ChildName = ChildName;
                complianceCPCaseVisitDetail.RiskName = RiskName;
                complianceCPCaseVisitDetail.ServiceName = ServiceProvidedName;
                complianceCPCaseVisitDetail.ProviderName = ServiceProviderName;
                complianceCPCaseVisitDetail.Status = "PENDING SYNC";

                if (string.IsNullOrEmpty(SelectedWasServiceAccessed.Value))
                    errorMessage += "Please Provide Was Service Accessed";
                else
                    complianceCPCaseVisitDetail.IsServiceAccessed = SelectedWasServiceAccessed.Value;

                if (string.IsNullOrEmpty(SelectedReasonServiceNotAccessed.Value) && SelectedWasServiceAccessed.Value == "NO")
                    errorMessage += "Please Provide Reason Service Not Accessed";
                else
                    complianceCPCaseVisitDetail.ServiceNotAccessedReason = SelectedReasonServiceNotAccessed.Value;
                    complianceCPCaseVisitDetail.DateServiceAccessed = null;

                if (string.IsNullOrEmpty(DateServiceAccessed.ToString()) && SelectedWasServiceAccessed.Value == "YES")
                    errorMessage += "Please Provide Date Service Accessed";
                else
                    complianceCPCaseVisitDetail.DateServiceAccessed = DateServiceAccessed;
                    complianceCPCaseVisitDetail.ServiceNotAccessedReason = null;



                if (errorMessage == "")
                {
                    Message = "Updating Database .. ";

                    var data = App.Database.GetTableRows("ComplianceCPCaseVisitHeader", "ChildProtectionVisitHeaderId", ChildProtectionVisitHeaderId.ToString());

                    if(data.Count == 0)
                    {
                        App.Database.AddOrUpdate(complianceCPCaseVisitHeader);
                    }

                    App.Database.AddOrUpdate(complianceCPCaseVisitDetail);

                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Success",
                        Message = "Save successful.",
                        Cancel = "OK"
                    });

                    //await Navigation.PopAsync(true);
                    await Navigation.PushAsync(new ComplianceCaptureForm4DPage(ChildDetailId.ToString(), false));
                }
                else
                {

                    if (errorMessage.Length > 0)
                    {
                        ValidateMessage = $"{ValidateMessage}\n{errorMessage}";
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(
                            MessageKeys.Error,
                            new MessagingServiceAlert
                            {
                                Title = "Please Check the Data and Try Again!!",
                                Message = ValidateMessage,
                                Cancel = "OK"
                            });
                        IsBusy = false;
                    }
                }



            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                           MessageKeys.Error,
                           new MessagingServiceAlert
                           {
                               Title = "Please Correct the Data and Try Again!!",
                               Message = e.Message,
                               Cancel = "OK"
                           });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            return;
        }

        public ObservableRangeCollection<SystemCodeDetail> YesNoOptions = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> LoadedYesNoOptions
        {
            get => YesNoOptions;
            set => SetProperty(ref YesNoOptions, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> ReasonsServiceNotAccessibleOptions = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> LoadedReasonsServiceNotAccessibleOptions
        {
            get => ReasonsServiceNotAccessibleOptions;
            set => SetProperty(ref ReasonsServiceNotAccessibleOptions, value);
        }

        public ObservableRangeCollection<ComplianceCPCaseVisitDetail> PendingSyncComplianceCPCaseFollowUp { get; } = new ObservableRangeCollection<ComplianceCPCaseVisitDetail>();
        public ObservableCollection<ComplianceCPCaseVisitDetail> GetPendingSyncComplianceCPCaseFollowUp(string id)
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetTableRows("ComplianceCPCaseVisitDetail", "ChildProtectionDetailID", id.ToString());
                var hh = new ObservableCollection<ComplianceCPCaseVisitDetail>();
                foreach (var item in items)
                {
                    var hhitem = (ComplianceCPCaseVisitDetail)item;
                    hh.Add(hhitem);
                }
                return hh;
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Data",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }
    }

}
