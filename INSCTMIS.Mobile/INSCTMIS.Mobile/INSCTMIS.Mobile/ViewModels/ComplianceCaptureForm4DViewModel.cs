﻿using FluentValidation;
using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Interface;
using INSCTMIS.Mobile.Pages;
using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    class ComplianceCaptureForm4DViewModel : LocalBaseViewModel
    {
        private string ChildProtectionDetailID;
        private IAppClient client;
        public INavigation Navigation;
        Dictionary<string, string> AppSettings = null;
        private ICommand _saveComplianceCommand;
        private ICommand _editComplianceCommand;
        private ICommand _saveRiskCommand;
        private ICommand _editRiskCommand;
        private readonly IValidator _validator;
        private SystemCodeDetail _selectedRefferedToCaseManagement;
        private SystemCodeDetail _selectedRisk;
        private SystemCodeDetail _selectedServiceProvided;
        private SystemCodeDetail _selectedServiceProvider;
        public ComplianceCPCase ComplianceCPCase { get; set; }
        public ComplianceCPCaseHeader ComplianceCPCaseHeader { get; set; }
        public ComplianceCPCaseDetail ComplianceCPCaseDetail { get; set; }
        public ComplianceCPHouseholdMembers ComplianceISNP { get; set; }

        public ObservableRangeCollection<SystemCodeDetail> YesNoOptions = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> LoadedYesNoOptions
        {
            get => YesNoOptions;
            set => SetProperty(ref YesNoOptions, value);
        }
        public SystemCodeDetail SelectedRefferedToCaseManagementOption
        {
            get => _selectedRefferedToCaseManagement;
            set
            {
                if (this._selectedRefferedToCaseManagement == value)
                {
                    return;
                }

                this._selectedRefferedToCaseManagement = value;

                this.OnPropertyChanged();
            }
        }

        public ObservableRangeCollection<SystemCodeDetail> RiskOptions = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> LoadedRiskOptions
        {
            get => RiskOptions;
            set => SetProperty(ref RiskOptions, value);
        }
        public SystemCodeDetail SelectedRiskOption
        {
            get => _selectedRisk;
            set
            {
                if (this._selectedRisk == value)
                {
                    return;
                }

                this._selectedRisk = value;

                this.OnPropertyChanged();
            }
        }

        public ObservableRangeCollection<SystemCodeDetail> ServiceProvidedOptions = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> LoadedServiceProvidedOptions
        {
            get => ServiceProvidedOptions;
            set => SetProperty(ref ServiceProvidedOptions, value);
        }
        public SystemCodeDetail SelectedServiceProvidedOption
        {
            get => _selectedServiceProvided;
            set
            {
                if (this._selectedServiceProvided == value)
                {
                    return;
                }

                this._selectedServiceProvided = value;

                this.OnPropertyChanged();
            }
        }

        public ObservableRangeCollection<SystemCodeDetail> ServiceProviderOptions = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> LoadedServiceProviderOptions
        {
            get => ServiceProviderOptions;
            set => SetProperty(ref ServiceProvidedOptions, value);
        }
        public SystemCodeDetail SelectedServiceProviderOption
        {
            get => _selectedServiceProvider;
            set
            {
                if (this._selectedServiceProvider == value)
                {
                    return;
                }

                this._selectedServiceProvider = value;

                this.OnPropertyChanged();
            }
        }



        public ComplianceCaptureForm4DViewModel(INavigation navigation, string id) : base(navigation)
        {
            client = DependencyService.Get<IAppClient>();
            Navigation = navigation;
            Error = false;
            ChildDetailID = Guid.Parse(id);
            ChildProtectionId = GenerateId();
            ChildProtectionHeaderId = Guid.NewGuid();
            ChildProtectionVisitHeaderId = Guid.NewGuid();
            Tab1Header = "Capture Form 4D";
            Tab1Title = "Child Data";
            Tab2Title = "Risks/Services";
            Tab3Title = "Existing Cases";
            Tab4Title = "Follow Ups";
            ChildProtectionIdLabel = "Child Protection ID";
            RefferedToCaseManagementLabel = "Reffered To CaseManagement";
            RemarksLabel = "Remarks";
            RiskLabel = "Risk Identified";
            ServiceProvidedLabel = "Service to be Provided";
            ServiceProviderLabel = "Service Provider";
            SaveAllButton = "Save All";
            AddNewButton = "Add New";

            var data = App.Database.GetTableRow("ComplianceCPHouseholdMembers", "ProfileDSDetailID", id.ToString());
            ComplianceISNP = (ComplianceCPHouseholdMembers)data;
            ClientTypeId = ComplianceISNP.ClientType;
            MemberNameLabel = ChildName = ComplianceISNP.HouseHoldMemberName;

            LoadedYesNoOptions.AddRange(App.Database.SystemCodeDetailsGetByCode("YesNoOptions"));
            LoadedRiskOptions.AddRange(App.Database.SystemCodeDetailsGetByCode("RiskTypeOptions"));
            LoadedServiceProvidedOptions.AddRange(App.Database.SystemCodeDetailsGetByCode("ChildProtectionServiceOptions"));
            LoadedServiceProviderOptions.AddRange(App.Database.SystemCodeDetailsGetByCode("ChildProtectionServiceProviderOptions"));

            ExistingISNPCasesForFollowUp.AddRange(GetExistingISNPCasesForFollowUp(id.ToString()));
            PendingISNPCasesFollowUpReadyForSync.AddRange(GetPendingISNPCasesFollowUpReadyForSync(id.ToString()));

            AppSettings = GetAppSettings();
        }

        private string childName;
        public string ChildName
        {
            get { return childName; }
            set { SetProperty(ref childName, value); }
        }

        private Guid childProtectionVisitHeaderId;
        public Guid ChildProtectionVisitHeaderId
        {
            get { return childProtectionVisitHeaderId; }
            set { SetProperty(ref childProtectionVisitHeaderId, value); }
        }

        private Guid childProtectionHeaderId;
        public Guid ChildProtectionHeaderId
        {
            get { return childProtectionHeaderId; }
            set { SetProperty(ref childProtectionHeaderId, value); }
        }

        private Guid childDetailId;
        public Guid ChildDetailID
        {
            get { return childDetailId; }
            set { SetProperty(ref childDetailId, value); }
        }

        private string clientTypeId;
        public string ClientTypeId
        {
            get { return clientTypeId; }
            set { SetProperty(ref clientTypeId, value); }
        }

        private string childProtectionId;
        public string ChildProtectionId
        {
            get { return childProtectionId; }
            set { SetProperty(ref childProtectionId, value); }
        }

        private string refferedToCaseManagement;
        public string RefferedToCaseManagement
        {
            get { return refferedToCaseManagement; }
            set { SetProperty(ref refferedToCaseManagement, value); }
        }

        private DateTime collectionDate;
        public DateTime CollectionDate
        {
            get { return collectionDate; }
            set { SetProperty(ref collectionDate, value); }
        }

        private string socialWorker;
        public string SocialWorker
        {
            get { return socialWorker; }
            set { SetProperty(ref socialWorker, value); }
        }

        private string remarks;
        public string Remarks
        {
            get { return remarks; }
            set { SetProperty(ref remarks, value); }
        }

        private string risk;
        public string Risk
        {
            get { return risk; }
            set { SetProperty(ref risk, value); }
        }
        private string riskName;
        public string RiskName
        {
            get { return riskName; }
            set { SetProperty(ref riskName, value); }
        }

        private string serviceProvided;
        public string ServiceProvided
        {
            get { return serviceProvided; }
            set { SetProperty(ref serviceProvided, value); }
        }

        private string serviceProvidedName;
        public string ServiceProvidedName
        {
            get { return serviceProvidedName; }
            set { SetProperty(ref serviceProvidedName, value); }
        }

        private string serviceProvider;
        public string ServiceProvider
        {
            get { return serviceProvider; }
            set { SetProperty(ref serviceProvider, value); }
        }

        private string serviceProviderName;
        public string ServiceProviderName
        {
            get { return serviceProviderName; }
            set { SetProperty(ref serviceProviderName, value); }
        }

        private string message;
        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private string messageSync;
        public string MessageSync
        {
            get { return messageSync; }
            set { SetProperty(ref messageSync, value); }
        }

        private string header;
        public string Header
        {
            get { return header; }
            set { SetProperty(ref header, value); }
        }


        private string saveAllButton;
        public string SaveAllButton
        {
            get { return saveAllButton; }
            set { SetProperty(ref saveAllButton, value); }
        }

        private string addNewButton;
        public string AddNewButton
        {
            get { return addNewButton; }
            set { SetProperty(ref addNewButton, value); }
        }

        private string tab1Header;
        public string Tab1Header
        {
            get { return tab1Header; }
            set { SetProperty(ref tab1Header, value); }
        }

        private string tab1Title;
        public string Tab1Title
        {
            get { return tab1Title; }
            set { SetProperty(ref tab1Title, value); }
        }

        private string tab2Title;
        public string Tab2Title
        {
            get { return tab2Title; }
            set { SetProperty(ref tab2Title, value); }
        }

        private string tab3Title;
        public string Tab3Title
        {
            get { return tab3Title; }
            set { SetProperty(ref tab3Title, value); }
        }

        private string tab4Title;
        public string Tab4Title
        {
            get { return tab4Title; }
            set { SetProperty(ref tab4Title, value); }
        }

        private string memberNameLabel;
        public string MemberNameLabel
        {
            get { return memberNameLabel; }
            set { SetProperty(ref memberNameLabel, value); }
        }

        private string childProtectionIdLabel;
        public string ChildProtectionIdLabel
        {
            get { return childProtectionIdLabel; }
            set { SetProperty(ref childProtectionIdLabel, value); }
        }

        private string refferedToCaseManagementLabel;
        public string RefferedToCaseManagementLabel
        {
            get { return refferedToCaseManagementLabel; }
            set { SetProperty(ref refferedToCaseManagementLabel, value); }
        }

        private string remarksLabel;
        public string RemarksLabel
        {
            get { return remarksLabel; }
            set { SetProperty(ref remarksLabel, value); }
        }

        private string riskLabel;
        public string RiskLabel
        {
            get { return riskLabel; }
            set { SetProperty(ref riskLabel, value); }
        }

        private string serviceProvidedLabel;
        public string ServiceProvidedLabel
        {
            get { return serviceProvidedLabel; }
            set { SetProperty(ref serviceProvidedLabel, value); }
        }

        private string serviceProviderLabel;
        public string ServiceProviderLabel
        {
            get { return serviceProviderLabel; }
            set { SetProperty(ref serviceProviderLabel, value); }
        }

        private int kebeleId;
        public int KebeleId
        {
            get { return kebeleId; }
            set { SetProperty(ref kebeleId, value); }
        }

        private int serviceId;
        public int ServiceId
        {
            get { return serviceId; }
            set { SetProperty(ref serviceId, value); }
        }

        private string household;
        public string Household
        {
            get { return household; }
            set { SetProperty(ref household, value); }
        }

        private bool error;
        public bool Error
        {
            get { return error; }
            set { SetProperty(ref error, value); }
        }

        public string GenerateId()
        {
            string Id = null;
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            Id = new string(Enumerable.Repeat(chars, 5)
              .Select(s => s[random.Next(s.Length)]).ToArray());

            return Id;
        }

        public ICommand SaveComplianceCommand => _saveComplianceCommand ?? (_saveComplianceCommand = new Command(async () => await ExecuteSaveCompliance()));
        private async Task ExecuteSaveCompliance()
        {
            try
            {
                IsBusy = true;
                Message = "Validating .. ";
                var errorMessage = "";
                var complianceHeader = new ComplianceCPCaseHeader();
                var data = App.Database.GetTableRows("ComplianceCPCaseDetail", "ChildProtectionHeaderID", ChildProtectionHeaderId);


                if(data.Count == 0)
                {
                    errorMessage += "Please Provide Atleast One Risk";
                }
                else
                {
                    complianceHeader.ClientTypeID = ClientTypeId;
                    complianceHeader.ChildProtectionId = ChildProtectionId;
                    complianceHeader.ChildProtectionHeaderID = ChildProtectionHeaderId;
                    complianceHeader.FiscalYear = AppSettings["FiscalYear"];
                    complianceHeader.SocialWorker = AppSettings["UserName"];
                    complianceHeader.CreatedBy = AppSettings["UserName"];
                    complianceHeader.CreatedOn = DateTime.Now;
                    complianceHeader.Status = "PENDING SYNC";

                    if(SelectedRefferedToCaseManagementOption != null){
                        if (SelectedRefferedToCaseManagementOption.Value == "0")
                        {
                            errorMessage += "Please Provide Reffered To CaseManagement";
                        }else{
                            complianceHeader.CaseManagementReferral = SelectedRefferedToCaseManagementOption.Value;
                        }
                    }

                    complianceHeader.ChildDetailID = ChildDetailID;
                    complianceHeader.Remarks = Remarks;
                    complianceHeader.CollectionDate = DateTime.Now;
                    complianceHeader.SocialWorker = Settings.UserName;
                }

                if (errorMessage == "")
                {
                    Message = "Updating Database .. ";

                    
                    App.Database.AddOrUpdate(complianceHeader);

                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Success",
                        Message = "Save successful.",
                        Cancel = "OK"
                    });

                    Error = false;
                }
                else
                {
                    Error = true;
                    if (errorMessage.Length > 0)
                    {
                        ValidateMessage = $"{ValidateMessage}\n{errorMessage}";
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(
                            MessageKeys.Error,
                            new MessagingServiceAlert
                            {
                                Title = "Please Check the Data and Try Again!!",
                                Message = ValidateMessage,
                                Cancel = "OK"
                            });
                        IsBusy = false;
                    }
                }



            }
            catch (Exception e)
            {
                Error = true;
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                           MessageKeys.Error,
                           new MessagingServiceAlert
                           {
                               Title = "Please Correct the Data and Try Again!!",
                               Message = e.Message,
                               Cancel = "OK"
                           });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            return;
        }

        public ICommand SaveRiskCommand => _saveRiskCommand ?? (_saveRiskCommand = new Command(async () => await ExecuteSaveRiskCommand()));
        public async Task ExecuteSaveRiskCommand()
        {
            try
            {
                IsBusy = true;
                Message = "Validating .. ";
                var errorMessage = "";
                var complianceDetail = new ComplianceCPCaseDetail();
                complianceDetail.ChildProtectionDetailID = Guid.NewGuid();
                complianceDetail.ChildProtectionHeaderID = ChildProtectionHeaderId;

                if (SelectedRiskOption == null) {
                    errorMessage += "Please Provide Risk Identified\n";
                } else {
                    complianceDetail.RiskId = SelectedRiskOption.Value;
                    complianceDetail.RiskName = SelectedRiskOption.Name;
                }


                if (SelectedServiceProvidedOption == null) {
                    errorMessage += "Please Provide Service Provided\n";
                } else {
                    complianceDetail.ServiceId = SelectedServiceProvidedOption.Value;
                    complianceDetail.ServiceName = SelectedServiceProvidedOption.Name;
                }


                if (SelectedServiceProviderOption == null) {
                    errorMessage += "Please Provide Service Provider\n";
                } else {
                    complianceDetail.ProviderId = SelectedServiceProviderOption.Value;
                    complianceDetail.ProviderName = SelectedServiceProviderOption.Name;
                }

                if (errorMessage == "")
                {
                    Message = "Updating Database .. ";

                    complianceDetail.ChildName = ChildName;
                    complianceDetail.Status = "PENDING SYNC";
                    App.Database.AddOrUpdate(complianceDetail);

                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Success",
                        Message = "Risk Added!",
                        Cancel = "OK"
                    });

                    Error = false;
                }
                else
                {
                    Error = true;

                    ValidateMessage = $"{errorMessage}";
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(
                        MessageKeys.Error,
                        new MessagingServiceAlert
                        {
                            Title = "Please Check the Data and Try Again!!",
                            Message = ValidateMessage,
                            Cancel = "OK"
                        });
                    IsBusy = false;
                }
            }
            catch (Exception e)
            {
                Error = true;

                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                           MessageKeys.Error,
                           new MessagingServiceAlert
                           {
                               Title = "Please Correct the Data and Try Again!!",
                               Message = e.Message,
                               Cancel = "OK"
                           });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            return;
        }

        public ObservableCollection<ComplianceCPCaseDetail> GetPendingISNPRisksReadyForSync(string id)
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetISNPTableRowsPendingSyncRows<ComplianceCPCaseDetail>("ComplianceCPCaseDetail");
                var hh = new ObservableCollection<ComplianceCPCaseDetail>();
                foreach (var item in items)
                {
                    var hhitem = (ComplianceCPCaseDetail)item;
                    hh.Add(hhitem);
                }
                return hh;
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Risks",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }
        public ObservableRangeCollection<ComplianceCPCaseDetail> PendingISNPRisksReadyForSync { get; } = new ObservableRangeCollection<ComplianceCPCaseDetail>();
        private ComplianceCPCaseDetail selectedComplianceCPCaseDetailCommand;
        public ComplianceCPCaseDetail SelectedComplianceCPCaseDetailCommand
        {
            get { return selectedComplianceCPCaseDetailCommand; }
            set
            {
                selectedComplianceCPCaseDetailCommand = value;
                OnPropertyChanged();
                if (selectedComplianceCPCaseDetailCommand == null)
                    return;
                Navigation.PushAsync(new ComplianceCaptureForm4DPage(selectedComplianceCPCaseDetailCommand.ChildProtectionHeaderID.ToString(), true));

                selectedComplianceCPCaseDetailCommand = null;
            }
        }

        public ObservableCollection<ComplianceCPCase> GetExistingISNPCasesForFollowUp(string id)
        {

            IsBusy = true;
            try
            {
                var items = App.Database.GetTableRows("ComplianceCPCase", "ChildDetailID", id);
                var hh = new ObservableCollection<ComplianceCPCase>();
                foreach (var item in items)
                {
                    var hhitem = (ComplianceCPCase)item;
                    hh.Add(hhitem);
                }
                return hh;
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Risks",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }
        public ObservableRangeCollection<ComplianceCPCase> ExistingISNPCasesForFollowUp { get; } = new ObservableRangeCollection<ComplianceCPCase>();
        private ComplianceCPCase selectedComplianceCPCaseCommand;
        public ComplianceCPCase SelectedComplianceCPCaseCommand
        {
            get { return selectedComplianceCPCaseCommand; }
            set
            {
                selectedComplianceCPCaseCommand = value;
                OnPropertyChanged();
                if (selectedComplianceCPCaseCommand == null)
                    return;
                Navigation.PushAsync(new ComplianceCaptureForm4DFollowUpPage(
                    selectedComplianceCPCaseCommand.ChildProtectionHeaderID.ToString(),
                    selectedComplianceCPCaseCommand.ChildProtectionDetailID.ToString(), 
                    selectedComplianceCPCaseCommand.ChildDetailID.ToString(),
                    selectedComplianceCPCaseCommand.RiskName,
                    selectedComplianceCPCaseCommand.ServiceName,
                    selectedComplianceCPCaseCommand.ProviderName));

                selectedComplianceCPCaseCommand = null;
            }
        }

        public ObservableRangeCollection<ComplianceCPCaseVisitDetail> PendingISNPCasesFollowUpReadyForSync { get; } = new ObservableRangeCollection<ComplianceCPCaseVisitDetail>();
        public ObservableCollection<ComplianceCPCaseVisitDetail> GetPendingISNPCasesFollowUpReadyForSync(string id)
        {

            IsBusy = true;
            try
            {
                var items = App.Database.GetTableRows("ComplianceCPCaseVisitDetail", "ChildDetailID", id);
                var hh = new ObservableCollection<ComplianceCPCaseVisitDetail>();
                foreach (var item in items)
                {
                    var hhitem = (ComplianceCPCaseVisitDetail)item;
                    hh.Add(hhitem);
                }
                return hh;
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting FollowUps",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }
        private ComplianceCPCase selectedISNPCaseFollowUpsReadyForSyncCommand;
        public ComplianceCPCase SelectedISNPCaseFollowUpsReadyForSyncCommand
        {
            get { return selectedComplianceCPCaseCommand; }
            set
            {
                selectedComplianceCPCaseCommand = value;
                OnPropertyChanged();
                if (selectedComplianceCPCaseCommand == null)
                    return;
                Navigation.PushAsync(new ComplianceCaptureForm4DFollowUpPage(
                    selectedISNPCaseFollowUpsReadyForSyncCommand.ChildProtectionHeaderID.ToString(),
                    selectedISNPCaseFollowUpsReadyForSyncCommand.ChildProtectionDetailID.ToString(),
                    selectedISNPCaseFollowUpsReadyForSyncCommand.ChildDetailID.ToString(),
                    selectedISNPCaseFollowUpsReadyForSyncCommand.RiskName,
                    selectedISNPCaseFollowUpsReadyForSyncCommand.ServiceName,
                    selectedISNPCaseFollowUpsReadyForSyncCommand.ProviderName));

                selectedComplianceCPCaseCommand = null;
            }
        }
    }
}
