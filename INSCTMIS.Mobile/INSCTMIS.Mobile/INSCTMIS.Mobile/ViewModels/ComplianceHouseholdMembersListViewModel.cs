﻿using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Interface;
using INSCTMIS.Mobile.Pages;
using MvvmHelpers;
using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    class ComplianceHouseholdMembersListViewModel : LocalBaseViewModel
    {
        private string Household;
        private IAppClient client;
        public INavigation Navigation;

        public ComplianceHouseholdMembersListViewModel(INavigation navigation, int kebeleid, int serviceid, string household, string id) : base(navigation)
        {

            Household = household;
            client = DependencyService.Get<IAppClient>();
            Navigation = navigation;
            ContentPageTitle = "Pending Upload";
            IsPDSHousehold = false;
            IsPLWHousehold = false;
            IsCMCHousehold = false;
            IsISNPHousehold = false;

            if (household.Equals("PDS"))
            {
                Title = "PDS Household Members";
                IsPDSHousehold = true;
                DownloadedTDSComplianceHouseholdMembers.AddRange(GetTDSHouseholdMembersReadyForCompliance(kebeleid, serviceid, household));
                PendingSyncTDSComplianceHouseholdMembers.AddRange(GetTDSHouseholdMembersReadyForSync(kebeleid, serviceid, household));
            }

            if (household.Equals("PLW"))
            {
                Title = "PLW Household Members";
                IsPLWHousehold = true;
                DownloadedPLWComplianceHouseholdMembers.AddRange(GetPLWHouseholdMembersReadyForCompliance(kebeleid, serviceid, household));
                PendingSyncPLWComplianceHouseholdMembers.AddRange(GetPLWHouseholdMembersReadyForSync(kebeleid, serviceid, household));
            }

            if (household.Equals("CMC"))
            {
                Title = "CMC Household Members";
                IsCMCHousehold = true;
                DownloadedCMCComplianceHouseholdMembers.AddRange(GetCMCHouseholdMembersReadyForCompliance(kebeleid, serviceid, household));
                PendingSyncCMCComplianceHouseholdMembers.AddRange(GetCMCHouseholdMembersReadyForSync(kebeleid, serviceid, household));
            }

            if (household.Equals("ISNP"))
            {
                Title = "ISNP Household Members";
                IsISNPHousehold = true;
                DownloadedISNPComplianceHouseholdMembers.AddRange(GetISNPHouseholdMembersReadyForCompliance(kebeleid, serviceid, household, id));
                //PendingSyncISNPComplianceHouseholdMembers.AddRange(GetISNPHouseholdMembersReadyForSync(kebeleid, serviceid, household));
            }

        }

        private string title;
        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }

        private string contentPageTitle;
        public string ContentPageTitle
        {
        get { return contentPageTitle; }
        set { SetProperty(ref contentPageTitle, value); }
        }

        private string message;
        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private string messageSync;

        public string MessageSync
        {
            get { return messageSync; }
            set { SetProperty(ref messageSync, value); }
        }

        private bool isPDSHousehold;
        public bool IsPDSHousehold
        {
            get { return isPDSHousehold; }
            set { SetProperty(ref isPDSHousehold, value); }
        }

        private bool isPLWHousehold;
        public bool IsPLWHousehold
        {
            get { return isPLWHousehold; }
            set { SetProperty(ref isPLWHousehold, value); }
        }

        private bool isCMCHousehold;
        public bool IsCMCHousehold
        {
            get { return isCMCHousehold; }
            set { SetProperty(ref isCMCHousehold, value); }
        }

        private bool isISNPHousehold;
        public bool IsISNPHousehold
        {
            get { return isISNPHousehold; }
            set { SetProperty(ref isISNPHousehold, value); }
        }

        public ObservableCollection<ComplianceTDSMembers> GetTDSHouseholdMembersReadyForCompliance(int kebeleid, int serviceid, string household)
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetTableRows("ComplianceTDSMembers", "KebeleID", kebeleid.ToString(), "ServiceID", serviceid.ToString(), "HasComplied", "");
                var hh = new ObservableCollection<ComplianceTDSMembers>();
                foreach (var item in items)
                {
                    var hhitem = (ComplianceTDSMembers)item;
                    hh.Add(hhitem);
                }
                return hh;
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Household Members",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }
        public ObservableCollection<ComplianceTDSMembers> GetTDSHouseholdMembersReadyForSync(int kebeleid, int serviceid, string household)
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetComplianceTableRowsPendingSyncRows<ComplianceTDSMembers>("ComplianceTDSMembers");
                var hh = new ObservableCollection<ComplianceTDSMembers>();
                foreach (var item in items)
                {
                    var hhitem = (ComplianceTDSMembers)item;
                    hh.Add(hhitem);
                }
                return hh;

            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Household Members",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }
        public ObservableRangeCollection<ComplianceTDSMembers> PendingSyncTDSComplianceHouseholdMembers { get; } = new ObservableRangeCollection<ComplianceTDSMembers>();
        public ObservableRangeCollection<ComplianceTDSMembers> DownloadedTDSComplianceHouseholdMembers { get; } = new ObservableRangeCollection<ComplianceTDSMembers>();
        private ComplianceTDSMembers selectedTDSHouseholdMemberCommand;
        public ComplianceTDSMembers SelectedTDSHouseholdMemberCommand
        {
            get { return selectedTDSHouseholdMemberCommand; }
            set
            {
                selectedTDSHouseholdMemberCommand = value;
                OnPropertyChanged();
                if (selectedTDSHouseholdMemberCommand == null)
                    return;
                Navigation.PushAsync(new ComplianceCaptureForm4ABCPage(selectedTDSHouseholdMemberCommand.KebeleID, selectedTDSHouseholdMemberCommand.ServiceID, Household, selectedTDSHouseholdMemberCommand.ProfileDSDetailID.ToString()));

                selectedTDSHouseholdMemberCommand = null;
            }
        }

        public ObservableCollection<ComplianceTDSPLWMembers> GetPLWHouseholdMembersReadyForCompliance(int kebeleid, int serviceid, string household)
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetTableRows("ComplianceTDSPLWMembers", "KebeleID", kebeleid.ToString(), "ServiceID", serviceid.ToString(), "HasComplied", "");
                var hh = new ObservableCollection<ComplianceTDSPLWMembers>();
                foreach (var item in items)
                {
                    var hhitem = (ComplianceTDSPLWMembers)item;
                    hh.Add(hhitem);
                }
                return hh;
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Household Members",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }
        public ObservableCollection<ComplianceTDSPLWMembers> GetPLWHouseholdMembersReadyForSync(int kebeleid, int serviceid, string household)
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetComplianceTableRowsPendingSyncRows<ComplianceTDSPLWMembers>("ComplianceTDSPLWMembers");
                var hh = new ObservableCollection<ComplianceTDSPLWMembers>();
                foreach (var item in items)
                {
                    var hhitem = (ComplianceTDSPLWMembers)item;
                    hh.Add(hhitem);
                }
                return hh;

            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Household Members",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }
        public ObservableRangeCollection<ComplianceTDSPLWMembers> PendingSyncPLWComplianceHouseholdMembers { get; } = new ObservableRangeCollection<ComplianceTDSPLWMembers>();
        public ObservableRangeCollection<ComplianceTDSPLWMembers> DownloadedPLWComplianceHouseholdMembers { get; } = new ObservableRangeCollection<ComplianceTDSPLWMembers>();
        private ComplianceTDSPLWMembers selectedPLWHouseholdMemberCommand;
        public ComplianceTDSPLWMembers SelectedPLWHouseholdMemberCommand
        {
            get { return selectedPLWHouseholdMemberCommand; }
            set
            {
                selectedPLWHouseholdMemberCommand = value;
                OnPropertyChanged();
                if (selectedPLWHouseholdMemberCommand == null)
                    return;
                Navigation.PushAsync(new ComplianceCaptureForm4ABCPage(selectedPLWHouseholdMemberCommand.KebeleID, selectedPLWHouseholdMemberCommand.ServiceID, Household, selectedPLWHouseholdMemberCommand.ProfileTDSPLWID.ToString()));

                selectedPLWHouseholdMemberCommand = null;
            }
        }

        public ObservableCollection<ComplianceTDSCMCMembers> GetCMCHouseholdMembersReadyForCompliance(int kebeleid, int serviceid, string household)
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetTableRows("ComplianceTDSCMCMembers", "KebeleID", kebeleid.ToString(), "ServiceID", serviceid.ToString(), "HasComplied", "");
                var hh = new ObservableCollection<ComplianceTDSCMCMembers>();
                foreach (var item in items)
                {
                    var hhitem = (ComplianceTDSCMCMembers)item;
                    hh.Add(hhitem);
                }
                return hh;
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Household Members",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }
        public ObservableCollection<ComplianceTDSCMCMembers> GetCMCHouseholdMembersReadyForSync(int kebeleid, int serviceid, string household)
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetComplianceTableRowsPendingSyncRows<ComplianceTDSCMCMembers>("ComplianceTDSCMCMembers");
                var hh = new ObservableCollection<ComplianceTDSCMCMembers>();
                foreach (var item in items)
                {
                    var hhitem = (ComplianceTDSCMCMembers)item;
                    hh.Add(hhitem);
                }
                return hh;

            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Household Members",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }
        public ObservableRangeCollection<ComplianceTDSCMCMembers> PendingSyncCMCComplianceHouseholdMembers { get; } = new ObservableRangeCollection<ComplianceTDSCMCMembers>();
        public ObservableRangeCollection<ComplianceTDSCMCMembers> DownloadedCMCComplianceHouseholdMembers { get; } = new ObservableRangeCollection<ComplianceTDSCMCMembers>();
        private ComplianceTDSCMCMembers selectedCMCHouseholdMemberCommand;
        public ComplianceTDSCMCMembers SelectedCMCHouseholdMemberCommand
        {
            get { return selectedCMCHouseholdMemberCommand; }
            set
            {
                selectedCMCHouseholdMemberCommand = value;
                OnPropertyChanged();
                if (selectedCMCHouseholdMemberCommand == null)
                    return;
                Navigation.PushAsync(new ComplianceCaptureForm4ABCPage(selectedCMCHouseholdMemberCommand.KebeleID, selectedCMCHouseholdMemberCommand.ServiceID, Household, selectedCMCHouseholdMemberCommand.ProfileTDSCMCID.ToString()));

                selectedCMCHouseholdMemberCommand = null;
            }
        }

        public ObservableCollection<ComplianceCPHouseholdMembers> GetISNPHouseholdMembersReadyForCompliance(int kebeleid, int serviceid, string household, string id)
        {
            IsBusy = true;
            try
            {
                Guid _id = new Guid(id);
                var items = App.Database.GetTableRows("ComplianceCPHouseholdMembers", "ProfileDSHeaderID", _id);
                var hh = new ObservableCollection<ComplianceCPHouseholdMembers>();
                foreach (var item in items)
                {
                    var hhitem = (ComplianceCPHouseholdMembers)item;
                    hh.Add(hhitem);
                }
                return hh;
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Household Members",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }
        public ObservableCollection<ComplianceCPHouseholdMembers> GetISNPHouseholdMembersReadyForSync(int kebeleid, int serviceid, string household)
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetTableRows("ComplianceCPHouseholdMembers", "KebeleID", kebeleid.ToString());
                var hh = new ObservableCollection<ComplianceCPHouseholdMembers>();
                foreach (var item in items)
                {
                    var hhitem = (ComplianceCPHouseholdMembers)item;
                    hh.Add(hhitem);
                }
                return hh;
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Household Members",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }
        public ObservableRangeCollection<ComplianceCPHouseholdMembers> PendingSyncISNPComplianceHouseholdMembers { get; } = new ObservableRangeCollection<ComplianceCPHouseholdMembers>();
        public ObservableRangeCollection<ComplianceCPHouseholdMembers> DownloadedISNPComplianceHouseholdMembers { get; } = new ObservableRangeCollection<ComplianceCPHouseholdMembers>();
        private ComplianceCPHouseholdMembers selectedISNPHouseholdMemberCommand;
        public ComplianceCPHouseholdMembers SelectedISNPHouseholdMemberCommand
        {
            get { return selectedISNPHouseholdMemberCommand; }
            set
            {
                selectedISNPHouseholdMemberCommand = value;
                OnPropertyChanged();
                if (selectedISNPHouseholdMemberCommand == null)
                    return;
                Navigation.PushAsync(new ComplianceCaptureForm4DPage(selectedISNPHouseholdMemberCommand.ProfileDSDetailID.ToString(), false));

                selectedISNPHouseholdMemberCommand = null;
            }
        }
    }
}
