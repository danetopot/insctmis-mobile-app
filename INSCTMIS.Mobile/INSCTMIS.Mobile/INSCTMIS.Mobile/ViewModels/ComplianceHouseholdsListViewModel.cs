﻿using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Interface;
using INSCTMIS.Mobile.Pages;
using MvvmHelpers;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    class ComplianceHouseholdsListViewModel : LocalBaseViewModel
    {
        private IAppClient client;
        public INavigation Navigation;

        public ComplianceHouseholdsListViewModel(INavigation navigation) : base(navigation)
        {
            client = DependencyService.Get<IAppClient>();
            Navigation = navigation;

            DownloadedTDSComplianceHouseholds.ReplaceRange(GetTDSHouseholdsReadyForCompliance());
            DownloadedPLWComplianceHouseholds.ReplaceRange(GetPLWHouseholdsReadyForCompliance());
            DownloadedCMCComplianceHouseholds.ReplaceRange(GetCMCHouseholdsReadyForCompliance());
            DownloadedISNPComplianceHouseholds.ReplaceRange(GetISNPHouseholdsReadyForCompliance());
        }

        private string message;
        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private string messageSync;

        public string MessageSync
        {
            get { return messageSync; }
            set { SetProperty(ref messageSync, value); }
        }

        public ObservableCollection<ComplianceTDS> GetTDSHouseholdsReadyForCompliance()
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetTable("ComplianceTDS");
                var hh = new ObservableCollection<ComplianceTDS>();
                foreach (var item in items)
                {
                    var hhitem = (ComplianceTDS)item;
                    hh.Add(hhitem);
                }
                return hh;

            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Households",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }

        public ObservableCollection<ComplianceTDSPLW> GetPLWHouseholdsReadyForCompliance()
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetTable("ComplianceTDSPLW");
                var hh = new ObservableCollection<ComplianceTDSPLW>();
                foreach (var item in items)
                {
                    var hhitem = (ComplianceTDSPLW)item;
                    hh.Add(hhitem);
                }
                return hh;

            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Households",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }

        public ObservableCollection<ComplianceTDSCMC> GetCMCHouseholdsReadyForCompliance()
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetTable("ComplianceTDSCMC");
                var hh = new ObservableCollection<ComplianceTDSCMC>();
                foreach (var item in items)
                {
                    var hhitem = (ComplianceTDSCMC)item;
                    hh.Add(hhitem);
                }
                return hh;

            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Households",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }

        public ObservableCollection<ComplianceCPHousehold> GetISNPHouseholdsReadyForCompliance()
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetTable("ComplianceCPHousehold");
                var hh = new ObservableCollection<ComplianceCPHousehold>();
                foreach (var item in items)
                {
                    var hhitem = (ComplianceCPHousehold)item;
                    hh.Add(hhitem);
                }
                return hh;

            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Households",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }

        public ObservableRangeCollection<ComplianceTDS> DownloadedTDSComplianceHouseholds { get; } = new ObservableRangeCollection<ComplianceTDS>();
        private ComplianceTDS selectedTDSHouseholdCommand;
        public ComplianceTDS SelectedTDSHouseholdCommand
        {
            get { return selectedTDSHouseholdCommand; }
            set
            {
                selectedTDSHouseholdCommand = value;
                OnPropertyChanged();
                if (selectedTDSHouseholdCommand == null)
                    return;
                Navigation.PushAsync(new ComplianceHouseholdMembersListPage(selectedTDSHouseholdCommand.KebeleID, selectedTDSHouseholdCommand.ServiceID, "PDS", null));

                selectedTDSHouseholdCommand = null;
            }
        }

        public ObservableRangeCollection<ComplianceTDSPLW> DownloadedPLWComplianceHouseholds { get; } = new ObservableRangeCollection<ComplianceTDSPLW>();
        private ComplianceTDSPLW selectedPLWHouseholdCommand;
        public ComplianceTDSPLW SelectedPLWHouseholdCommand
        {
            get { return selectedPLWHouseholdCommand; }
            set
            {
                selectedPLWHouseholdCommand = value;
                OnPropertyChanged();
                if (selectedPLWHouseholdCommand == null)
                    return;
                Navigation.PushAsync(new ComplianceHouseholdMembersListPage(selectedPLWHouseholdCommand.KebeleID, selectedPLWHouseholdCommand.ServiceID, "PLW", null));

                selectedPLWHouseholdCommand = null;
            }
        }

        public ObservableRangeCollection<ComplianceTDSCMC> DownloadedCMCComplianceHouseholds { get; } = new ObservableRangeCollection<ComplianceTDSCMC>();
        private ComplianceTDSCMC selectedCMCHouseholdCommand;
        public ComplianceTDSCMC SelectedCMCHouseholdCommand
        {
            get { return selectedCMCHouseholdCommand; }
            set
            {
                selectedCMCHouseholdCommand = value;
                OnPropertyChanged();
                if (selectedCMCHouseholdCommand == null)
                    return;
                Navigation.PushAsync(new ComplianceHouseholdMembersListPage(selectedCMCHouseholdCommand.KebeleID, selectedCMCHouseholdCommand.ServiceID, "CMC", null));

                selectedCMCHouseholdCommand = null;
            }
        }


        public ObservableRangeCollection<ComplianceCPHousehold> DownloadedISNPComplianceHouseholds { get; } = new ObservableRangeCollection<ComplianceCPHousehold>();
        private ComplianceCPHousehold selectedISNPHouseholdCommand;
        public ComplianceCPHousehold SelectedISNPHouseholdCommand
        {
            get { return selectedISNPHouseholdCommand; }
            set
            {
                selectedISNPHouseholdCommand = value;
                OnPropertyChanged();
                if (selectedISNPHouseholdCommand == null)
                    return;
                Navigation.PushAsync(new ComplianceHouseholdMembersListPage(selectedISNPHouseholdCommand.KebeleID, selectedISNPHouseholdCommand.KebeleID, "ISNP", selectedISNPHouseholdCommand.ProfileDSHeaderID.ToString()));

                selectedISNPHouseholdCommand = null;
            }
        }




    }
}
