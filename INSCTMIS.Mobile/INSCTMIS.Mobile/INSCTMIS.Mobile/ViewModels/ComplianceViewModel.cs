﻿using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Interface;
using INSCTMIS.Mobile.Pages;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    class ComplianceViewModel : LocalBaseViewModel
    {
        private IAppClient client;
        public INavigation Navigation;

        public ComplianceViewModel(INavigation navigation) : base(navigation)
        {
            client = DependencyService.Get<IAppClient>();
            Navigation = navigation;
            LoadComplianceHouseholdText = "Load Compliance Households";
        }

        private string message;
        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private string messageSync;
        public string MessageSync
        {
            get { return messageSync; }
            set { SetProperty(ref messageSync, value); }
        }

        private string loadComplianceHouseholdText;
        public string LoadComplianceHouseholdText
        {
            get { return loadComplianceHouseholdText; }
            set { SetProperty(ref loadComplianceHouseholdText, value); }
        }

        private ICommand loadComplianceHouseholdCommand;
        public ICommand LoadComplianceHouseholdCommand => loadComplianceHouseholdCommand ?? (loadComplianceHouseholdCommand = new Command(async () => await ExecuteLoadComplianceHouseholdAsync()));
        private async Task ExecuteLoadComplianceHouseholdAsync()
        {
            if (IsBusy)
            {
                return;
            }
            IsBusy = true;
            MessageSync = "Populating Tables... ...";
            LoadComplianceHouseholdText = "Loading Compliance Households . . .";

            try
            {
                await Navigation.PushAsync(new ComplianceHouseholdsListPage());

                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Success",
                    Message = "Compliance households loaded successfully",
                    Cancel = "OK"
                });
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error",
                    Message = "" + ex.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageSync = string.Empty;
                LoadComplianceHouseholdText = "Load Compliance Households";
                IsBusy = false;
                await Task.FromResult(0);
            }

        }
        
    }
}
