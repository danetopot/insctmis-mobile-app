﻿using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Interface;
using INSCTMIS.Mobile.Pages;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    class HouseholdProfileViewModel : LocalBaseViewModel
    {
        private IAppClient client;
        public INavigation Navigation;

        private string message;
        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        public HouseholdProfileViewModel(INavigation navigation) : base(navigation)
        {
            client = DependencyService.Get<IAppClient>();

            Form1AText = "Capture Form 1A";
            Form1BText = "Capture Form 1B";
            Form1CText = "Capture Form 1C";
            Navigation = navigation;
        }

        private string form1AText;
        public string Form1AText
        {
            get { return form1AText; }
            set { SetProperty(ref form1AText, value); }
        }

        private string form1BText;
        public string Form1BText
        {
            get { return form1BText; }
            set { SetProperty(ref form1BText, value); }
        }

        private string form1CText;
        public string Form1CText
        {
            get { return form1CText; }
            set { SetProperty(ref form1CText, value); }
        }


        private ICommand captureForm1ACommand;
        public ICommand CaptureForm1ACommand => captureForm1ACommand ?? (captureForm1ACommand = new Command(async () => await ExecuteCaptureForm1AAsync()));
        private async Task ExecuteCaptureForm1AAsync()
        {
            if (IsBusy)
            {
                return;
            }

            try
            {
                await Navigation.PushAsync(new RegisteredHouseholdListPage());
            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Please  Try Again!!",
                        Message = e.Message,
                        Cancel = "OK"
                    });
                return;
            }

        }

        private ICommand captureForm1BCommand;
        public ICommand CaptureForm1BCommand => captureForm1BCommand ?? (captureForm1ACommand = new Command(async () => await ExecuteCaptureForm1BAsync()));
        private async Task ExecuteCaptureForm1BAsync()
        {
            if (IsBusy)
            {
                return;
            }
            try
            {
                await Navigation.PushAsync(new RegisteredPLWListPage());
            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Please  Try Again!!",
                        Message = e.Message,
                        Cancel = "OK"
                    });
                return;
            }

        }

        private ICommand captureForm1CCommand;
        public ICommand CaptureForm1CCommand => captureForm1CCommand ?? (captureForm1CCommand = new Command(async () => await ExecuteCaptureForm1CAsync()));
        private async Task ExecuteCaptureForm1CAsync()
        {
            if (IsBusy)
            {
                return;
            }
            try
            {
                await Navigation.PushAsync(new RegisteredCaretakersListPage());
            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Please  Try Again!!",
                        Message = e.Message,
                        Cancel = "OK"
                    });
                return;
            }

        }
    }
}
