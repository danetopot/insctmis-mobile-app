﻿using System;
using System.Linq;
using Xamarin.Forms;
using INSCTMIS.Mobile.Models;
using INSCTMIS.Mobile.Services;
using MvvmHelpers;
using INSCTMIS.Mobile.Interface;
using Plugin.DeviceInfo;
using System.Windows.Input;
using System.Threading.Tasks;
using INSCTMIS.Mobile.Helpers;
using FluentValidation.Results;
using Plugin.Share;
using Plugin.Share.Abstractions;
using INSCTMIS.Mobile.Database;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace INSCTMIS.Mobile.ViewModels
{
    public class LocalBaseViewModel : BaseViewModel
    {
        protected INavigation Navigation { get; }

        protected IToast Toast { get; } = DependencyService.Get<IToast>();

        public LocalBaseViewModel(INavigation navigation = null)
        {
            Navigation = navigation;

            AppVersion = $"Software Build {CrossDeviceInfo.Current.AppBuild} - Version. {CrossDeviceInfo.Current.AppVersion}. ";

            DisplayName = $"{Settings.Current.UserName} ";
        }

        private string _validateMessage;

        public string ValidateMessage
        {
            get
            {
                return _validateMessage;
            }
            set
            {
                SetProperty(ref this._validateMessage, value, "ValidateMessage");
            }
        }

        private string _appVersion;

        public string AppVersion
        {
            get
            {
                return _appVersion;
            }
            set
            {
                SetProperty(ref this._appVersion, value, "AppVersion");
            }
        }

        private string _displayName;

        public string DisplayName
        {
            get
            {
                return _displayName;
            }
            set
            {
                SetProperty(ref this._displayName, value, "DisplayName");
            }
        }

        protected string GetErrorListFromValidationResult(ValidationResult validationResult)
        {
            var errorList = validationResult.Errors.Select(x => x.ErrorMessage).ToList();
            return String.Join("\n", errorList.ToArray()); ;
        }

        public virtual void Subscribe()
        {
        }

        public virtual void OnAppearing()
        {
        }

        public virtual void OnDisappearing()
        {
        }

        public virtual void OnBackButtonPressed()
        {
        }

        public virtual void OnPopped(Page page)
        {
        }

        public string filter = string.Empty;

        public string Filter
        {
            get { return filter; }
            set { SetProperty(ref filter, value); }

        }


        public string message = string.Empty;
        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        public Settings Settings
        {
            get
            {
                return Settings.Current;
            }
        }

        public static void Init(bool mock = true)
        {
            DependencyService.Register<IAppClient, AppClient>();
        }

        public string kebeleId;
        public string KebeleId
        {
            get { return kebeleId; }
            set { SetProperty(ref kebeleId, value); }
        }

        public string reportingPeriodId;
        public string ReportingPeriodId
        {
            get { return reportingPeriodId; }
            set { SetProperty(ref reportingPeriodId, value); }
        }

        public string fiscalYearId;
        public string FiscalYearId
        {
            get { return fiscalYearId; }
            set { SetProperty(ref fiscalYearId, value); }
        }

        public string serverURL;
        public string ServerURL
        {
            get { return serverURL; }
            set { SetProperty(ref serverURL, value); }
        }

        public Dictionary<string, string> GetAppSettings()
        {
            Dictionary<string, string> appSettings = new Dictionary<string, string>();
            appSettings.Add("Location", null);
            appSettings.Add("ReportingPeriod", null);
            appSettings.Add("FiscalYear", null);
            appSettings.Add("Server", null);

            var items = App.Database.GetTable("Setting");
            if (items.Count > 0)
            {
                var hh = new ObservableCollection<Setting>();
                foreach (var item in items)
                {
                    var hhitem = (Setting)item;
                    if (hhitem.Name == "UserId") { appSettings.Remove("UserId"); appSettings.Add("UserId", hhitem.Value); }
                    if (hhitem.Name == "UserRoleId") { appSettings.Remove("UserRoleId"); appSettings.Add("UserRoleId", hhitem.Value); }
                    if (hhitem.Name == "UserName") { appSettings.Remove("UserName"); appSettings.Add("UserName", hhitem.Value); }
                    if (hhitem.Name == "Location") { appSettings.Remove("Location");  appSettings.Add("Location", hhitem.Value); }
                    if (hhitem.Name == "ReportingPeriod") { appSettings.Remove("ReportingPeriod"); ;  appSettings.Add("ReportingPeriod", hhitem.Value); }
                    if (hhitem.Name == "FiscalYear") { appSettings.Remove("FiscalYear"); ;  appSettings.Add("FiscalYear", hhitem.Value); }
                    if (hhitem.Name == "Server") { appSettings.Remove("Server"); ;  appSettings.Add("Server", hhitem.Value); }
                }

            }

            return appSettings;
        }
    }
}
