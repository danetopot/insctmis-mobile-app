﻿using FluentValidation;
using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Helpers;
using INSCTMIS.Mobile.Interface;
using INSCTMIS.Mobile.Models;
using INSCTMIS.Mobile.Pages;
using INSCTMIS.Mobile.Validators;
using Plugin.Connectivity;
using Plugin.Share;
using Plugin.Share.Abstractions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    public class LoginViewModel : LocalBaseViewModel
    {
        private IAppClient client;
        private Setting settings;

        public LoginViewModel(INavigation navigation) : base(navigation)
        {
            client = DependencyService.Get<IAppClient>();
            
            email = Settings.Current.Email;
            username = Settings.Current.UserName;
            password = Settings.Current.Password;
            _validator = new LoginValidator();
            
        }

        private readonly IValidator _validator;

        private string message;

        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private string username;
        public string Username
        {
            get { return username; }
            set { SetProperty(ref username, value); }
        }

        private string password;
        public string Password
        {
            get { return password; }
            set { SetProperty(ref password, value); }
        }

        private string email;
        public string Email
        {
            get { return email; }
            set { SetProperty(ref email, value); }
        }



        private ICommand logoutCommand;

        public ICommand LogOutCommand =>
            logoutCommand ?? (logoutCommand = new Command(async () => await ExecuteLogoutAsync()));

        private async Task ExecuteLogoutAsync()
        {
            if (IsBusy)
            {
                return;
            }

            try
            {
                IsBusy = true;
                Message = "Establishing secure connection with the Main server ... ...";
                
                Settings.LoggedIn = false;
                Settings.FirstRun = true;

                await Navigation.PopToRootAsync();
                await Navigation.PushAsync(new LoginPage());
                await Navigation.PushModalAsync(new LoginPage());
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Log Out",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;
        }

        private ICommand loginCommand;

        public ICommand LoginCommand =>
            loginCommand ?? (loginCommand = new Command(async () => await ExecuteLoginAsync()));

        private async Task ExecuteLoginAsync()
        {
            if (IsBusy)
            {
                return;
            }

            var validationResult = _validator.Validate(this);

            if (!validationResult.IsValid)
            {
                ValidateMessage = GetErrorListFromValidationResult(validationResult);
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Sign In Error",
                    Message = ValidateMessage,
                    Cancel = "OK"
                });
            }

            if (string.IsNullOrWhiteSpace(username))
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Sign in Information",
                    Message = "We do need your username :-)",
                    Cancel = "OK"
                });
                return;
            }

            if (string.IsNullOrWhiteSpace(password))
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Sign in Information",
                    Message = "Password is empty!",
                    Cancel = "OK"
                });
                return;
            }
            try
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Login Failed",
                        Message = "It looks like you are offline. \n" +
                                  "Please check your internet connection and try again.",
                        Cancel = "OK"
                    });
                    return;
                }

                IsBusy = true;
                Message = "Establishing secure connection with the Main server ... ...";
                
                AccountResponse accountResponse = null;
                ListingOptionsResponse optionsResponse = null;
                accountResponse = await client.LoginSocialWorker(username, password);
                bool IsAuthenticated = true? accountResponse.IsAuthenticated.Equals("1") : false;

                if (IsAuthenticated)
                {
                    Message = "Secure Connection Established...";

                    try
                    {
                        if (IsAuthenticated)
                        {
                            optionsResponse = await client.GetListingSettings(accountResponse);

                            if (optionsResponse == null)
                            {
                                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                                {
                                    Title = "Unable to Sign in. Check your Username and Password",
                                    Message = optionsResponse.Error,
                                    Cancel = "OK"
                                });
                                return;
                            }
                            else
                            {

                                App.Database.AddOrUpdate(optionsResponse.SocialWorker);

                                foreach (var item in optionsResponse.Region)
                                {
                                    App.Database.AddOrUpdate(item);
                                }

                                foreach (var item in optionsResponse.Woreda)
                                {
                                    App.Database.AddOrUpdate(item);
                                }

                                foreach (var item in optionsResponse.Kebele)
                                {
                                    App.Database.AddOrUpdate(item);
                                }

                                foreach (var item in optionsResponse.ServiceProvider)
                                {
                                    App.Database.AddOrUpdate(item);
                                }

                                foreach (var item in optionsResponse.IntegratedService)
                                {
                                    App.Database.AddOrUpdate(item);
                                }

                                foreach (var item in optionsResponse.MonitoringService)
                                {
                                    App.Database.AddOrUpdate(item);
                                }

                                foreach (var item in optionsResponse.SystemCodeDetail)
                                {
                                    App.Database.AddOrUpdate(item);
                                }

                                Settings.UserId = optionsResponse.SocialWorker.UserId;
                                Settings.UserName = optionsResponse.SocialWorker.Username;
                                Settings.UserRoleId = optionsResponse.SocialWorker.RoleId;
                                Settings.Current.LastSyncDown = DateTime.UtcNow;
                                Settings.Current.HasSyncedDataDownwards = true;
                                Settings.FirstRun = false;

                                List<Setting> usersettings = new List<Setting>
                                {
                                   new Setting { ID = Guid.NewGuid(), Name = "UserId", Value= (optionsResponse.SocialWorker.UserId).ToString() },
                                   new Setting { ID = Guid.NewGuid(), Name = "UserRoleId", Value= (optionsResponse.SocialWorker.RoleId).ToString() },
                                   new Setting { ID = Guid.NewGuid(), Name = "UserName", Value= (optionsResponse.SocialWorker.Username).ToString() }
                                };
                                
                                foreach(var usersetting in usersettings)
                                {
                                    App.Database.AddOrUpdate(usersetting);
                                }

                                IsBusy = false;
                                await Finish();

                            }

                        }
                        else
                        {
                            MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                            {
                                Title = "Unable to Sign in. Check your Username and Password",
                                Message = optionsResponse.Error,
                                Cancel = "OK"
                            });
                            return;
                        }

                        
                    }
                    catch (Exception ex)
                    {
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                        {
                            Title = "Unable to Sign in.  ",
                            Message = ex.Message,
                            Cancel = "OK"
                        });
                        IsBusy = false;
                        return;
                    }
                }
                else
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Unable to Sign in.",
                        Message = "Check your Username and Password",
                        Cancel = "OK"
                    });
                }
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Sign in",
                    Message = "The username or password provided is incorrect.\n" + ex.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
        }


        private async Task Finish()
        {
            App.GoToMainPage();
        }
    }
}