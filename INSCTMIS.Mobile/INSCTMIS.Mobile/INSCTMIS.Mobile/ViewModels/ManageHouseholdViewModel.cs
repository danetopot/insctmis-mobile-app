﻿using FluentValidation;
using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Interface;
using INSCTMIS.Mobile.Pages;
using INSCTMIS.Mobile.Validators;
using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    public class ManageHouseholdViewModel : LocalBaseViewModel
    {
        private IAppClient client;
        Dictionary<string, string> AppSettings = null;
        private readonly IValidator _validator;
        private DataStore db;

        private int regionID;
        public int RegionID
        {
            get { return regionID; }
            set { SetProperty(ref regionID, value); }
        }

        private int woredaID;
        public int WoredaID {
            get { return woredaID; }
            set { SetProperty(ref woredaID, value); }
        }

        public int kebeleID;
        public int KebeleID {
            get { return kebeleID; }
            set { SetProperty(ref kebeleID, value); }
        }

        public string gote;
        public string Gote {
            get { return gote; }
            set { SetProperty(ref gote, value); }
        }

        public string gare;
        public string Gare {
            get { return gare; }
            set { SetProperty(ref gare, value); }
        }

        public string householdIDNumber;
        public string HouseholdIDNumber {
            get { return householdIDNumber; }
            set { SetProperty(ref householdIDNumber, value); }
        }

        public string nameOfHouseHoldHead;
        public string NameOfHouseHoldHead {
            get { return nameOfHouseHoldHead; }
            set { SetProperty(ref nameOfHouseHoldHead, value); }
        }

        public string cbhiMembership;
        public string CBHIMembership
        {
            get { return cbhiMembership; }
            set { SetProperty(ref cbhiMembership, value); }
        }

        public string cbhiNumber;
        public string CBHINumber {
            get { return cbhiNumber; }
            set { SetProperty(ref cbhiNumber, value); }
        }

        public DateTime collectionDate;
        public DateTime CollectionDate {
            get { return collectionDate; }
            set { SetProperty(ref collectionDate, value); }
        }

        public string remarks;
        public string Remarks {
            get { return remarks; }
            set { SetProperty(ref remarks, value); }
        }

        public string cccCBSPCMember;
        public string CCCCBSPCMember {
            get { return cccCBSPCMember; }
            set { SetProperty(ref cccCBSPCMember, value); }
        }

        private bool isCBHIMembership;
        public bool IsCBHIMembership
        {
            get { return isCBHIMembership; }
            set { SetProperty(ref isCBHIMembership, value); }
        }

        private Region _selectedRegion;
        public ObservableRangeCollection<Region> RegionOptions = new ObservableRangeCollection<Region>();
        public ObservableRangeCollection<Region> LoadedRegionOptions
        {
            get => RegionOptions;
            set => SetProperty(ref RegionOptions, value);
        }
        public Region SelectedRegion
        {
            get => _selectedRegion;
            set
            {
                if (this._selectedRegion == value)
                {
                    return;
                }

                this._selectedRegion = value;

                this.OnPropertyChanged();
            }
        }

        private Woreda _selectedWoreda;
        public ObservableRangeCollection<Woreda> WoredaOptions = new ObservableRangeCollection<Woreda>();
        public ObservableRangeCollection<Woreda> LoadedWoredaOptions
        {
            get => WoredaOptions;
            set => SetProperty(ref WoredaOptions, value);
        }
        public Woreda SelectedWoreda
        {
            get => _selectedWoreda;
            set
            {
                if (this._selectedWoreda == value)
                {
                    return;
                }

                this._selectedWoreda = value;

                this.OnPropertyChanged();
            }
        }


        private Kebele _selectedKebele;
        public ObservableRangeCollection<Kebele> KebeleOptions = new ObservableRangeCollection<Kebele>();
        public ObservableRangeCollection<Kebele> LoadedKebeleOptions
        {
            get => KebeleOptions;
            set => SetProperty(ref KebeleOptions, value);
        }
        public Kebele SelectedKebele
        {
            get => _selectedKebele;
            set
            {
                if (this._selectedKebele == value)
                {
                    return;
                }

                this._selectedKebele = value;

                this.OnPropertyChanged();
            }
        }

        private SystemCodeDetail _selectedCBHIMembership;
        public ObservableRangeCollection<SystemCodeDetail> CBHIMembershipOptions = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> LoadedCBHIMembershipOptions
        {
            get => CBHIMembershipOptions;
            set => SetProperty(ref CBHIMembershipOptions, value);
        }
        public SystemCodeDetail SelectedCBHIMembership
        {
            get => _selectedCBHIMembership;
            set
            {
                if (this._selectedCBHIMembership == value)
                {
                    return;
                }

                this._selectedCBHIMembership = value;

                if (this._selectedCBHIMembership.Value.Equals("YES"))
                {
                    IsCBHIMembership = true;
                }
                else
                {

                    IsCBHIMembership = false;
                }

                this.OnPropertyChanged();
            }
        }

        private SocialWorker _selectedSocialWorker;
        public ObservableRangeCollection<SocialWorker> SocialWorkerOptions = new ObservableRangeCollection<SocialWorker>();
        public ObservableRangeCollection<SocialWorker> LoadedSocialWorkerOptions
        {
            get => SocialWorkerOptions;
            set => SetProperty(ref SocialWorkerOptions, value);
        }
        public SocialWorker SelectedSocialWorker
        {
            get => _selectedSocialWorker;
            set
            {
                if (this._selectedSocialWorker == value)
                {
                    return;
                }

                this._selectedSocialWorker = value;

                this.OnPropertyChanged();
            }
        }

        public ManageHouseholdViewModel(INavigation navigation) : base(navigation)
        {
            db = App.Database;
            _validator = new HouseholdValidator();
            LoadedCBHIMembershipOptions.AddRange(App.Database.SystemCodeDetailsGetByCode("YesNoOptions"));
            LoadedRegionOptions.AddRange(App.Database.GetRegionDetails());
            LoadedWoredaOptions.AddRange(App.Database.GetWoredaDetails());
            LoadedKebeleOptions.AddRange(App.Database.GetKebeleDetails());
            LoadedSocialWorkerOptions.AddRange(App.Database.GetSocialWorkerDetails());

            AppSettings = GetAppSettings();
        }

        private ICommand _saveNewHouseholdCommand;
        public ICommand SaveNewHouseholdCommand => _saveNewHouseholdCommand ?? (_saveNewHouseholdCommand = new Command(async () => await SaveHouseholdCommand()));
        async Task SaveHouseholdCommand()
        {
            IsBusy = true;
            Message = "Validating household data... ";

            if (SelectedRegion != null)
                this.RegionID = SelectedRegion.RegionID;
            if (SelectedWoreda != null)
                this.WoredaID = SelectedWoreda.WoredaID;
            if (SelectedKebele != null)
                this.KebeleID = SelectedKebele.KebeleID;
            if (SelectedCBHIMembership != null)
                this.CBHIMembership = SelectedCBHIMembership.Value;

            var validationResult = _validator.Validate(this);
            if (!validationResult.IsValid)
            {
                ValidateMessage = GetErrorListFromValidationResult(validationResult);
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Please Check the Data and Try Again!!",
                    Message = ValidateMessage,
                    Cancel = "OK"
                });
                IsBusy = false;
                return;
            }

            try
            {
                Form1A household = new Form1A()
                {
                    RegionID = SelectedRegion.RegionID,
                    WoredaID = SelectedWoreda.WoredaID,
                    KebeleID = SelectedKebele.KebeleID,
                    Gote = Gote,
                    Gare = Gare,
                    HouseholdIDNumber = HouseholdIDNumber,
                    NameOfHouseHoldHead = NameOfHouseHoldHead,
                    CBHIMembership = CBHIMembership,
                    CBHINumber = CBHINumber,
                    CollectionDate = DateTime.Now,
                    SocialWorker = AppSettings["UserName"],
                    Remarks = Remarks,
                    CCCCBSPCMember = CCCCBSPCMember,
                    CreatedBy = AppSettings["UserName"],
                    CreatedOn = DateTime.Now,
                    Status = "PENDING SYNC"
                };

                db.Create(household);
                MessagingCenter.Send(this, "Add Household", household);
                await Navigation.PopAsync(true);
                IsBusy = false;
                return;
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error\n",
                    Message = ex.Message,
                    Cancel = "OK"
                });
                IsBusy = false;
                return;
            }
        }
    }
}
