﻿using FluentValidation;
using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Services;
using INSCTMIS.Mobile.Validators;
using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    public class ManagePLWViewModel : LocalBaseViewModel
    {
        Dictionary<string, string> AppSettings = null;
        private readonly IValidator _validator;
        private DataStore db;

        public ManagePLWViewModel(INavigation navigation) : base(navigation)
        {
            db = App.Database;
            _validator = new PLWValidator();

            IsCBHIMembership = false;
            LoadedRegionOptions.AddRange(App.Database.GetRegionDetails());
            LoadedWoredaOptions.AddRange(App.Database.GetWoredaDetails());
            LoadedKebeleOptions.AddRange(App.Database.GetKebeleDetails());
            LoadedSocialWorkerOptions.AddRange(App.Database.GetSocialWorkerDetails());
            LoadedCBHIMembershipOptions.AddRange(App.Database.SystemCodeDetailsGetByCode("YesNoOptions"));
            LoadedNutritionalStatusOptions.AddRange(App.Database.SystemCodeDetailsGetByCode("NutritionalStatusOptions"));
            LoadedPregantOrLactatingOptions.AddRange(App.Database.SystemCodeDetailsGetByCode("PLWOptions"));
            LoadedChildProtectionRiskOptions.AddRange(App.Database.SystemCodeDetailsGetByCode("YesNoOptions"));

            AppSettings = GetAppSettings();
        }

        private ICommand _saveNewPLWCommand;
        public ICommand SaveNewPLWCommand => _saveNewPLWCommand ?? (_saveNewPLWCommand = new Command(async () => await SavePLWCommand()));
        private async Task SavePLWCommand()
        {
            IsBusy = true;
            Message = "Validating PLW data... ";
            var errorMessage = "";

            if (CBHINumber == null && SelectedCBHIMembership.Value == "YES")
                errorMessage += "CBHIMembership No. Is Required\n";

            if (SelectedRegion != null)
                this.RegionID = SelectedRegion.RegionID;
            if (SelectedWoreda != null)
                this.WoredaID = SelectedWoreda.WoredaID;
            if (SelectedKebele != null)
                this.KebeleID = SelectedKebele.KebeleID;
            if (SelectedCBHIMembership != null)
                this.CBHIMembership = SelectedCBHIMembership.Value;

            var validationResult = _validator.Validate(this);

            if (!validationResult.IsValid && errorMessage.Length > 0)
            {
                ValidateMessage = GetErrorListFromValidationResult(validationResult);
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error",
                    Message = $"{ValidateMessage}\n{errorMessage}",
                    Cancel = "OK"
                });
                IsBusy = false;
                return;
            }
            else
            {
                try
                {

                    Form1B plw = new Form1B()
                    {
                        RegionID = SelectedRegion.RegionID,
                        WoredaID = SelectedWoreda.WoredaID,
                        KebeleID = SelectedKebele.KebeleID,
                        Gote = Gote,
                        Gare = Gare,
                        PLW = SelectedPregantOrLactating.Value,
                        NameOfPLW = NameOfPLW,
                        HouseHoldIDNumber = HouseHoldIDNumber,
                        CBHIMembership = SelectedCBHIMembership.Value,
                        CBHINumber = CBHINumber,
                        CollectionDate = DateTime.Now,
                        SocialWorker = AppSettings["UserName"],
                        Remarks = Remarks,
                        CCCCBSPCMember = CCCCBSPCMember,
                        MedicalRecordNumber = MedicalRecordNumber,
                        PLWAge = PLWAge,
                        StartDateTDS = StartDateTDS,
                        EndDateTDS = EndDateTDS,
                        NutritionalStatusPLW = SelectedNutritionalStatus.Value,
                        ChildProtectionRisk = SelectedPotentialChildProtectionRisk.Value,
                        CreatedBy = AppSettings["UserName"],
                        CreatedOn = DateTime.Now,
                        Status = "PENDING SYNC"
                    };

                    db.Create(plw);
                    MessagingCenter.Send(this, "Add PLW", plw);
                    await Navigation.PopAsync(true);
                    return;
                }
                catch (Exception ex)
                {
                    MessagingService.Current.SendMessage(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Error",
                        Message = $"{ex.Message}",
                        Cancel = "OK"
                    });
                    IsBusy = false;
                    return;
                }
            }
            
        }

        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public int Kebele { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
        public string PLW { get; set; }
        public string NameOfPLW { get; set; }
        public string HouseHoldIDNumber { get; set; } 
        public string CCCCBSPCMember { get; set; } 
        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }
        public DateTime CollectionDate { get; set; }
        public string Remarks { get; set; }
        public string MedicalRecordNumber { get; set; } 
        public int PLWAge { get; set; } 

        private DateTime startDateTDS;
        public DateTime StartDateTDS
        {
            get { return startDateTDS; }
            set { SetProperty(ref startDateTDS, value); }
        }

        private DateTime endDateTDS;
        public DateTime EndDateTDS
        {
            get { return endDateTDS; }
            set { SetProperty(ref endDateTDS, value); }
        }

        public string NutritionalStatusPLW { get; set; }
        public string ChildProtectionRisk { get; set; }
        private bool isCBHIMembership;
        public bool IsCBHIMembership
        {
            get { return isCBHIMembership; }
            set { SetProperty(ref isCBHIMembership, value); }
        }

        private Region _selectedRegion;
        public ObservableRangeCollection<Region> RegionOptions = new ObservableRangeCollection<Region>();
        public ObservableRangeCollection<Region> LoadedRegionOptions
        {
            get => RegionOptions;
            set => SetProperty(ref RegionOptions, value);
        }
        public Region SelectedRegion
        {
            get => _selectedRegion;
            set
            {
                if (this._selectedRegion == value)
                {
                    return;
                }

                this._selectedRegion = value;

                this.OnPropertyChanged();
            }
        }

        private Woreda _selectedWoreda;
        public ObservableRangeCollection<Woreda> WoredaOptions = new ObservableRangeCollection<Woreda>();
        public ObservableRangeCollection<Woreda> LoadedWoredaOptions
        {
            get => WoredaOptions;
            set => SetProperty(ref WoredaOptions, value);
        }
        public Woreda SelectedWoreda
        {
            get => _selectedWoreda;
            set
            {
                if (this._selectedWoreda == value)
                {
                    return;
                }

                this._selectedWoreda = value;

                this.OnPropertyChanged();
            }
        }


        private Kebele _selectedKebele;
        public ObservableRangeCollection<Kebele> KebeleOptions = new ObservableRangeCollection<Kebele>();
        public ObservableRangeCollection<Kebele> LoadedKebeleOptions
        {
            get => KebeleOptions;
            set => SetProperty(ref KebeleOptions, value);
        }
        public Kebele SelectedKebele
        {
            get => _selectedKebele;
            set
            {
                if (this._selectedKebele == value)
                {
                    return;
                }

                this._selectedKebele = value;

                this.OnPropertyChanged();
            }
        }

        private SystemCodeDetail _selectedCBHIMembership;
        public ObservableRangeCollection<SystemCodeDetail> CBHIMembershipOptions = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> LoadedCBHIMembershipOptions
        {
            get => CBHIMembershipOptions;
            set => SetProperty(ref CBHIMembershipOptions, value);
        }
        public SystemCodeDetail SelectedCBHIMembership
        {
            get => _selectedCBHIMembership;
            set
            {
                if (this._selectedCBHIMembership == value)
                {
                    return;
                }

                this._selectedCBHIMembership = value;

                if (this._selectedCBHIMembership.Value.Equals("YES"))
                {
                    IsCBHIMembership = true;
                }
                else
                {

                    IsCBHIMembership = false;
                }

                this.OnPropertyChanged();
            }
        }

        private SocialWorker _selectedSocialWorker;
        public ObservableRangeCollection<SocialWorker> SocialWorkerOptions = new ObservableRangeCollection<SocialWorker>();
        public ObservableRangeCollection<SocialWorker> LoadedSocialWorkerOptions
        {
            get => SocialWorkerOptions;
            set => SetProperty(ref SocialWorkerOptions, value);
        }
        public SocialWorker SelectedSocialWorker
        {
            get => _selectedSocialWorker;
            set
            {
                if (this._selectedSocialWorker == value)
                {
                    return;
                }

                this._selectedSocialWorker = value;

                this.OnPropertyChanged();
            }
        }

        private SystemCodeDetail _selectedPregantOrLactating;
        public ObservableRangeCollection<SystemCodeDetail> PregantOrLactatingOptions = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> LoadedPregantOrLactatingOptions
        {
            get => PregantOrLactatingOptions;
            set => SetProperty(ref PregantOrLactatingOptions, value);
        }
        public SystemCodeDetail SelectedPregantOrLactating
        {
            get => _selectedPregantOrLactating;
            set
            {
                if (this._selectedPregantOrLactating == value)
                {
                    return;
                }

                this._selectedPregantOrLactating = value;

                this.OnPropertyChanged();
            }
        }

        private SystemCodeDetail _selectedNutritionalStatus;
        public ObservableRangeCollection<SystemCodeDetail> NutritionalStatusOptions = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> LoadedNutritionalStatusOptions
        {
            get => NutritionalStatusOptions;
            set => SetProperty(ref NutritionalStatusOptions, value);
        }
        public SystemCodeDetail SelectedNutritionalStatus
        {
            get => _selectedNutritionalStatus;
            set
            {
                if (this._selectedNutritionalStatus == value)
                {
                    return;
                }

                this._selectedNutritionalStatus = value;

                this.OnPropertyChanged();
            }
        }

        private SystemCodeDetail _selectedPotentialChildProtectionRisk;
        public ObservableRangeCollection<SystemCodeDetail> ProtectionRiskOptions = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> LoadedChildProtectionRiskOptions
        {
            get => ProtectionRiskOptions;
            set => SetProperty(ref ProtectionRiskOptions, value);
        }
        public SystemCodeDetail SelectedPotentialChildProtectionRisk
        {
            get => _selectedPotentialChildProtectionRisk;
            set
            {
                if (this._selectedPotentialChildProtectionRisk == value)
                {
                    return;
                }

                this._selectedPotentialChildProtectionRisk = value;

                this.OnPropertyChanged();
            }
        }
    }
}
