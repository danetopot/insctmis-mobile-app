﻿using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Interface;
using INSCTMIS.Mobile.Pages;
using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    class MonitoringCaptureForm5ViewModel : LocalBaseViewModel
    {
        private IAppClient client;
        private INavigation Navigation;
        private MonitoringService MonitoringService;
        Dictionary<string, string> AppSettings = null;
        public string Household, ProfileDetailID, ReportingPeriod;
        public int KebeleID, ReportingPeriodID;

        public MonitoringCaptureForm5ViewModel(INavigation navigation, string household, string profiledetailid, int kebeleid, int reportingperiodid, string nocomplyreason, string formname) : base(navigation)
        {
            client = DependencyService.Get<IAppClient>();
            Navigation = navigation;
            Household = household;
            ProfileDetailID = profiledetailid;
            ReportingPeriodID = reportingperiodid;
            KebeleID = kebeleid;
            Button = "Save";
            MitigationActionLabel = "Mitigation Action";

            MonitoringService = (MonitoringService)(App.Database.GetTableRow("MonitoringService", "Reason", nocomplyreason, "FormName", formname));
            NonComplianceReasonId = MonitoringService.Id;
            NonComplianceReasonLabel = NonComplianceReason = nocomplyreason;


            if (household.Equals("PDS1"))
            {
                LoadedMitigationActionOptions.AddRange(App.Database.MitigationActionsByForm("5A1"));
            }

            if (household.Equals("PDS2"))
            {
                LoadedMitigationActionOptions.AddRange(App.Database.MitigationActionsByForm("5A2"));
            }

            if (household.Equals("PLW"))
            {
                LoadedMitigationActionOptions.AddRange(App.Database.MitigationActionsByForm("5B"));
            }

            if (household.Equals("CMC"))
            {
                LoadedMitigationActionOptions.AddRange(App.Database.MitigationActionsByForm("5C"));
            }

            AppSettings = GetAppSettings();
        }
        

        private string memberNameLabel;
        public string MemberNameLabel
        {
            get { return memberNameLabel; }
            set { SetProperty(ref memberNameLabel, value); }
        }

        private string nonComplianceReasonLabel;
        public string NonComplianceReasonLabel
        {
            get { return nonComplianceReasonLabel; }
            set { SetProperty(ref nonComplianceReasonLabel, value); }
        }

        private string mitigationActionLabel;
        public string MitigationActionLabel
        {
            get { return mitigationActionLabel; }
            set { SetProperty(ref mitigationActionLabel, value); }
        }

        private string nonComplianceReason;
        public string NonComplianceReason
        {
            get { return nonComplianceReason; }
            set { SetProperty(ref nonComplianceReason, value); }
        }

        private int nonComplianceReasonId;
        public int NonComplianceReasonId
        {
            get { return nonComplianceReasonId; }
            set { SetProperty(ref nonComplianceReasonId, value); }
        }

        private string button;
        public string Button
        {
            get { return button; }
            set { SetProperty(ref button, value); }
        }

        public ObservableRangeCollection<MonitoringService> MitigationActionOptions = new ObservableRangeCollection<MonitoringService>();
        public ObservableRangeCollection<MonitoringService> LoadedMitigationActionOptions
        {
            get => MitigationActionOptions;
            set => SetProperty(ref MitigationActionOptions, value);
        }
        private MonitoringService _selectedMitigationAction;
        public MonitoringService SelectedMitigationActionOption
        {
            get => _selectedMitigationAction;
            set
            {
                if (this._selectedMitigationAction == value)
                {
                    return;
                }

                this._selectedMitigationAction = value;

                this.OnPropertyChanged();
            }
        }

        

        private ICommand _saveMonitoringCommand;
        public ICommand SaveMonitoringCommand => _saveMonitoringCommand ?? (_saveMonitoringCommand = new Command(async () => await ExecuteSaveMonitoring()));

        private async Task ExecuteSaveMonitoring()
        {
            try
            {
                IsBusy = true;
                Message = "Validating .. ";
                var errorMessage = "";
                var monitoring = new Monitoring {  MonitoringID = Guid.NewGuid()  };

                if (errorMessage == "")
                {
                    Message = "Updating Database .. ";

                    if (SelectedMitigationActionOption.Id < 1) {
                        errorMessage += "Please Provide Mitigation Action";
                    } else {
                        monitoring.ActionResponse = SelectedMitigationActionOption.Action;
                        monitoring.ActionResponseID = SelectedMitigationActionOption.Id;
                    }

                    monitoring.NonComplianceReasonID = NonComplianceReasonId;
                    monitoring.NonComplianceReason = NonComplianceReason;
                    monitoring.Household = Household;
                    monitoring.KebeleID = KebeleID;
                    monitoring.ProfileDetailID = ProfileDetailID;
                    monitoring.ReportingPeriodID = ReportingPeriodID;
                    monitoring.SocialWorker = Settings.UserName; ;
                    monitoring.CompletedDate = DateTime.Now;
                    monitoring.MonitoringBy = Convert.ToInt64(AppSettings["UserId"]);
                    monitoring.Status = "PENDING SYNC";

                    App.Database.AddOrUpdate(monitoring);

                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Success",
                        Message = "Save successful.",
                        Cancel = "OK"
                    });


                    var data = (SystemCodeDetail)App.Database.GetTableRow("SystemCodeDetail", "Value", (ReportingPeriodID).ToString(), "SystemCode", "ReportingPeriod");
                    var reportingperiod = data.Name;

                    await Navigation.PopAsync();
                    await Navigation.PushAsync(new MonitoringForm5Page(Household, KebeleID, ProfileDetailID, reportingperiod));
                }
                else
                {
                    if (errorMessage.Length > 0)
                    {
                        ValidateMessage = $"{ValidateMessage}\n{errorMessage}";
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(
                            MessageKeys.Error,
                            new MessagingServiceAlert
                            {
                                Title = "Please Check the Data and Try Again!!",
                                Message = ValidateMessage,
                                Cancel = "OK"
                            });
                        IsBusy = false;
                    }
                }



            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                           MessageKeys.Error,
                           new MessagingServiceAlert
                           {
                               Title = "Please Correct the Data and Try Again!!",
                               Message = e.Message,
                               Cancel = "OK"
                           });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            return;
        }

        private string GetFormType(string hh)
        {
            string formname = string.Empty;

            if (hh.Equals("PDS1")) { formname = "5A1"; }
            if (hh.Equals("PDS2")) { formname = "5A2"; }
            if (hh.Equals("PLW")) { formname = "5B"; }
            if (hh.Equals("CMC")) { formname = "5C"; }

            return formname;
        }
    }
}
