﻿using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Interface;
using INSCTMIS.Mobile.Pages;
using MvvmHelpers;
using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    class MonitoringForm5ViewModel : LocalBaseViewModel
    {
        private IAppClient client;
        public INavigation Navigation;
        public string Household, ProfileDetailID;
        public int KebeleID, ReportingPeriodID;

        public MonitoringForm5ViewModel(INavigation navigation, string household, int kebeleid, string profiledetailid, string reportingperiod) : base(navigation)
        {
            client = DependencyService.Get<IAppClient>();
            Navigation = navigation;
            var data = (SystemCodeDetail)App.Database.GetTableRow("SystemCodeDetail", "Name", reportingperiod, "SystemCode", "ReportingPeriod");

            Household = household;
            ProfileDetailID = profiledetailid;
            ReportingPeriodID = Convert.ToInt32(data.Value);
            KebeleID = kebeleid;
            Title = "Non-Compliance Reasons";
            TitleNonComplianceReasons = "Reasons";
            TitlePendingSync = "Pending Upload";

            DownloadedHouseholdsMonitoringServices.AddRange(GetHouseholdsMonitoringServices(Household));
            PendingSyncMonitoringHouseholds.AddRange(GetPendingSyncMonitoringHouseholds(ProfileDetailID));
        }

        private string titleNonComplianceReasons;
        public string TitleNonComplianceReasons
        {
            get { return titleNonComplianceReasons; }
            set { SetProperty(ref titleNonComplianceReasons, value); }
        }

        private string nonComplianceReason;
        public string NonComplianceReason
        {
            get { return nonComplianceReason; }
            set { SetProperty(ref nonComplianceReason, value); }
        }

        private string titlePendingSync;
        public string TitlePendingSync
        {
            get { return titlePendingSync; }
            set { SetProperty(ref titlePendingSync, value); }
        }

        private string title;
        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }


        private string message;
        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private string messageSync;
        public string MessageSync
        {
            get { return messageSync; }
            set { SetProperty(ref messageSync, value); }
        }

        public ObservableCollection<MonitoringService> GetHouseholdsMonitoringServices(string household)
        {
            string FormName = GetFormType(household);

            IsBusy = true;
            try
            {
                var items = App.Database.GetTableRows("MonitoringService", "FormName", FormName);
                var hh = new ObservableCollection<MonitoringService>();
                foreach (var item in items)
                {
                    var hhitem = (MonitoringService)item;
                    hh.Add(hhitem);
                }
                return hh;

            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Households",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }
        public ObservableRangeCollection<MonitoringService> DownloadedHouseholdsMonitoringServices { get; } = new ObservableRangeCollection<MonitoringService>();
        private MonitoringService selectedMonitoringServiceCommand;
        public MonitoringService SelectedMonitoringServiceCommand
        {
            get { return selectedMonitoringServiceCommand; }
            set
            {
                selectedMonitoringServiceCommand = value;
                OnPropertyChanged();
                if (selectedMonitoringServiceCommand == null)
                    return;
                Navigation.PushAsync(new MonitoringCaptureForm5Page(Household, ProfileDetailID, KebeleID, ReportingPeriodID, selectedMonitoringServiceCommand.Reason, SelectedMonitoringServiceCommand.FormName));

                selectedMonitoringServiceCommand = null;
            }
        }


        public ObservableCollection<Monitoring> GetPendingSyncMonitoringHouseholds(string id)
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetMonitoringTableRowsPendingSyncRows<Monitoring>("Monitoring");
                var hh = new ObservableCollection<Monitoring>();
                foreach (var item in items)
                {
                    var hhitem = (Monitoring)item;
                    hh.Add(hhitem);
                }
                return hh;

            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Household Members",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }
        public ObservableRangeCollection<Monitoring> PendingSyncMonitoringHouseholds { get; } = new ObservableRangeCollection<Monitoring>();
        private Monitoring selectedMonitoringCommand;
        public Monitoring SelectedMonitoringCommand
        {
            get { return selectedMonitoringCommand; }
            set
            {
                selectedMonitoringCommand = value;
                OnPropertyChanged();
                if (selectedMonitoringCommand == null)
                    return;
                Navigation.PushAsync(
                    new MonitoringCaptureForm5Page(
                    selectedMonitoringCommand.Household,
                    selectedMonitoringCommand.ProfileDetailID,
                    selectedMonitoringCommand.KebeleID,
                    selectedMonitoringCommand.ReportingPeriodID,
                    GetFormType(selectedMonitoringCommand.Household),
                    NonComplianceReason)
                    );

                selectedMonitoringCommand = null;
            }
        }

        private string GetFormType(string hh)
        {
            string formname = string.Empty;

            if (hh.Equals("PDS1")) { formname = "5A1"; }
            if (hh.Equals("PDS2")) { formname = "5A2"; }
            if (hh.Equals("PLW")) { formname = "5B"; }
            if (hh.Equals("CMC")) { formname = "5C"; }

            return formname;
        }
    }
}
