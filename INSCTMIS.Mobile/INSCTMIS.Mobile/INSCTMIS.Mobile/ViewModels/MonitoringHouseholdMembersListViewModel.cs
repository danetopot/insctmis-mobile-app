﻿using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Interface;
using INSCTMIS.Mobile.Pages;
using MvvmHelpers;
using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    class MonitoringHouseholdMembersListViewModel : LocalBaseViewModel
    {
        private IAppClient client;
        public INavigation Navigation;

        public MonitoringHouseholdMembersListViewModel(INavigation navigation,string household, string headerid) : base(navigation)
        {
            client = DependencyService.Get<IAppClient>();
            Navigation = navigation;

            if (household.Equals("PDS1"))
            {
                Title = "PDS School Members";
                DownloadedTDS1MonitoringHouseholdMembers.AddRange(GetTDS1HouseholdMembersReadyForMonitoring(headerid));
            }

            if (household.Equals("PDS2"))
            {
                Title = "PDS Household Members";
                DownloadedTDS2MonitoringHouseholdMembers.AddRange(GetTDS2HouseholdMembersReadyForMonitoring(headerid));
            }

            if (household.Equals("PLW"))
            {
                Title = "PLW Household Members";
                DownloadedPLWMonitoringHouseholdMembers.AddRange(GetPLWHouseholdMembersReadyForMonitoring(headerid));
            }

            if (household.Equals("CMC"))
            {
                Title = "CMC Household Members";
                DownloadedCMCMonitoringHouseholdMembers.AddRange(GetCMCHouseholdMembersReadyForMonitoring(headerid));
            }
        }

        private string title;
        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }

        public ObservableCollection<Monitoring5A1Members> GetTDS1HouseholdMembersReadyForMonitoring(string id)
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetTableRows("Monitoring5A1Members", "ProfileDSHeaderID", id);
                var hh = new ObservableCollection<Monitoring5A1Members>();
                foreach (var item in items)
                {
                    var hhitem = (Monitoring5A1Members)item;
                    hh.Add(hhitem);
                }
                return hh;
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Household Members",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }
        public ObservableRangeCollection<Monitoring5A1Members> DownloadedTDS1MonitoringHouseholdMembers { get; } = new ObservableRangeCollection<Monitoring5A1Members>();
        private Monitoring5A1Members selectedTDS1HouseholdMemberCommand;
        public Monitoring5A1Members SelectedTDS1HouseholdMemberCommand
        {
            get { return selectedTDS1HouseholdMemberCommand; }
            set
            {
                selectedTDS1HouseholdMemberCommand = value;
                OnPropertyChanged();
                if (selectedTDS1HouseholdMemberCommand == null)
                    return;
                Navigation.PushAsync(new MonitoringForm5Page("PDS1", selectedTDS1HouseholdMemberCommand.KebeleID, selectedTDS1HouseholdMemberCommand.ProfileDSDetailID, selectedTDS1HouseholdMemberCommand.ReportingPeriod));

                selectedTDS1HouseholdMemberCommand = null;
            }
        }

        public ObservableCollection<Monitoring5A2Members> GetTDS2HouseholdMembersReadyForMonitoring(string id)
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetTableRows("Monitoring5A2Members", "ProfileDSHeaderID", id);
                var hh = new ObservableCollection<Monitoring5A2Members>();
                foreach (var item in items)
                {
                    var hhitem = (Monitoring5A2Members)item;
                    hh.Add(hhitem);
                }
                return hh;
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Household Members",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }
        public ObservableRangeCollection<Monitoring5A2Members> DownloadedTDS2MonitoringHouseholdMembers { get; } = new ObservableRangeCollection<Monitoring5A2Members>();
        private Monitoring5A2Members selectedTDS2HouseholdMemberCommand;
        public Monitoring5A2Members SelectedTDS2HouseholdMemberCommand
        {
            get { return selectedTDS2HouseholdMemberCommand; }
            set
            {
                selectedTDS2HouseholdMemberCommand = value;
                OnPropertyChanged();
                if (selectedTDS2HouseholdMemberCommand == null)
                    return;
                Navigation.PushAsync(new MonitoringForm5Page("PDS2", selectedTDS2HouseholdMemberCommand.KebeleID, selectedTDS2HouseholdMemberCommand.ProfileDSDetailID, selectedTDS2HouseholdMemberCommand.ReportingPeriod));

                selectedTDS2HouseholdMemberCommand = null;
            }
        }

        public ObservableCollection<Monitoring5BMembers> GetPLWHouseholdMembersReadyForMonitoring(string id)
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetTableRows("Monitoring5BMembers", "ProfileTDSPLWID", id);
                var hh = new ObservableCollection<Monitoring5BMembers>();
                foreach (var item in items)
                {
                    var hhitem = (Monitoring5BMembers)item;
                    hh.Add(hhitem);
                }
                return hh;
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Household Members",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }
        public ObservableRangeCollection<Monitoring5BMembers> DownloadedPLWMonitoringHouseholdMembers { get; } = new ObservableRangeCollection<Monitoring5BMembers>();
        private Monitoring5BMembers selectedPLWHouseholdMemberCommand;
        public Monitoring5BMembers SelectedPLWHouseholdMemberCommand
        {
            get { return selectedPLWHouseholdMemberCommand; }
            set
            {
                selectedPLWHouseholdMemberCommand = value;
                OnPropertyChanged();
                if (selectedPLWHouseholdMemberCommand == null)
                    return;
                Navigation.PushAsync(new MonitoringForm5Page("PLW", selectedPLWHouseholdMemberCommand.KebeleID, selectedPLWHouseholdMemberCommand.ProfileTDSPLWID, selectedPLWHouseholdMemberCommand.ReportingPeriod));

                selectedPLWHouseholdMemberCommand = null;
            }
        }

        public ObservableCollection<Monitoring5CMembers> GetCMCHouseholdMembersReadyForMonitoring(string id)
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetTableRows("Monitoring5CMembers", "ProfileTDSCMCID", id);
                var hh = new ObservableCollection<Monitoring5CMembers>();
                foreach (var item in items)
                {
                    var hhitem = (Monitoring5CMembers)item;
                    hh.Add(hhitem);
                }
                return hh;
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Household Members",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }
        public ObservableRangeCollection<Monitoring5CMembers> DownloadedCMCMonitoringHouseholdMembers { get; } = new ObservableRangeCollection<Monitoring5CMembers>();
        private Monitoring5CMembers selectedCMCHouseholdMemberCommand;
        public Monitoring5CMembers SelectedCMCHouseholdMemberCommand
        {
            get { return selectedCMCHouseholdMemberCommand; }
            set
            {
                selectedCMCHouseholdMemberCommand = value;
                OnPropertyChanged();
                if (selectedCMCHouseholdMemberCommand == null)
                    return;
                Navigation.PushAsync(new MonitoringForm5Page("CMC", selectedCMCHouseholdMemberCommand.KebeleID, selectedCMCHouseholdMemberCommand.ProfileTDSCMCID, selectedCMCHouseholdMemberCommand.ReportingPeriod));

                selectedCMCHouseholdMemberCommand = null;
            }
        }
    }
}
