﻿using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Interface;
using INSCTMIS.Mobile.Pages;
using MvvmHelpers;
using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    class MonitoringHouseholdsListViewModel : LocalBaseViewModel
    {
        private IAppClient client;
        public INavigation Navigation;

        public MonitoringHouseholdsListViewModel(INavigation navigation) : base(navigation)
        {
            client = DependencyService.Get<IAppClient>();
            Navigation = navigation;

            DownloadedTDS1MonitoringHouseholds.ReplaceRange(GetTDS1HouseholdsReadyForMonitoring());
            DownloadedTDS2MonitoringHouseholds.ReplaceRange(GetTDS2HouseholdsReadyForMonitoring());
            DownloadedPLWMonitoringHouseholds.ReplaceRange(GetPLWHouseholdsReadyForMonitoring());
            DownloadedCMCMonitoringHouseholds.ReplaceRange(GetCMCHouseholdsReadyForMonitoring());
        }

        private string message;
        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private string messageSync;
        public string MessageSync
        {
            get { return messageSync; }
            set { SetProperty(ref messageSync, value); }
        }

        public ObservableCollection<Monitoring5A1Household> GetTDS1HouseholdsReadyForMonitoring()
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetTable("Monitoring5A1Household");
                var hh = new ObservableCollection<Monitoring5A1Household>();
                foreach (var item in items)
                {
                    var hhitem = (Monitoring5A1Household)item;
                    hh.Add(hhitem);
                }
                return hh;

            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Households",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }
        public ObservableCollection<Monitoring5A2Household> GetTDS2HouseholdsReadyForMonitoring()
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetTable("Monitoring5A2Household");
                var hh = new ObservableCollection<Monitoring5A2Household>();
                foreach (var item in items)
                {
                    var hhitem = (Monitoring5A2Household)item;
                    hh.Add(hhitem);
                }
                return hh;

            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Households",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }
        public ObservableCollection<Monitoring5BHousehold> GetPLWHouseholdsReadyForMonitoring()
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetTable("Monitoring5BHousehold");
                var hh = new ObservableCollection<Monitoring5BHousehold>();
                foreach (var item in items)
                {
                    var hhitem = (Monitoring5BHousehold)item;
                    hh.Add(hhitem);
                }
                return hh;

            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Households",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }
        public ObservableCollection<Monitoring5CHousehold> GetCMCHouseholdsReadyForMonitoring()
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetTable("Monitoring5CHousehold");
                var hh = new ObservableCollection<Monitoring5CHousehold>();
                foreach (var item in items)
                {
                    var hhitem = (Monitoring5CHousehold)item;
                    hh.Add(hhitem);
                }
                return hh;

            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Households",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }

        public ObservableRangeCollection<Monitoring5A1Household> DownloadedTDS1MonitoringHouseholds { get; } = new ObservableRangeCollection<Monitoring5A1Household>();
        private Monitoring5A1Household selectedTDS1HouseholdCommand;
        public Monitoring5A1Household SelectedTDS1HouseholdCommand
        {
            get { return selectedTDS1HouseholdCommand; }
            set
            {
                selectedTDS1HouseholdCommand = value;
                OnPropertyChanged();
                if (selectedTDS1HouseholdCommand == null)
                    return;
                Navigation.PushAsync(new MonitoringHouseholdMembersListPage("PDS1", selectedTDS1HouseholdCommand.ProfileDSHeaderID));

                selectedTDS1HouseholdCommand = null;
            }
        }

        public ObservableRangeCollection<Monitoring5A2Household> DownloadedTDS2MonitoringHouseholds { get; } = new ObservableRangeCollection<Monitoring5A2Household>();
        private Monitoring5A2Household selectedTDS2HouseholdCommand;
        public Monitoring5A2Household SelectedTDS2HouseholdCommand
        {
            get { return selectedTDS2HouseholdCommand; }
            set
            {
                selectedTDS2HouseholdCommand = value;
                OnPropertyChanged();
                if (selectedTDS2HouseholdCommand == null)
                    return;
                Navigation.PushAsync(new MonitoringHouseholdMembersListPage("PDS2", selectedTDS2HouseholdCommand.ProfileDSHeaderID));

                selectedTDS2HouseholdCommand = null;
            }
        }

        public ObservableRangeCollection<Monitoring5BHousehold> DownloadedPLWMonitoringHouseholds { get; } = new ObservableRangeCollection<Monitoring5BHousehold>();
        private Monitoring5BHousehold selectedPLWHouseholdCommand;
        public Monitoring5BHousehold SelectedPLWHouseholdCommand
        {
            get { return selectedPLWHouseholdCommand; }
            set
            {
                selectedPLWHouseholdCommand = value;
                OnPropertyChanged();
                if (selectedPLWHouseholdCommand == null)
                    return;
                Navigation.PushAsync(new MonitoringHouseholdMembersListPage("PLW", selectedPLWHouseholdCommand.ProfileTDSPLWID));

                selectedPLWHouseholdCommand = null;
            }
        }

        public ObservableRangeCollection<Monitoring5CHousehold> DownloadedCMCMonitoringHouseholds { get; } = new ObservableRangeCollection<Monitoring5CHousehold>();
        private Monitoring5CHousehold selectedCMCHouseholdCommand;
        public Monitoring5CHousehold SelectedCMCHouseholdCommand
        {
            get { return selectedCMCHouseholdCommand; }
            set
            {
                selectedCMCHouseholdCommand = value;
                OnPropertyChanged();
                if (selectedCMCHouseholdCommand == null)
                    return;
                Navigation.PushAsync(new MonitoringHouseholdMembersListPage("CMC", selectedCMCHouseholdCommand.ProfileTDSCMCID));

                selectedCMCHouseholdCommand = null;
            }
        }
    }

    
}
