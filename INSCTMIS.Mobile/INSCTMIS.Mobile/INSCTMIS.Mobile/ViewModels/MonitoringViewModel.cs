﻿using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Interface;
using INSCTMIS.Mobile.Pages;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    class MonitoringViewModel : LocalBaseViewModel
    {
        private IAppClient client;
        public INavigation Navigation;

        public MonitoringViewModel(INavigation navigation) : base(navigation)
        {
            client = DependencyService.Get<IAppClient>();
            Navigation = navigation;
            LoadMonitoringHouseholdText = "Load Monitoring Households";
        }

        private string message;
        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private string messageSync;
        public string MessageSync
        {
            get { return messageSync; }
            set { SetProperty(ref messageSync, value); }
        }

        private string loadMonitoringHouseholdText;
        public string LoadMonitoringHouseholdText
        {
            get { return loadMonitoringHouseholdText; }
            set { SetProperty(ref loadMonitoringHouseholdText, value); }
        }

        private ICommand loadMonitoringHouseholdCommand;
        public ICommand LoadMonitoringHouseholdCommand => loadMonitoringHouseholdCommand ?? (loadMonitoringHouseholdCommand = new Command(async () => await ExecuteLoadMonitoringHouseholdAsync()));
        private async Task ExecuteLoadMonitoringHouseholdAsync()
        {
            if (IsBusy)
            {
                return;
            }
            IsBusy = true;
            MessageSync = "Populating Tables... ...";
            loadMonitoringHouseholdText = "Loading Monitoring Households . . .";

            try
            {
                await Navigation.PushAsync(new MonitoringHouseholdsListPage());

                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Success",
                    Message = "Monitoring households loaded successfully",
                    Cancel = "OK"
                });
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error",
                    Message = "" + ex.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageSync = string.Empty;
                loadMonitoringHouseholdText = "Load Monitoring Households";
                IsBusy = false;
                await Task.FromResult(0);
            }

        }
    }
}
