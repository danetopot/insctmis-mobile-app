﻿using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Pages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    public class RegisteredCaretakersListViewModel : LocalBaseViewModel
    {
        public RegisteredCaretakersListViewModel(INavigation navigation) : base(navigation)
        {
            //Instantiate variables needed
            db = App.Database;
            LoadRegisteredCaretakers();
            AddCaretakerCommand = new Command(async () => await Navigation.PushAsync(new ManageCaretakerPage()));
            LoadCaretakerCommand = new Command(() => LoadRegisteredCaretakers());

            //Subscribe for added households
            MessagingCenter.Subscribe<ManageCaretakerViewModel, Form1C>(this, "Add Caretaker", (obj, item) =>
            {
                Form1C newHousehold = item as Form1C;
                var Region = (Database.Region)db.GetTableRow("Region", "RegionID", "" + newHousehold.RegionID);
                var Woreda = (Woreda)db.GetTableRow("Woreda", "WoredaID", "" + newHousehold.WoredaID);
                var Kebele = (Kebele)db.GetTableRow("Kebele", "KebeleID", "" + newHousehold.KebeleID);

                RegisteredCaretakersList.Insert(0, new Form1CViewModel()
                {
                    Id = item.Id,
                    Location = String.Concat(Region.RegionName + ", ", Woreda.WoredaName + ", ", Kebele.KebeleName),
                    HouseHoldIDNumber = item.HouseHoldIDNumber,
                    NameOfCareTaker = item.NameOfCareTaker + " (ID: " + item.CaretakerID + ")",
                    MalnourishedChildName = item.MalnourishedChildName + " (ID: " + item.ChildID + ")",
                    MalnourishedChildSex = item.MalnourishedChildSex,
                    ChildDateOfBirth = item.ChildDateOfBirth

                });

                PageTitle = RegisteredCaretakersList.Count > 0 ?
                                String.Concat("Registered Caretakers - ", RegisteredCaretakersList.Count) :
                                "Registered Caretakers";
                OnPropertyChanged(nameof(PageTitle));
            });
        }

        void LoadRegisteredCaretakers()
        {
            IsBusy = true;
            try
            {
                List<Form1C> caretakers = db.GetTableRows<Form1C>("Form1C");
                RegisteredCaretakersList = new ObservableCollection<Form1CViewModel>();
                if(caretakers.Count > 0)
                {
                    foreach (var item in caretakers)
                    {
                        var Region = (Database.Region)db.GetTableRow("Region", "RegionID", "" + item.RegionID);
                        var Woreda = (Woreda)db.GetTableRow("Woreda", "WoredaID", "" + item.WoredaID);
                        var Kebele = (Kebele)db.GetTableRow("Kebele", "KebeleID", "" + item.KebeleID);

                        RegisteredCaretakersList.Insert(0, new Form1CViewModel()
                        {
                            Id = item.Id,
                            Location = String.Concat(Region.RegionName + ", ", Woreda.WoredaName + ", ", Kebele.KebeleName),
                            HouseHoldIDNumber = item.HouseHoldIDNumber,
                            NameOfCareTaker = item.NameOfCareTaker + " (ID: " + item.CaretakerID + ")",
                            MalnourishedChildName = item.MalnourishedChildName + " (ID: " + item.ChildID + ")",
                            MalnourishedChildSex = item.MalnourishedChildSex,
                            ChildDateOfBirth = item.ChildDateOfBirth

                        });
                    }
                }

                PageTitle = RegisteredCaretakersList.Count > 0 ?
                                String.Concat("Registered Caretakers - ", RegisteredCaretakersList.Count) :
                                "Registered Caretakers";
                OnPropertyChanged(nameof(PageTitle));


            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to load Caretakers.  ",
                    Message = e.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                IsBusy = false;
            }
        }

        private DataStore db;
        public string Message { get; set; }

        public ObservableCollection<Form1CViewModel> RegisteredCaretakersList { get; set; }

        public string PageTitle { get; set; }

        public Command AddCaretakerCommand { get; set; }

        public Command LoadCaretakerCommand { get; set; }
    }

    public class Form1CViewModel
    {
        public int Id { get; set; }
        public string Location { get; set; }
        public string HouseHoldIDNumber { get; set; } //AKA PSNP HH Number
        public string NameOfCareTaker { get; set; }
        public string MalnourishedChildName { get; set; }
        public string MalnourishedChildSex { get; set; }
        public DateTime ChildDateOfBirth { get; set; }
    }
}
