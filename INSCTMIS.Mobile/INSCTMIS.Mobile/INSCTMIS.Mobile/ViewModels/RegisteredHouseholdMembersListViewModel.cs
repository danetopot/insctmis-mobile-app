﻿using FormsToolkit;
using INSCTMIS.Mobile.Database;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace INSCTMIS.Mobile.ViewModels
{
    public class RegisteredHouseholdMembersListViewModel : LocalBaseViewModel
    {
        public RegisteredHouseholdMembersListViewModel(int household)
        {
            db = App.Database;
            Household = household;
            LoadHouseholdMembers();
        }

        void LoadHouseholdMembers()
        {
            IsBusy = true;
            try
            {
                List<HouseholdMember> householdMembers = db.GetTableRows<HouseholdMember>("HouseholdMember", "Form1AId", "" + Household);
                RegisteredHouseholdMembersList = new ObservableCollection<HouseholdMemberViewModel>();
                foreach (var item in householdMembers)
                {

                    RegisteredHouseholdMembersList.Insert(0, new HouseholdMemberViewModel()
                    {
                        Id = item.Id,
                        HouseHoldMemberName = item.HouseHoldMemberName,
                        DateOfBirth = item.DateOfBirth,
                        Age = item.Age
                    }); ;

                }

                RegisteredHouseholdMembers = "Household Members";

                PageTitle = RegisteredHouseholdMembersList.Count > 0 ?
                                String.Concat(RegisteredHouseholdMembers, " (", RegisteredHouseholdMembersList.Count, ")") :
                                RegisteredHouseholdMembers + " (0)";
                OnPropertyChanged(nameof(PageTitle));

            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to load household members.  ",
                    Message = e.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                IsBusy = false;
            }
        }

        private DataStore db;

        public string Message { get; set; }

        public ObservableCollection<HouseholdMemberViewModel> RegisteredHouseholdMembersList { get; set; }

        public string PageTitle { get; set; }

        public int Household { get; set; }


        public string registeredHouseholdMembers;
        public string RegisteredHouseholdMembers
        {
            get { return registeredHouseholdMembers; }
            set { SetProperty(ref registeredHouseholdMembers, value); }
        }
    }

    public class HouseholdMemberViewModel
    {
        public int Id { get; set; }
        public string HouseHoldMemberName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int Age { get; set; }
    }
}
