﻿using FormsToolkit;
using INSCTMIS.Mobile.Database;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace INSCTMIS.Mobile.ViewModels
{
    public class RegisteredPLWInfantsListViewModel : LocalBaseViewModel
    {
        public RegisteredPLWInfantsListViewModel(int plwId)
        {
            db = App.Database;
            PLWId = plwId;
            LoadInfants();
        }

        void LoadInfants()
        {
            IsBusy = true;
            try
            {
                List<Infant> infants = db.GetTableRows<Infant>("Infant", "Form1BId", "" + PLWId);
                RegisteredInfantsList = new ObservableCollection<Infant>();
                foreach (var item in infants)
                {

                    RegisteredInfantsList.Insert(0, new Infant()
                    {
                        Id = item.Id,
                        BabyName = item.BabyName,
                        BabyDateOfBirth = item.BabyDateOfBirth,
                        BabySex = item.BabySex
                    }); ;

                }

                PageTitle = RegisteredInfantsList.Count > 0 ?
                                String.Concat("Infants - ", RegisteredInfantsList.Count) :
                                "Infants (0)";
                OnPropertyChanged(nameof(PageTitle));

            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to infants.  ",
                    Message = e.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                IsBusy = false;
            }
        }

        private DataStore db;
        public string Message { get; set; }
        public ObservableCollection<Infant> RegisteredInfantsList { get; set; }
        public string PageTitle { get; set; }
        public int PLWId { get; set; }
    }
}
