﻿using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Pages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    public class RegisteredPLWListViewModel : LocalBaseViewModel
    {

        public RegisteredPLWListViewModel(INavigation navigation) : base(navigation)
        {
            //Instantiate variables needed
            db = App.Database;
            LoadRegisteredPWL();
            AddPLWCommand = new Command(async () => await Navigation.PushAsync(new ManagePLWPage()));
            LoadPLWCommand = new Command(() => LoadRegisteredPWL());
            addInfantCommand = new Command(AddPWLInfant);
            viewInfantCommand = new Command(ViewPLWInfants);

            //Subscribe for added households
            MessagingCenter.Subscribe<ManagePLWViewModel, Form1B>(this, "Add PLW", (obj, item) =>
            {
                Form1B newHousehold = item as Form1B;
                var Region = (Database.Region)db.GetTableRow("Region", "RegionID", "" + newHousehold.RegionID);
                var Woreda = (Woreda)db.GetTableRow("Woreda", "WoredaID", "" + newHousehold.WoredaID);
                var Kebele = (Kebele)db.GetTableRow("Kebele", "KebeleID", "" + newHousehold.KebeleID);
                var count = db.GetTableRows<Infant>("Infant", "Form1BId", "" + item.Id).Count;

                RegisteredPLWList.Insert(0, new Form1BViewModel()
                {
                    Id = item.Id,
                    Location = String.Concat(Region.RegionName + ", ", Woreda.WoredaName + ", ", Kebele.KebeleName),
                    HouseholdIDNumber = item.HouseHoldIDNumber,
                    NameOfPLW = item.NameOfPLW,
                    InfantCount = count > 0 ? count + " Infant(s)" : "No Infant",
                    PLW = item
                });

                PageTitle = RegisteredPLWList.Count > 0 ?
                               String.Concat("Registered PLW - ", RegisteredPLWList.Count) :
                               "Registered PLW";
                OnPropertyChanged(nameof(PageTitle));
            });

            MessagingCenter.Subscribe<ManagePLWInfantViewModel>(this, "Add Infant", (obj) =>
            {
                LoadRegisteredPWL();
                OnPropertyChanged(nameof(RegisteredPLWList));
            });
        }


        void LoadRegisteredPWL()
        {
            IsBusy = true;
            try
            {
                List<Form1B> plws = db.GetTableRows<Form1B>("Form1B");
                RegisteredPLWList = new ObservableCollection<Form1BViewModel>();
                foreach (var item in plws)
                {
                    var Region = (Database.Region)db.GetTableRow("Region", "RegionID", "" + item.RegionID);
                    var Woreda = (Woreda)db.GetTableRow("Woreda", "WoredaID", "" + item.WoredaID);
                    var Kebele = (Kebele)db.GetTableRow("Kebele", "KebeleID", "" + item.KebeleID);
                    var count = db.GetTableRows<Infant>("Infant", "Form1BId", "" + item.Id).Count;

                    RegisteredPLWList.Insert(0, new Form1BViewModel()
                    {
                        Id = item.Id,
                        Location = String.Concat(Region.RegionName + ", ", Woreda.WoredaName + ", ", Kebele.KebeleName),
                        HouseholdIDNumber = item.HouseHoldIDNumber,
                        NameOfPLW = item.NameOfPLW,
                        InfantCount = count > 0 ? count + " Infant(s)" : "No Infant",
                        PLW = item
                    });
                }

                PageTitle = RegisteredPLWList.Count > 0 ?
                                String.Concat("Registered PLW - ", RegisteredPLWList.Count) :
                                "Registered PLW";
                OnPropertyChanged(nameof(PageTitle));


            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to load PLWs.  ",
                    Message = e.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                IsBusy = false;
            }
        }

        async void AddPWLInfant(object sender)
        {
            //Grab the active household from the sender object
            Form1BViewModel model = (Form1BViewModel)sender;
            await Navigation.PushAsync(new ManagePLWInfantPage(model.Id));
        }

        async void ViewPLWInfants(object sender)
        {
            Form1BViewModel model = (Form1BViewModel)sender;
            await Navigation.PushAsync(new RegisteredPLWInfantsListPage(model.Id));
        }

        private DataStore db;
        public string Message { get; set; }

        public ObservableCollection<Form1BViewModel> RegisteredPLWList { get; set; }
        private Form1B _selectedPLW { get; set; }
        public Form1B SelectedPLW
        {
            get => _selectedPLW;
            set
            {
                if (_selectedPLW != value)
                {
                    _selectedPLW = value;
                }
            }
        }

        public string PageTitle { get; set; }

        public Command AddPLWCommand { get; set; }

        public Command LoadPLWCommand { get; set; }
        public ICommand addInfantCommand { get; set; }
        public ICommand viewInfantCommand { get; set; }
        public ICommand AddInfantCommand
        {
            get => addInfantCommand;
        }
        public ICommand ViewInfantsCommand
        {
            get => viewInfantCommand;
        }
    }

    public class Form1BViewModel
    {
        public int Id { get; set; }
        public string Location { get; set; }
        public string HouseholdIDNumber { get; set; } //AKA PSNP HH Number
        public string NameOfPLW { get; set; }
        public string InfantCount { get; set; }
        public Form1B PLW { get; set; }
    }
}
