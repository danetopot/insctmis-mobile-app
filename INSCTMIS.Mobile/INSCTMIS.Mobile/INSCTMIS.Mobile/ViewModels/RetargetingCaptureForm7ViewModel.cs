﻿using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Interface;
using INSCTMIS.Mobile.Pages;
using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    class RetargetingCaptureForm7ViewModel : LocalBaseViewModel
    {
        private IAppClient client;
        public INavigation Navigation;
        private ICommand _saveRetargetingCommand;
        private ICommand _editRetargetingCommand;
        private RetargetingMembers RetargetingMembers;
        Dictionary<string, string> AppSettings = null;

        private bool isPregnant;
        public bool IsPregnant
        {
            get { return isPregnant; }
            set { SetProperty(ref isPregnant, value); }
        }

        private bool isLactating;
        public bool IsLactating
        {
            get { return isLactating; }
            set { SetProperty(ref isLactating, value); }
        }

        private bool isDisabled;
        public bool IsDisabled
        {
            get { return isDisabled; }
            set { SetProperty(ref isDisabled, value); }
        }

        private bool isChronicallyIll;
        public bool IsChronicallyIll
        {
            get { return isChronicallyIll; }
            set { SetProperty(ref isChronicallyIll, value); }
        }

        private bool isNutritionalStatus;
        public bool IsNutritionalStatus
        {
            get { return isNutritionalStatus; }
            set { SetProperty(ref isNutritionalStatus, value); }
        }

        private bool isChildUnderTSForCMAM;
        public bool IsChildUnderTSForCMAM
        {
            get { return isChildUnderTSForCMAM; }
            set { SetProperty(ref isChildUnderTSForCMAM, value); }
        }

        private bool isEnrolledInSchool;
        public bool IsEnrolledInSchool
        {
            get { return isEnrolledInSchool; }
            set { SetProperty(ref isEnrolledInSchool, value); }
        }

        private bool isGrade;
        public bool IsGrade
        {
            get { return isGrade; }
            set { SetProperty(ref isGrade, value); }
        }

        private bool isSchoolName;
        public bool IsSchoolName
        {
            get { return isSchoolName; }
            set { SetProperty(ref isSchoolName, value); }
        }

        private string cccMemberLabel;
        public string CCCMemberLabel
        {
            get { return cccMemberLabel; }
            set { SetProperty(ref cccMemberLabel, value); }
        }

        private string statusLabel;
        public string StatusLabel
        {
            get { return statusLabel; }
            set { SetProperty(ref statusLabel, value); }
        }

        private string pregnantLabel;
        public string PregnantLabel
        {
            get { return pregnantLabel; }
            set { SetProperty(ref pregnantLabel, value); }
        }

        private string lactatingLabel;
        public string LactatingLabel
        {
            get { return lactatingLabel; }
            set { SetProperty(ref lactatingLabel, value); }
        }

        private string disabledLabel;
        public string DisabledLabel
        {
            get { return disabledLabel; }
            set { SetProperty(ref disabledLabel, value); }
        }

        private string chronicallyIllLabel;
        public string ChronicallyIllLabel
        {
            get { return chronicallyIllLabel; }
            set { SetProperty(ref chronicallyIllLabel, value); }
        }

        private string nutritionalStatusLabel;
        public string NutritionalStatusLabel
        {
            get { return nutritionalStatusLabel; }
            set { SetProperty(ref nutritionalStatusLabel, value); }
        }

        private string childUnderTSForCMAMLabel;
        public string ChildUnderTSForCMAMLabel
        {
            get { return childUnderTSForCMAMLabel; }
            set { SetProperty(ref childUnderTSForCMAMLabel, value); }
        }

        private string enrolledInSchoolLabel;
        public string EnrolledInSchoolLabel
        {
            get { return enrolledInSchoolLabel; }
            set { SetProperty(ref enrolledInSchoolLabel, value); }
        }

        private string gradeLabel;
        public string GradeLabel
        {
            get { return gradeLabel; }
            set { SetProperty(ref gradeLabel, value); }
        }

        private string schoolNameLabel;
        public string SchoolNameLabel
        {
            get { return schoolNameLabel; }
            set { SetProperty(ref schoolNameLabel, value); }
        }

        private string button;
        public string Button
        {
            get { return button; }
            set { SetProperty(ref button, value); }
        }

        private string message;
        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private string messageSync;
        public string MessageSync
        {
            get { return messageSync; }
            set { SetProperty(ref messageSync, value); }
        }

        private string memberName;
        public string MemberName
        {
            get { return memberName; }
            set { SetProperty(ref memberName, value); }
        }

        private string cccMember;
        public string CCCMember
        {
            get { return cccMember; }
            set { SetProperty(ref cccMember, value); }
        }

        private string schoolName;
        public string SchoolName
        {
            get { return schoolName; }
            set { SetProperty(ref schoolName, value); }
        }

        private string retargetingHeaderID;
        public string RetargetingHeaderID
        {
            get { return retargetingHeaderID; }
            set { SetProperty(ref retargetingHeaderID, value); }
        }

        private string retargetingDetailID;
        public string RetargetingDetailID
        {
            get { return retargetingDetailID; }
            set { SetProperty(ref retargetingDetailID, value); }
        }

        private SystemCodeDetail _selectedStatus;
        public ObservableRangeCollection<SystemCodeDetail> ClientStatusOptions = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> LoadedClientStatusOptions
        {
            get => ClientStatusOptions;
            set => SetProperty(ref ClientStatusOptions, value);
        }
        public SystemCodeDetail SelectedStatus
        {
            get => _selectedStatus;
            set
            {
                if (this._selectedStatus == value)
                {
                    return;
                }

                this._selectedStatus = value;
                if (this._selectedStatus.Value == "1")
                {
                    IsDisabled = true;
                    IsChronicallyIll = true;
                }
                else
                {
                    IsDisabled = false;
                    IsChronicallyIll = false;
                }

                this.OnPropertyChanged();
            }
        }

        private SystemCodeDetail _selectedPregnant;
        public ObservableRangeCollection<SystemCodeDetail> PregnantOptions = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> LoadedPregnantOptions
        {
            get => PregnantOptions;
            set => SetProperty(ref PregnantOptions, value);
        }
        public SystemCodeDetail SelectedPregnant
        {
            get => _selectedPregnant;
            set
            {
                if (this._selectedPregnant == value)
                {
                    return;
                }

                this._selectedPregnant = value;

                this.OnPropertyChanged();
            }
        }

        private SystemCodeDetail _selectedLactating;
        public ObservableRangeCollection<SystemCodeDetail> LactatingOptions = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> LoadedLactatingOptions
        {
            get => LactatingOptions;
            set => SetProperty(ref LactatingOptions, value);
        }
        public SystemCodeDetail SelectedLactating
        {
            get => _selectedLactating;
            set
            {
                if (this._selectedLactating == value)
                {
                    return;
                }

                this._selectedLactating = value;

                this.OnPropertyChanged();
            }
        }

        private SystemCodeDetail _selectedDisabled;
        public ObservableRangeCollection<SystemCodeDetail> DisabledOptions = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> LoadedDisabledOptions
        {
            get => DisabledOptions;
            set => SetProperty(ref DisabledOptions, value);
        }
        public SystemCodeDetail SelectedDisabled
        {
            get => _selectedDisabled;
            set
            {
                if (this._selectedDisabled == value)
                {
                    return;
                }

                this._selectedDisabled = value;

                this.OnPropertyChanged();
            }
        }

        private SystemCodeDetail _selectedChronicallyIll;
        public ObservableRangeCollection<SystemCodeDetail> ChronicallyIllOptions = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> LoadedChronicallyIllOptions
        {
            get => ChronicallyIllOptions;
            set => SetProperty(ref ChronicallyIllOptions, value);
        }
        public SystemCodeDetail SelectedChronicallyIll
        {
            get => _selectedChronicallyIll;
            set
            {
                if (this._selectedChronicallyIll == value)
                {
                    return;
                }

                this._selectedChronicallyIll = value;

                this.OnPropertyChanged();
            }
        }

        private SystemCodeDetail _selectedNutritionalStatus;
        public ObservableRangeCollection<SystemCodeDetail> NutritionalStatusOptions = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> LoadedNutritionalStatusOptions
        {
            get => NutritionalStatusOptions;
            set => SetProperty(ref NutritionalStatusOptions, value);
        }
        public SystemCodeDetail SelectedNutritionalStatus
        {
            get => _selectedNutritionalStatus;
            set
            {
                if (this._selectedNutritionalStatus == value)
                {
                    return;
                }

                this._selectedNutritionalStatus = value;

                this.OnPropertyChanged();
            }
        }

        private SystemCodeDetail _selectedChildUnderTSForCMAM;
        public ObservableRangeCollection<SystemCodeDetail> ChildUnderTSForCMAMOptions = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> LoadedChildUnderTSForCMAMOptions
        {
            get => ChildUnderTSForCMAMOptions;
            set => SetProperty(ref ChildUnderTSForCMAMOptions, value);
        }
        public SystemCodeDetail SelectedChildUnderTSForCMAM
        {
            get => _selectedChildUnderTSForCMAM;
            set
            {
                if (this._selectedChildUnderTSForCMAM == value)
                {
                    return;
                }

                this._selectedChildUnderTSForCMAM = value;

                this.OnPropertyChanged();
            }
        }

        private SystemCodeDetail _selectedEnrolledInSchool;
        public ObservableRangeCollection<SystemCodeDetail> EnrolledInSchoolOptions = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> LoadedEnrolledInSchoolOptions
        {
            get => EnrolledInSchoolOptions;
            set => SetProperty(ref EnrolledInSchoolOptions, value);
        }
        public SystemCodeDetail SelectedEnrolledInSchool
        {
            get => _selectedEnrolledInSchool;
            set
            {
                if (this._selectedEnrolledInSchool == value)
                {
                    return;
                }

                this._selectedEnrolledInSchool = value;
                if(this._selectedEnrolledInSchool.Value == "YES")
                {
                    IsGrade = true;
                    IsSchoolName = true;
                }
                else
                {
                    IsGrade = false;
                    IsSchoolName = false;
                }

                this.OnPropertyChanged();
            }
        }

        private SystemCodeDetail _selectedGrade;
        public ObservableRangeCollection<SystemCodeDetail> GradeOptions = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> LoadedGradeOptions
        {
            get => GradeOptions;
            set => SetProperty(ref GradeOptions, value);
        }
        public SystemCodeDetail SelectedGrade
        {
            get => _selectedGrade;
            set
            {
                if (this._selectedGrade == value)
                {
                    return;
                }

                this._selectedGrade = value;

                this.OnPropertyChanged();
            }
        }

        public ICommand SaveRetargetingCommand => _saveRetargetingCommand ?? (_saveRetargetingCommand = new Command(async () => await ExecuteSaveRetargeting()));
        private async Task ExecuteSaveRetargeting()
        {
            try
            {
                IsBusy = true;
                Message = "Validating .. ";
                var errorMessage = "";
                var retarget = new Retargeting {
                    RetargetingHeaderID = RetargetingHeaderID,
                    RetargetingDetailID=RetargetingDetailID,
                    SocialWorker = Settings.UserName
                };
                retarget.CCCMember = CCCMember;
                retarget.HouseHoldMemberName = MemberName;
                retarget.CreatedOn = DateTime.Now;
                retarget.RetargetingBy = Convert.ToInt64(AppSettings["UserId"]);
                retarget.RecordStatus = "PENDING SYNC";


                if (CCCMember != null)
                {
                    retarget.CCCMember = CCCMember;
                } else
                {
                    errorMessage += "Please Provide CCC Member\n";
                }

                if(SelectedStatus != null)
                {
                    if (SelectedStatus.Value == "0") {
                        errorMessage += "Please Provide Client Status\n";
                    } else {
                        retarget.Status = SelectedStatus.Name;
                        retarget.StatusId = SelectedStatus.Value;
                    }

                    if (SelectedDisabled.Value == "0" && SelectedStatus.Value == "1") {
                        errorMessage += "Please Provide Disabled\n";
                    } else {
                        retarget.Handicapped = SelectedDisabled.Value;
                    }

                    if (SelectedChronicallyIll.Value == "0" && SelectedStatus.Value == "1") {
                        errorMessage += "Please Provide Chronically Ill\n";
                    } else {
                        retarget.ChronicallyIll = SelectedChronicallyIll.Value;
                    }
                }
                else
                {
                    errorMessage += "Please Provide Client Status\n";
                }
                

                if(SelectedPregnant != null && IsPregnant)
                { 
                    if (SelectedPregnant.Value == "0") {
                        errorMessage += "Please Provide Pregnant\n";
                    } else {
                        retarget.Pregnant = SelectedPregnant.Value;
                    }
                }

                if (SelectedLactating != null && IsLactating)
                {
                    if (SelectedLactating.Value == "0")
                    {
                        errorMessage += "Please Provide Lactating\n";
                    }
                    else
                    {
                        retarget.Lactating = SelectedStatus.Value;
                    }
                }


                if (SelectedNutritionalStatus != null && IsNutritionalStatus)
                {
                    if (SelectedNutritionalStatus.Value == "0")
                    {
                        errorMessage += "Please Provide Nutritional Status\n";
                    }
                    else
                    {
                        retarget.NutritionalStatus = SelectedNutritionalStatus.Value;
                    }
                }

                if (SelectedChildUnderTSForCMAM !=null  && IsChildUnderTSForCMAM)
                {
                    if (SelectedChildUnderTSForCMAM.Value == "0")
                    {
                        errorMessage += "Please Provide Child Under TSF or CMAM\n";
                    }
                    else
                    {
                        retarget.ChildUnderTSForCMAM = SelectedChildUnderTSForCMAM.Value;
                    }
                }

                if (SelectedEnrolledInSchool != null && IsEnrolledInSchool)
                {
                    if (SelectedEnrolledInSchool.Value == "0")
                    {
                        errorMessage += "Please Provide Enrolled In School\n";
                    }
                    else
                    {
                        retarget.EnrolledInSchool = SelectedEnrolledInSchool.Value;
                    }

                    if(SelectedGrade != null && SelectedEnrolledInSchool.Value == "YES" && IsGrade)
                    {
                        if (SelectedGrade.Value == "0")
                        {
                            errorMessage += "Please Provide Grade\n";
                        }
                        else
                        {
                            retarget.Grade = SelectedGrade.Value;
                        }
                    }


                    if (SchoolName == null && SelectedEnrolledInSchool.Value == "YES")
                    {
                        errorMessage += "Please Provide School Name\n";
                    }
                    else
                    {
                        retarget.SchoolName = SchoolName;
                    }
                }

                



                if (errorMessage == "")
                {
                    Message = "Updating Database .. ";

                    App.Database.AddOrUpdate(retarget);

                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Success",
                        Message = "Save successful.",
                        Cancel = "OK"
                    });

                    
                    await Navigation.PushAsync(new RetargetingHouseholdMembersListPage(RetargetingHeaderID));
                }
                else
                {
                    if (errorMessage.Length > 0)
                    {
                        ValidateMessage = $"{ValidateMessage}\n{errorMessage}";
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(
                            MessageKeys.Error,
                            new MessagingServiceAlert
                            {
                                Title = "Please Check the Data and Try Again!!",
                                Message = ValidateMessage,
                                Cancel = "OK"
                            });
                        IsBusy = false;
                    }
                    errorMessage = "";
                }



            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                           MessageKeys.Error,
                           new MessagingServiceAlert
                           {
                               Title = "Please Correct the Data and Try Again!!",
                               Message = e.Message,
                               Cancel = "OK"
                           });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            return;
        }

        public RetargetingCaptureForm7ViewModel(INavigation navigation, string retargetingdetailid, bool edit) : base(navigation)
        {
            client = DependencyService.Get<IAppClient>();
            Navigation = navigation;

            CCCMemberLabel = "CCC Member";
            StatusLabel = "Status";
            PregnantLabel = "Pregnant";
            LactatingLabel = "Lactating";
            DisabledLabel = "Disabled";
            ChronicallyIllLabel = "Chronically Ill";
            NutritionalStatusLabel = "Nutritional Status";
            ChildUnderTSForCMAMLabel = "Child Under TSF or CMAM";
            EnrolledInSchoolLabel = "Enrolled In School";
            GradeLabel = "Grade";
            SchoolNameLabel = "School Name";
            Button = "Save";

            var data = App.Database.GetTableRow("RetargetingMembers", "RetargetingDetailID", retargetingdetailid);
            RetargetingMembers = (RetargetingMembers)data;
            MemberName = RetargetingMembers.HouseHoldMemberName;
            RetargetingHeaderID = RetargetingMembers.RetargetingHeaderID;
            RetargetingDetailID = RetargetingMembers.RetargetingDetailID;

            LoadedClientStatusOptions.AddRange(App.Database.SystemCodeDetailsGetByCode("ClientStatusOptions"));
            LoadedPregnantOptions.AddRange(App.Database.SystemCodeDetailsGetByCode("YesNoOptions"));
            LoadedLactatingOptions.AddRange(App.Database.SystemCodeDetailsGetByCode("YesNoOptions"));
            LoadedDisabledOptions.AddRange(App.Database.SystemCodeDetailsGetByCode("YesNoOptions"));
            LoadedChronicallyIllOptions.AddRange(App.Database.SystemCodeDetailsGetByCode("YesNoOptions"));
            LoadedNutritionalStatusOptions.AddRange(App.Database.SystemCodeDetailsGetByCode("NutritionalStatusOptions"));
            LoadedChildUnderTSForCMAMOptions.AddRange(App.Database.SystemCodeDetailsGetByCode("YesNoOptions"));
            LoadedEnrolledInSchoolOptions.AddRange(App.Database.SystemCodeDetailsGetByCode("YesNoOptions"));
            LoadedGradeOptions.AddRange(App.Database.SystemCodeDetailsGetByCode("SchoolGradeOptions"));

            #region Handle Skip Patterns
            IsPregnant = false;
            IsLactating = false;
            IsDisabled = false;
            IsChronicallyIll = false;
            IsNutritionalStatus = false;
            IsChildUnderTSForCMAM = false;
            IsEnrolledInSchool = false;
            IsGrade = false;
            IsSchoolName = false;

            if (RetargetingMembers.Sex == "F")
            {
                IsPregnant = true;
                IsLactating = true;
            }
            if (RetargetingMembers.Age >= 12 && RetargetingMembers.Age <= 50 && RetargetingMembers.Sex == "F")
            {
                IsPregnant = true;
            }
            if (RetargetingMembers.Age <= 5)
            {
                IsNutritionalStatus = true;
                IsChildUnderTSForCMAM = true;
            }

            if (RetargetingMembers.Age > 5 && RetargetingMembers.Age < 18)
            {
                IsEnrolledInSchool = true;
            }
            #endregion

            AppSettings = GetAppSettings();
        }
    }
}
