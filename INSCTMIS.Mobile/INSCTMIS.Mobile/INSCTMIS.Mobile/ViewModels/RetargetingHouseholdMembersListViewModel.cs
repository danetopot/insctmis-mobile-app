﻿using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Interface;
using INSCTMIS.Mobile.Pages;
using MvvmHelpers;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    class RetargetingHouseholdMembersListViewModel : LocalBaseViewModel
    {
        private IAppClient client;
        public INavigation Navigation;

        public RetargetingHouseholdMembersListViewModel(INavigation navigation, string retargetingheaderid) : base(navigation)
        {
            client = DependencyService.Get<IAppClient>();
            Navigation = navigation;
            RetargetingHeaderID = retargetingheaderid;

            DownloadedHouseholdMembersReadyForRetargeting.ReplaceRange(GetHouseholdMembersReadyForRetargeting(retargetingheaderid));
        }

        private string retargetingHeaderID;
        public string RetargetingHeaderID
        {
            get { return retargetingHeaderID; }
            set { SetProperty(ref retargetingHeaderID, value); }
        }


        private string message;
        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private string messageSync;
        public string MessageSync
        {
            get { return messageSync; }
            set { SetProperty(ref messageSync, value); }
        }

        public ObservableCollection<RetargetingMembers> GetHouseholdMembersReadyForRetargeting(string retargetingheaderid)
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetTableRows("RetargetingMembers", "RetargetingHeaderID", retargetingheaderid);
                var hh = new ObservableCollection<RetargetingMembers>();
                foreach (var item in items)
                {
                    var hhitem = (RetargetingMembers)item;
                    hh.Add(hhitem);
                }
                return hh;

            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Households",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }
        public ObservableRangeCollection<RetargetingMembers> DownloadedHouseholdMembersReadyForRetargeting { get; } = new ObservableRangeCollection<RetargetingMembers>();
        private RetargetingMembers selectedRetargetingHouseholdMemberCommand;
        public RetargetingMembers SelectedRetargetingHouseholdMemberCommand
        {
            get { return selectedRetargetingHouseholdMemberCommand; }
            set
            {
                selectedRetargetingHouseholdMemberCommand = value;
                OnPropertyChanged();
                if (selectedRetargetingHouseholdMemberCommand == null)
                    return;
                Navigation.PushAsync(new RetargetingCaptureForm7Page(selectedRetargetingHouseholdMemberCommand.RetargetingDetailID, false));

                selectedRetargetingHouseholdMemberCommand = null;
            }
        }
    }
}
