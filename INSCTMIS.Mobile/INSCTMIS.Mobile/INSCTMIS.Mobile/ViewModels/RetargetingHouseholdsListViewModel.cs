﻿using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Interface;
using INSCTMIS.Mobile.Pages;
using MvvmHelpers;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    class RetargetingHouseholdsListViewModel : LocalBaseViewModel
    {
        private IAppClient client;
        public INavigation Navigation;

        public RetargetingHouseholdsListViewModel(INavigation navigation) : base(navigation)
        {
            client = DependencyService.Get<IAppClient>();
            Navigation = navigation;

            DownloadedTDSHouseholdsReadyForRetargeting.ReplaceRange(GetTDSHouseholdsReadyForRetargeting(1));
            DownloadedPLWHouseholdsReadyForRetargeting.ReplaceRange(GetPLWHouseholdsReadyForRetargeting(2));
            DownloadedCMCHouseholdsReadyForRetargeting.ReplaceRange(GetCMCHouseholdsReadyForRetargeting(3));
            PendingRetargetingMembersReadyForSync.ReplaceRange(GetPendingRetargetingMembersReadyForSync());
        }

        private string message;
        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private string messageSync;
        public string MessageSync
        {
            get { return messageSync; }
            set { SetProperty(ref messageSync, value); }
        }

        public ObservableCollection<RetargetingHousehold> GetTDSHouseholdsReadyForRetargeting(int household)
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetTableRows("RetargetingHousehold", "ClientCategory", household);
                var hh = new ObservableCollection<RetargetingHousehold>();
                foreach (var item in items)
                {
                    var hhitem = (RetargetingHousehold)item;
                    hh.Add(hhitem);
                }
                return hh;

            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Households",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }
        public ObservableRangeCollection<RetargetingHousehold> DownloadedTDSHouseholdsReadyForRetargeting { get; } = new ObservableRangeCollection<RetargetingHousehold>();
        private RetargetingHousehold selectedRetargetingTDSHouseholdCommand;
        public RetargetingHousehold SelectedRetargetingTDSHouseholdCommand
        {
            get { return selectedRetargetingTDSHouseholdCommand; }
            set
            {
                selectedRetargetingTDSHouseholdCommand = value;
                OnPropertyChanged();
                if (selectedRetargetingTDSHouseholdCommand == null)
                    return;
                Navigation.PushAsync(new RetargetingHouseholdMembersListPage(selectedRetargetingTDSHouseholdCommand.RetargetingHeaderID));

                selectedRetargetingTDSHouseholdCommand = null;
            }
        }

        public ObservableCollection<RetargetingHousehold> GetPLWHouseholdsReadyForRetargeting(int household)
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetTableRows("RetargetingHousehold", "ClientCategory", household);
                var hh = new ObservableCollection<RetargetingHousehold>();
                foreach (var item in items)
                {
                    var hhitem = (RetargetingHousehold)item;
                    hh.Add(hhitem);
                }
                return hh;

            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Households",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }
        public ObservableRangeCollection<RetargetingHousehold> DownloadedPLWHouseholdsReadyForRetargeting { get; } = new ObservableRangeCollection<RetargetingHousehold>();
        private RetargetingHousehold selectedRetargetingPLWHouseholdCommand;
        public RetargetingHousehold SelectedRetargetingPLWHouseholdCommand
        {
            get { return selectedRetargetingPLWHouseholdCommand; }
            set
            {
                selectedRetargetingPLWHouseholdCommand = value;
                OnPropertyChanged();
                if (selectedRetargetingPLWHouseholdCommand == null)
                    return;
                Navigation.PushAsync(new RetargetingHouseholdMembersListPage(selectedRetargetingPLWHouseholdCommand.RetargetingHeaderID));

                selectedRetargetingPLWHouseholdCommand = null;
            }
        }

        public ObservableCollection<RetargetingHousehold> GetCMCHouseholdsReadyForRetargeting(int household)
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetTableRows("RetargetingHousehold", "ClientCategory", household);
                var hh = new ObservableCollection<RetargetingHousehold>();
                foreach (var item in items)
                {
                    var hhitem = (RetargetingHousehold)item;
                    hh.Add(hhitem);
                }
                return hh;

            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Households",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }
        public ObservableRangeCollection<RetargetingHousehold> DownloadedCMCHouseholdsReadyForRetargeting { get; } = new ObservableRangeCollection<RetargetingHousehold>();
        private RetargetingHousehold selectedRetargetingCMCHouseholdCommand;
        public RetargetingHousehold SelectedRetargetingCMCHouseholdCommand
        {
            get { return selectedRetargetingCMCHouseholdCommand; }
            set
            {
                selectedRetargetingCMCHouseholdCommand = value;
                OnPropertyChanged();
                if (selectedRetargetingCMCHouseholdCommand == null)
                    return;
                Navigation.PushAsync(new RetargetingHouseholdMembersListPage(selectedRetargetingCMCHouseholdCommand.RetargetingHeaderID));

                selectedRetargetingCMCHouseholdCommand = null;
            }
        }

        public ObservableCollection<Retargeting> GetPendingRetargetingMembersReadyForSync()
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetTable("Retargeting");
                var hh = new ObservableCollection<Retargeting>();
                if(hh != null)
                {
                    foreach (var item in items)
                    {
                        var hhitem = (Retargeting)item;
                        hh.Add(hhitem);
                    }
                }
                return hh;
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Households",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }
        public ObservableRangeCollection<Retargeting> PendingRetargetingMembersReadyForSync { get; } = new ObservableRangeCollection<Retargeting>();
        private Retargeting selectedRetargetingMembersCommand;
        public Retargeting SelectedRetargetingMembersCommand
        {
            get { return selectedRetargetingMembersCommand; }
            set
            {
                selectedRetargetingMembersCommand = value;
                OnPropertyChanged();
                if (selectedRetargetingMembersCommand == null)
                    return;
                Navigation.PushAsync(new RetargetingCaptureForm7Page(selectedRetargetingMembersCommand.RetargetingDetailID.ToString(), true));

                selectedRetargetingMembersCommand = null;
            }
        }

    }
}
