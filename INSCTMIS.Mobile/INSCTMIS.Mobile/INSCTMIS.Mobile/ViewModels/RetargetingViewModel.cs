﻿using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Interface;
using INSCTMIS.Mobile.Pages;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    class RetargetingViewModel : LocalBaseViewModel
    {
        private IAppClient client;
        public INavigation Navigation;

        public RetargetingViewModel(INavigation navigation) : base(navigation)
        {
            client = DependencyService.Get<IAppClient>();
            Navigation = navigation;
            LoadRetargetingHouseholdText = "Load Retargeting Households";
        }

        private string message;
        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private string messageSync;
        public string MessageSync
        {
            get { return messageSync; }
            set { SetProperty(ref messageSync, value); }
        }

        private string loadRetargetingHouseholdText;
        public string LoadRetargetingHouseholdText
        {
            get { return loadRetargetingHouseholdText; }
            set { SetProperty(ref loadRetargetingHouseholdText, value); }
        }

        private ICommand loadRetargetingHouseholdCommand;
        public ICommand LoadRetargetingHouseholdCommand => loadRetargetingHouseholdCommand ?? (loadRetargetingHouseholdCommand = new Command(async () => await ExecuteLoadRetargetingHouseholdAsync()));
        private async Task ExecuteLoadRetargetingHouseholdAsync()
        {
            if (IsBusy)
            {
                return;
            }
            IsBusy = true;
            MessageSync = "Populating Tables... ...";
            LoadRetargetingHouseholdText = "Loading Retargeting Households . . .";

            try
            {
                await Navigation.PushAsync(new RetargetingHouseholdsListPage());

                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Success",
                    Message = "Retargeting households loaded successfully",
                    Cancel = "OK"
                });
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error",
                    Message = "" + ex.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageSync = string.Empty;
                LoadRetargetingHouseholdText = "Load Retargeting Households";
                IsBusy = false;
                await Task.FromResult(0);
            }

        }
    }
}
