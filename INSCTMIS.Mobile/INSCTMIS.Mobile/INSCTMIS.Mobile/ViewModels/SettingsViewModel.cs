﻿using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Interface;
using INSCTMIS.Mobile.Pages;
using MvvmHelpers;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    class SettingsViewModel : LocalBaseViewModel
    {
        private IAppClient client;
        public INavigation Navigation;
        public Setting settings;

        private ICommand _saveLocationSettingsCommand;
        private ICommand _saveReportingPeriodSettingsCommand;
        private ICommand _saveFiscalYearSettingsCommand;
        private ICommand _saveURLSettingsCommand;

        public SettingsViewModel(INavigation navigation) : base(navigation)
        {
            client = DependencyService.Get<IAppClient>();
            Navigation = navigation;

            TabPageTitle = "Settings";
            Button = "Save";
            ContentPageLocationTitle = "Location";
            ContentPageReportingPeriodTitle = "Reporting Period";
            ContentPageFiscalYearTitle = "Fiscal Year";
            contentPageURLTitle = "Server";
            KebeleLabel = "Kebele";
            ReportingPeriodLabel = "Reporting Period";
            FiscalYearLabel = "Fiscal Year";
            URLLabel = "URL";

            LoadedReportingPeriodOptions.AddRange(App.Database.SystemCodeDetailsGetByCode("ReportingPeriod"));
            LoadedFiscalYearOptions.AddRange(App.Database.SystemCodeDetailsGetByCode("FiscalYear"));
            LoadedKebeleOptions.AddRange(App.Database.GetKebeleDetails());
        }

        private string button;
        public string Button
        {
            get { return button; }
            set { SetProperty(ref button, value); }
        }

        private string message;
        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private string messageSync;
        public string MessageSync
        {
            get { return messageSync; }
            set { SetProperty(ref messageSync, value); }
        }

        private string reportingPeriod;
        public string ReportingPeriod
        {
            get { return reportingPeriod; }
            set { SetProperty(ref reportingPeriod, value); }
        }

        private string fiscalYear;
        public string FiscalYear
        {
            get { return fiscalYear; }
            set { SetProperty(ref fiscalYear, value); }
        }

        private string kebele;
        public string Kebele
        {
            get { return kebele; }
            set { SetProperty(ref kebele, value); }
        }

        private string url;
        public string URL
        {
            get { return url; }
            set { SetProperty(ref url, value); }
        }

        private string reportingPeriodLabel;
        public string ReportingPeriodLabel
        {
            get { return reportingPeriodLabel; }
            set { SetProperty(ref reportingPeriodLabel, value); }
        }

        private string fiscalYearLabel;
        public string FiscalYearLabel
        {
            get { return fiscalYearLabel; }
            set { SetProperty(ref fiscalYearLabel, value); }
        }

        private string kebeleLabel;
        public string KebeleLabel
        {
            get { return kebeleLabel; }
            set { SetProperty(ref kebeleLabel, value); }
        }

        private string urlLabel;
        public string URLLabel
        {
            get { return urlLabel; }
            set { SetProperty(ref urlLabel, value); }
        }

        private string tabPageTitle;
        public string TabPageTitle
        {
            get { return tabPageTitle; }
            set { SetProperty(ref tabPageTitle, value); }
        }

        private string contentPageLocationTitle;
        public string ContentPageLocationTitle
        {
            get { return contentPageLocationTitle; }
            set { SetProperty(ref contentPageLocationTitle, value); }
        }

        private string contentPageReportingPeriodTitle;
        public string ContentPageReportingPeriodTitle
        {
            get { return contentPageReportingPeriodTitle; }
            set { SetProperty(ref contentPageReportingPeriodTitle, value); }
        }

        private string contentPageFiscalYearTitle;
        public string ContentPageFiscalYearTitle
        {
            get { return contentPageFiscalYearTitle; }
            set { SetProperty(ref contentPageFiscalYearTitle, value); }
        }

        private string contentPageURLTitle;
        public string ContentPageURLTitle
        {
            get { return contentPageURLTitle; }
            set { SetProperty(ref contentPageURLTitle, value); }
        }

        private SystemCodeDetail _selectedReportingPeriod;
        public ObservableRangeCollection<SystemCodeDetail> ReportingPeriodOptions = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> LoadedReportingPeriodOptions
        {
            get => ReportingPeriodOptions;
            set => SetProperty(ref ReportingPeriodOptions, value);
        }
        public SystemCodeDetail SelectedReportingPeriod
        {
            get => _selectedReportingPeriod;
            set
            {
                if (this._selectedReportingPeriod == value)
                {
                    return;
                }

                this._selectedReportingPeriod = value;

                this.OnPropertyChanged();
            }
        }

        private SystemCodeDetail _selectedFiscalYear;
        public ObservableRangeCollection<SystemCodeDetail> FiscalYearOptions = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> LoadedFiscalYearOptions
        {
            get => FiscalYearOptions;
            set => SetProperty(ref FiscalYearOptions, value);
        }
        public SystemCodeDetail SelectedFiscalYear
        {
            get => _selectedFiscalYear;
            set
            {
                if (this._selectedFiscalYear == value)
                {
                    return;
                }

                this._selectedFiscalYear = value;

                this.OnPropertyChanged();
            }
        }

        private Kebele _selectedKebele;
        public ObservableRangeCollection<Kebele> KebeleOptions = new ObservableRangeCollection<Kebele>();
        public ObservableRangeCollection<Kebele> LoadedKebeleOptions
        {
            get => KebeleOptions;
            set => SetProperty(ref KebeleOptions, value);
        }
        public Kebele SelectedKebele
        {
            get => _selectedKebele;
            set
            {
                if (this._selectedKebele == value)
                {
                    return;
                }

                this._selectedKebele = value;

                this.OnPropertyChanged();
            }
        }

        public ICommand SaveLocationSettingsCommand => _saveLocationSettingsCommand ?? (_saveLocationSettingsCommand = new Command(async () => await ExecuteSaveLocationSettings()));
        private async Task ExecuteSaveLocationSettings()
        {
            try
            {
                IsBusy = true;
                Message = "Validating .. ";
                var errorMessage = "";

                if (SelectedKebele != null)
                {
                    if (SelectedKebele.KebeleID <= 0)
                    {
                        errorMessage += "Please Provide Kebele\n";
                    }
                }



                if (errorMessage == "")
                {
                    Message = "Updating Database .. ";

                    settings = new Setting();
                    settings.ID = Guid.NewGuid();
                    settings.Name = "Location";
                    settings.Value = (SelectedKebele.KebeleID).ToString();
                    App.Database.AddOrUpdate(settings);

                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Success",
                        Message = "Save successful.",
                        Cancel = "OK"
                    });


                }
                else
                {
                    if (errorMessage.Length > 0)
                    {
                        ValidateMessage = $"{ValidateMessage}\n{errorMessage}";
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(
                            MessageKeys.Error,
                            new MessagingServiceAlert
                            {
                                Title = "Please Check the Data and Try Again!!",
                                Message = ValidateMessage,
                                Cancel = "OK"
                            });
                        IsBusy = false;
                    }
                    errorMessage = "";
                }



            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                           MessageKeys.Error,
                           new MessagingServiceAlert
                           {
                               Title = "Please Correct the Data and Try Again!!",
                               Message = e.Message,
                               Cancel = "OK"
                           });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            return;
        }

        public ICommand SaveReportingPeriodSettingsCommand => _saveReportingPeriodSettingsCommand ?? (_saveReportingPeriodSettingsCommand = new Command(async () => await ExecuteSaveReportingPeriodSettings()));
        private async Task ExecuteSaveReportingPeriodSettings()
        {
            try
            {
                IsBusy = true;
                Message = "Validating .. ";
                var errorMessage = "";

                if (SelectedReportingPeriod != null)
                {
                    if (SelectedReportingPeriod.Value == "0")
                    {
                        errorMessage += "Please Provide Reporting Period\n";
                    }
                }


                if (errorMessage == "")
                {
                    Message = "Updating Database .. ";

                    settings = new Setting();
                    settings.ID = Guid.NewGuid();
                    settings.Name = "ReportingPeriod";
                    settings.Value = SelectedReportingPeriod.Value;
                    App.Database.AddOrUpdate(settings);

                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Success",
                        Message = "Save successful.",
                        Cancel = "OK"
                    });


                }
                else
                {
                    if (errorMessage.Length > 0)
                    {
                        ValidateMessage = $"{ValidateMessage}\n{errorMessage}";
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(
                            MessageKeys.Error,
                            new MessagingServiceAlert
                            {
                                Title = "Please Check the Data and Try Again!!",
                                Message = ValidateMessage,
                                Cancel = "OK"
                            });
                        IsBusy = false;
                    }
                    errorMessage = "";
                }



            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                           MessageKeys.Error,
                           new MessagingServiceAlert
                           {
                               Title = "Please Correct the Data and Try Again!!",
                               Message = e.Message,
                               Cancel = "OK"
                           });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            return;
        }

        public ICommand SaveFiscalYearSettingsCommand => _saveFiscalYearSettingsCommand ?? (_saveFiscalYearSettingsCommand = new Command(async () => await ExecuteSaveFiscalYearSettings()));
        private async Task ExecuteSaveFiscalYearSettings()
        {
            try
            {
                IsBusy = true;
                Message = "Validating .. ";
                var errorMessage = "";

                if(SelectedFiscalYear != null)
                {
                    if (SelectedFiscalYear.Value == "0")
                    {
                        errorMessage += "Please Provide Fiscal Year\n";
                    }
                }


                if (errorMessage == "")
                {
                    Message = "Updating Database .. ";

                    settings = new Setting();
                    settings.ID = Guid.NewGuid();
                    settings.Name = "FiscalYear";
                    settings.Value = SelectedFiscalYear.Value;
                    App.Database.AddOrUpdate(settings);

                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Success",
                        Message = "Save successful.",
                        Cancel = "OK"
                    });


                }
                else
                {
                    if (errorMessage.Length > 0)
                    {
                        ValidateMessage = $"{ValidateMessage}\n{errorMessage}";
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(
                            MessageKeys.Error,
                            new MessagingServiceAlert
                            {
                                Title = "Please Check the Data and Try Again!!",
                                Message = ValidateMessage,
                                Cancel = "OK"
                            });
                        IsBusy = false;
                    }
                    errorMessage = "";
                }



            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                           MessageKeys.Error,
                           new MessagingServiceAlert
                           {
                               Title = "Please Correct the Data and Try Again!!",
                               Message = e.Message,
                               Cancel = "OK"
                           });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            return;
        }

        public ICommand SaveURLSettingsCommand => _saveURLSettingsCommand ?? (_saveURLSettingsCommand = new Command(async () => await ExecuteSaveURLSettings()));
        private async Task ExecuteSaveURLSettings()
        {
            try
            {
                IsBusy = true;
                Message = "Validating .. ";
                var errorMessage = "";

                if (SelectedFiscalYear != null)
                {
                    if (SelectedFiscalYear.Value == "0")
                    {
                        errorMessage += "Please Provide Fiscal Year\n";
                    }
                }


                if (errorMessage == "")
                {
                    Message = "Updating Database .. ";

                    settings = new Setting();
                    settings.ID = Guid.NewGuid();
                    settings.Name = "Server";
                    settings.Value = URL;
                    App.Database.AddOrUpdate(settings);

                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Success",
                        Message = "Save successful.",
                        Cancel = "OK"
                    });


                }
                else
                {
                    if (errorMessage.Length > 0)
                    {
                        ValidateMessage = $"{ValidateMessage}\n{errorMessage}";
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(
                            MessageKeys.Error,
                            new MessagingServiceAlert
                            {
                                Title = "Please Check the Data and Try Again!!",
                                Message = ValidateMessage,
                                Cancel = "OK"
                            });
                        IsBusy = false;
                    }
                    errorMessage = "";
                }



            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                           MessageKeys.Error,
                           new MessagingServiceAlert
                           {
                               Title = "Please Correct the Data and Try Again!!",
                               Message = e.Message,
                               Cancel = "OK"
                           });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            return;
        }
    }
}
