﻿using FormsToolkit;
using INSCTMIS.Mobile.Converters;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Helpers;
using INSCTMIS.Mobile.Interface;
using INSCTMIS.Mobile.Models;
using Plugin.Connectivity;
using Plugin.DeviceInfo;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    public class SyncViewModel : LocalBaseViewModel
    {
        #region Properties

        private IAppClient client;
        public INavigation Navigation;
        private IToast toast;
        public SystemCodeDetail SystemCodeDetail { get; set; }
        Dictionary<string, string> AppSettings = null;

        private string message;
        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private string email;

        public string Email
        {
            get { return email; }
            set { SetProperty(ref email, value); }
        }

        private string password;

        public string Password
        {
            get { return password; }
            set { SetProperty(ref password, value); }
        }

        public SyncViewModel(INavigation navigation) : base(navigation)
        {
            client = DependencyService.Get<IAppClient>();
            toast = DependencyService.Get<IToast>();
            Navigation = navigation;
            email = Settings.Email;
            password = Settings.Password;

            SyncedDownload = Settings.LastSyncDown;
            SyncedUpload = Settings.LastSync;

            SyncDownRegistrationText = "Download Data from Server";
            SyncUpRegistrationText = "Upload  Data To Server";

            SyncDownComplianceText = "Download Data from Server";
            SyncUpComplianceText = "Upload Data To Server";

            SyncDownMonitoringText = "Download Data from Server";
            SyncUpMonitoringText = "Upload Data To Server";

            SyncDownRetargetingText = "Download Data from Server";
            SyncUpRetargetingText = "Upload Data To Server";

            SyncDownISNPText = "Download Data from Server";
            SyncUpISNPText = "Upload Data To Server";
        }

        private int localListing;

        public int LocalListing
        {
            get { return localListing; }
            set { SetProperty(ref localListing, value); }
        }

        private DateTime syncedUpload;

        public DateTime SyncedUpload
        {
            get { return syncedUpload; }
            set { SetProperty(ref syncedUpload, value); }
        }

        private DateTime syncedDownload;

        public DateTime SyncedDownload
        {
            get { return syncedDownload; }
            set { SetProperty(ref syncedDownload, value); }
        }

        #endregion Properties

        #region Sync

        private string syncUpRegistrationText;
        public string SyncUpRegistrationText
        {
            get { return syncUpRegistrationText; }
            set { SetProperty(ref syncUpRegistrationText, value); }
        }

        private string syncDownRegistrationText;
        public string SyncDownRegistrationText
        {
            get { return syncDownRegistrationText; }
            set { SetProperty(ref syncDownRegistrationText, value); }
        }


        private string syncUpComplianceText;
        public string SyncUpComplianceText
        {
            get { return syncUpComplianceText; }
            set { SetProperty(ref syncUpComplianceText, value); }
        }


        private string syncDownComplianceText;
        public string SyncDownComplianceText
        {
            get { return syncDownComplianceText; }
            set { SetProperty(ref syncDownComplianceText, value); }
        }

        private string syncUpMonitoringText;
        public string SyncUpMonitoringText
        {
            get { return syncUpComplianceText; }
            set { SetProperty(ref syncUpComplianceText, value); }
        }

        private string syncDownMonitoringText;
        public string SyncDownMonitoringText
        {
            get { return syncDownMonitoringText; }
            set { SetProperty(ref syncDownMonitoringText, value); }
        }


        private string syncUpRetargetingText;
        public string SyncUpRetargetingText
        {
            get { return syncUpRetargetingText; }
            set { SetProperty(ref syncUpRetargetingText, value); }
        }

        private string syncDownRetargetingText;
        public string SyncDownRetargetingText
        {
            get { return syncDownRetargetingText; }
            set { SetProperty(ref syncDownRetargetingText, value); }
        }

        private string syncUpISNPText;
        public string SyncUpISNPText
        {
            get { return syncUpISNPText; }
            set { SetProperty(ref syncUpISNPText, value); }
        }

        private string syncDownISNPText;
        public string SyncDownISNPText
        {
            get { return syncDownISNPText; }
            set { SetProperty(ref syncDownISNPText, value); }
        }

        private string logRegistration;
        public string LogRegistration
        {
            get { return logRegistration; }
            set { SetProperty(ref logRegistration, value); }
        }

        private string messageSync;
        public string MessageSync
        {
            get { return messageSync; }
            set { SetProperty(ref messageSync, value); }
        }

        private string messageComplianceSync;
        public string MessageComplianceSync
        {
            get { return messageComplianceSync; }
            set { SetProperty(ref messageComplianceSync, value); }
        }

        private string messageMonitoringSync;
        public string MessageMonitoringSync
        {
            get { return messageMonitoringSync; }
            set { SetProperty(ref messageMonitoringSync, value); }
        }
    
        private string messageRetargetingSync;
        public string MessageRetargetingSync
        {
            get { return messageRetargetingSync; }
            set { SetProperty(ref messageRetargetingSync, value); }
        }

        private string messageHouseholdProfileSync;
        public string MessageHouseholdProfileSync
        {
            get { return messageHouseholdProfileSync; }
            set { SetProperty(ref messageHouseholdProfileSync, value); }
        }

        private string messageISNPSync;
        public string MessageISNPSync
        {
            get { return messageISNPSync; }
            set { SetProperty(ref messageISNPSync, value); }
        }

        private string moduleName;
        public string ModuleName
        {
            get { return moduleName; }
            set { SetProperty(ref moduleName, value); }
        }

        private ICommand syncUpRegistrationAsyncCommand;
        public ICommand SyncUpRegistrationAsyncCommand => syncUpRegistrationAsyncCommand ?? (syncUpRegistrationAsyncCommand = new Command(async () => await ExecuteSyncUpRegistrationAsync()));
        private async Task ExecuteSyncUpRegistrationAsync()
        {
            AppSettings = GetAppSettings();

            if (IsBusy)
            {
                return;
            }

            MessageSync = "Uploading data to the server... ...";
            SyncUpRegistrationText = "Uploading Data . . .";
            ModuleName = "Household Profile";

            try
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Sync Failed",
                        Message = "Uh Oh, It looks like you have gone offline. \n" +
                                  "Please check your internet connection and try again.",
                        Cancel = "OK"
                    });
                    return;
                }
                IsBusy = true;

                var device = new UserDevice()
                {
                    ModuleName = ModuleName,
                    AppBuild = CrossDeviceInfo.Current.AppBuild,
                    AppVersion = CrossDeviceInfo.Current.AppVersion,
                    DeviceName = CrossDeviceInfo.Current.DeviceName,
                    DeviceId = CrossDeviceInfo.Current.Id,
                    DeviceManufacturer = CrossDeviceInfo.Current.Manufacturer,
                    DeviceModel = CrossDeviceInfo.Current.Model,
                    Version = CrossDeviceInfo.Current.Version,
                    IsDevice = CrossDeviceInfo.Current.IsDevice,
                    VersionNumber = CrossDeviceInfo.Current.VersionNumber.ToString()
                };

                var errorMessage = "";
                var localTDSData = App.Database.GetRegistrationTableRowsPendingSyncRows<Form1A>("Form1A");
                var localTDSMembersData = App.Database.GetTableRows<HouseholdMember>("HouseholdMember");
                var localPLWData = App.Database.GetRegistrationTableRowsPendingSyncRows<Form1B>("Form1B");
                var localPLWMembersData = App.Database.GetTableRows<Infant>("Infant");
                var localCMCData = App.Database.GetRegistrationTableRowsPendingSyncRows<Form1C>("Form1C");

                #region FORM 1A
                if (localTDSData.Any())
                {
                    int i = 1;
                    foreach (var item in localTDSData)
                    {
                        var ProfileDSHeaderID = Guid.NewGuid();
                        device.DataId = ProfileDSHeaderID;
                        var pdsDetailsListCaptureVm = new List<HouseholdProfileCapturePDSMembersVm>();
                        var pdsHeaderCaptureVm = new HouseholdProfileCapturePDSVm()
                        {
                            ProfileDSHeaderID = ProfileDSHeaderID,
                            ProfileDSDetailID = Guid.NewGuid(),
                            HouseHold = "PDS",
                            KebeleID = item.KebeleID,
                            Gote = item.Gote,
                            Gare = item.Gare,
                            NameOfHouseHoldHead = item.NameOfHouseHoldHead,
                            HouseHoldIDNumber = item.HouseholdIDNumber,
                            Remarks = item.Remarks,
                            CollectionDate = item.CollectionDate,
                            SocialWorker = item.SocialWorker,
                            CCCCBSPCMember = item.CCCCBSPCMember,
                            CBHIMembership = item.CBHIMembership,
                            CBHINumber = item.CBHINumber,
                            CreatedBy = item.CreatedBy,
                            CreatedOn = item.CreatedOn,
                            UserDevice = device
                        };

                        var pdsDetails = localTDSMembersData.Where(x => x.Form1AId == item.Id);
                        if(pdsDetails.Count() > 0)
                        {
                            foreach (var pdsDetail in pdsDetails)
                            {
                                var pdsDetailCaptureVm = new HouseholdProfileCapturePDSMembersVm()
                                {
                                    ProfileDSHeaderID = ProfileDSHeaderID,
                                    ProfileDSDetailID = Guid.NewGuid(),
                                    HouseHoldMemberName = pdsDetail.HouseHoldMemberName,
                                    IndividualID = pdsDetail.IndividualID,
                                    Pregnant = pdsDetail.Pregnant,
                                    Lactating = pdsDetail.Lactating,
                                    DateOfBirth = pdsDetail.DateOfBirth,
                                    Age = pdsDetail.Age,
                                    Sex = pdsDetail.Sex,
                                    PWL = null,
                                    Handicapped = pdsDetail.Handicapped,
                                    ChronicallyIll = pdsDetail.ChronicallyIll,
                                    NutritionalStatus = pdsDetail.NutritionalStatus,
                                    ChildUnderTSForCMAM = pdsDetail.childUnderTSForCMAM,
                                    EnrolledInSchool = pdsDetail.EnrolledInSchool,
                                    Grade = pdsDetail.Grade,
                                    SchoolName = pdsDetail.SchoolName,
                                    ChildProtectionRisk = pdsDetail.ChildProtectionRisk
                                };

                                pdsDetailsListCaptureVm.Add(pdsDetailCaptureVm);
                            }
                        }
                        else
                        {
                            pdsDetailsListCaptureVm = null;
                        }
                        

                        var householdProfileVm = new HouseholdProfileVm
                        {
                            PDSHousehold = pdsHeaderCaptureVm,
                            PDSHouseholdMembers = pdsDetailsListCaptureVm,
                            PLWHousehold = null,
                            PLWHouseholdMembers = null,
                            CMCHousehold = null,
                        };
                        MessageHouseholdProfileSync = $"Processing PDS Household #{i} out of {localTDSData.Count} ... ";
                        i++;

                        var feedback = await client.PostHouseholdProfiles(householdProfileVm);
                        if (feedback.StatusId == null || feedback.StatusId == -1)
                        {
                            errorMessage += $"{feedback.Description}\n";
                            System.Threading.Thread.Sleep(50);
                        }
                        else
                        {
                            item.Status = "SYNCED";
                            App.Database.AddOrUpdate(item);
                        }
                    }
                }
                #endregion

                #region FORM 1B
                if (localPLWData.Any())
                {
                    int i = 1;
                    foreach (var item in localPLWData)
                    {
                        var ProfileDSHeaderID = Guid.NewGuid();
                        device.DataId = ProfileDSHeaderID;
                        var plwDetailsListCaptureVm = new List<HouseholdProfileCapturePLWMembersVm>();
                        var pdsHeaderCaptureVm = new HouseholdProfileCapturePLWVm()
                        {
                            ProfileDSHeaderID = ProfileDSHeaderID,
                            ProfileDSDetailID = Guid.NewGuid(),
                            HouseHold = "PLW",
                            KebeleID = item.KebeleID,
                            Gote = item.Gote,
                            Gare = item.Gare,
                            CollectionDate = item.CollectionDate,
                            SocialWorker = item.SocialWorker,
                            CCCCBSPCMember = item.CCCCBSPCMember,
                            PLW = item.PLW,
                            NameOfPLW = item.NameOfPLW,
                            HouseHoldIDNumber = item.HouseHoldIDNumber,
                            MedicalRecordNumber = item.MedicalRecordNumber,
                            PLWAge = item.PLWAge,
                            NutritionalStatusPLW = item.NutritionalStatusPLW,
                            StartDateTDS = item.StartDateTDS,
                            EndDateTDS = item.EndDateTDS,
                            Remarks = item.Remarks,
                            ChildProtectionRisk = item.ChildProtectionRisk,
                            CBHIMembership = item.CBHIMembership,
                            CBHINumber = item.CBHINumber,
                            CreatedBy = item.CreatedBy,
                            CreatedOn = item.CreatedOn,
                            UserDevice = device
                        };

                        var plwDetails = localPLWMembersData.Where(x => x.Form1BId == item.Id); 
                        if (plwDetails.Count() > 0)
                        {
                            foreach (var plwDetail in plwDetails)
                            {
                                var plwDetailCaptureVm = new HouseholdProfileCapturePLWMembersVm()
                                {
                                    ProfileDSHeaderID = ProfileDSHeaderID,
                                    ProfileDSDetailID = Guid.NewGuid(),
                                    BabyDateOfBirth = plwDetail.BabyDateOfBirth,
                                    BabyName = plwDetail.BabyName,
                                    BabySex = plwDetail.BabySex,
                                    NutritionalStatusInfant = plwDetail.NutritionalStatusInfant
                                };

                                plwDetailsListCaptureVm.Add(plwDetailCaptureVm);
                            }
                        }
                        else
                        {
                            plwDetailsListCaptureVm = null;
                        }

                        var householdProfileVm = new HouseholdProfileVm
                        {
                            PDSHousehold = null,
                            PDSHouseholdMembers = null,
                            PLWHousehold = pdsHeaderCaptureVm,
                            PLWHouseholdMembers = plwDetailsListCaptureVm,
                            CMCHousehold = null,
                        };
                        MessageHouseholdProfileSync = $"Processing PLW Household #{i} out of {localPLWData.Count} ... ";
                        i++;

                        var feedback = await client.PostHouseholdProfiles(householdProfileVm);
                        if (feedback.StatusId == null || feedback.StatusId == -1)
                        {
                            errorMessage += $"{feedback.Description}\n";
                        }
                        else
                        {
                            item.Status = "SYNCED";
                            App.Database.AddOrUpdate(item);
                        }
                    }
                }
                #endregion

                #region FORM 1C
                if (localCMCData.Any())
                {
                    int i = 1;
                    foreach (var item in localCMCData)
                    {
                        var ProfileDSHeaderID = Guid.NewGuid();

                        device.DataId = ProfileDSHeaderID;
                        var CMCDetailCaptureVm = new HouseholdProfileCaptureCMCVm();
                        var CMCHeaderCaptureVm = new HouseholdProfileCaptureCMCVm()
                        {
                            ProfileDSHeaderID = ProfileDSHeaderID,
                            HouseHold = "CMC",
                            KebeleID = item.KebeleID,
                            Gote = item.Gote,
                            Gare = item.Gare,
                            CollectionDate = item.CollectionDate,
                            SocialWorker = item.SocialWorker,
                            CCCCBSPCMember = item.CCCCBSPCMember,
                            CBHIMembership = item.CBHIMembership,
                            CBHINumber = item.CBHINumber,
                            CreatedBy = item.CreatedBy,
                            NameOfCareTaker = item.NameOfCareTaker,
                            CaretakerID = item.CaretakerID,
                            HouseHoldIDNumber = item.HouseHoldIDNumber,
                            ChildID = item.ChildID,
                            MalnourishedChildName = item.MalnourishedChildName,
                            MalnourishedChildSex = item.MalnourishedChildSex,
                            ChildDateOfBirth = item.ChildDateOfBirth,
                            DateTypeCertificate = item.DateTypeCertificate,
                            MalnourishmentDegree = item.MalnourishmentDegree,
                            StartDateTDS = item.StartDateTDS,
                            NextCNStatusDate = item.NextCNStatusDate,
                            EndDateTDS = item.EndDateTDS,
                            Remarks = item.Remarks,
                            ChildProtectionRisk = item.ChildProtectionRisk,
                            CreatedOn = item.CreatedOn,
                            UserDevice = device
                        };

                        var householdProfileVm = new HouseholdProfileVm
                        {
                            PDSHousehold = null,
                            PDSHouseholdMembers = null,
                            PLWHousehold = null,
                            PLWHouseholdMembers = null,
                            CMCHousehold = CMCHeaderCaptureVm,
                        };
                        MessageHouseholdProfileSync = $"Processing CMC Household #{i} out of {localCMCData.Count} ... ";
                        i++;

                        var feedback = await client.PostHouseholdProfiles(householdProfileVm);
                        if (feedback.StatusId == null || feedback.StatusId == -1)
                        {
                            errorMessage += $"{feedback.Description}\n";
                        }
                        else
                        {
                            item.Status = "SYNCED";
                            App.Database.AddOrUpdate(item);
                        }
                    }
                }
                #endregion

                if (errorMessage == "")
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Success",
                        Message = "Sync Successful",
                        Cancel = "OK"
                    });
                }
                else
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(
                        MessageKeys.Error,
                        new MessagingServiceAlert
                        {
                            Title = "Error",
                            Message = $"{errorMessage}",
                            Cancel = "OK"
                        });
                }
                
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Sync",
                    Message = "The Sync failed. Please try again later.\n\n" + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageSync = string.Empty;
                MessageHouseholdProfileSync = string.Empty;
                SyncUpRegistrationText = "Upload Data to Server";
                IsBusy = false;
                await Task.FromResult(0);
            }

        }

        private ICommand syncUpComplianceAsyncCommand;
        public ICommand SyncUpComplianceAsyncCommand => syncUpComplianceAsyncCommand ?? (syncUpComplianceAsyncCommand = new Command(async () => await ExecuteSyncUpComplianceAsync()));
        private async Task ExecuteSyncUpComplianceAsync()
        {
            AppSettings = GetAppSettings();

            if (IsBusy)
            {
                return;
            }

            MessageSync = "Uploading data to the server... ...";
            SyncUpComplianceText = "Uploading Data . . .";
            ModuleName = "Compliance";

            try
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Sync Failed",
                        Message = "Uh Oh, It looks like you have gone offline. \n" +
                                  "Please check your internet connection and try again.",
                        Cancel = "OK"
                    });
                    return;
                }
                IsBusy = true;

                var device = new UserDevice()
                {
                    ModuleName = ModuleName,
                    AppBuild = CrossDeviceInfo.Current.AppBuild,
                    AppVersion = CrossDeviceInfo.Current.AppVersion,
                    DeviceName = CrossDeviceInfo.Current.DeviceName,
                    DeviceId = CrossDeviceInfo.Current.Id,
                    DeviceManufacturer = CrossDeviceInfo.Current.Manufacturer,
                    DeviceModel = CrossDeviceInfo.Current.Model,
                    Version = CrossDeviceInfo.Current.Version,
                    IsDevice = CrossDeviceInfo.Current.IsDevice,
                    VersionNumber = CrossDeviceInfo.Current.VersionNumber.ToString()
                };



                if (AppSettings["Location"] != null && AppSettings["ReportingPeriod"] != null)
                {
                    var errorMessage = "";
                    var localTDSData = App.Database.GetComplianceTableRowsPendingSyncRows<ComplianceTDSMembers>("ComplianceTDSMembers");
                    var localPLWData = App.Database.GetComplianceTableRowsPendingSyncRows<ComplianceTDSPLWMembers>("ComplianceTDSPLWMembers");
                    var localCMCData = App.Database.GetComplianceTableRowsPendingSyncRows<ComplianceTDSCMCMembers>("ComplianceTDSCMCMembers");

                    if (localTDSData.Any())
                    {
                        int i = 1;
                        foreach (var item in localTDSData)
                        {
                            device.DataId = item.ProfileDSDetailID;
                            var complianceCaptureVm = new ComplianceCaptureVm()
                            {
                                KebeleID = item.KebeleID,
                                ReportingPeriodID = item.ReportingPeriodID,
                                IndividualProfileID = item.ProfileDSDetailID,
                                ServiceID = item.ServiceID,
                                HouseholdType = "PDS",
                                HasComplied = item.HasComplied,
                                Remarks = item.Remarks,
                                CreatedById = Convert.ToInt32(AppSettings["UserId"]),
                                UserDevice = device
                            };

                            MessageComplianceSync = $"Processing PDS Record Ref.#{i} out of {localTDSData.Count} ... ";
                            i++;

                            var feedback = await client.PostHouseholdsReadyForCompliance(complianceCaptureVm);
                            if (feedback.StatusId == null || feedback.StatusId == -1)
                            {
                                errorMessage += $"{feedback.Description}";
                            }
                            else
                            {
                                item.Status = "SYNCED";
                                App.Database.AddOrUpdate(item);
                            }
                        }
                    }

                   

                    if (errorMessage == "")
                    {
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                        {
                            Title = "Success",
                            Message = "Sync Successful",
                            Cancel = "OK"
                        });
                    }
                    else
                    {
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(
                            MessageKeys.Error,
                            new MessagingServiceAlert
                            {
                                Title = "Error",
                                Message =$"Sync Error \n" + errorMessage,
                                Cancel = "OK"
                            });
                        IsBusy = false;
                    }
                }
                else
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Unable to Sync",
                        Message = "Please Configure Kebele and/or Reporting Period",
                        Cancel = "OK"
                    });
                }


            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Sync",
                    Message = "The Sync failed. Please try again later.\n\n" + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageSync = string.Empty;
                MessageComplianceSync = string.Empty;
                SyncUpComplianceText = "Upload Data to Server";
                IsBusy = false;
                await Task.FromResult(0);
            }

        }

        private ICommand syncDownComplianceAsyncCommand;
        public ICommand SyncDownComplianceAsyncCommand => syncDownComplianceAsyncCommand ?? (syncDownComplianceAsyncCommand = new Command(async () => await ExecuteSyncDownComplianceAsync()));
        private async Task ExecuteSyncDownComplianceAsync()
        {
            AppSettings = GetAppSettings();

            if (IsBusy)
            {
                return;
            }
                
            ComplianceResponse complianceResponse = null;
            MessageSync = "Pulling data from the server... ...";
            SyncDownComplianceText = "Downloading Data . . .";


            try
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Sync Failed",
                        Message = "Uh Oh, It looks like you have gone offline. \n" +
                                  "Please check your internet connection and try again.",
                        Cancel = "OK"
                    });
                    return;
                }
                IsBusy = true;

                if (AppSettings["Location"] != null && AppSettings["ReportingPeriod"] != null)
                {

                    QueryParameter queryParameter = new QueryParameter
                    {
                        KebeleID = AppSettings["Location"],
                        ReportingPeriodID = AppSettings["ReportingPeriod"]
                    };

                    complianceResponse = await client.GetHouseholdsReadyForCompliance(queryParameter);

                    if (complianceResponse.TDSHouseholds != null)
                    {
                        foreach (var item in complianceResponse.TDSHouseholds)
                        {
                            App.Database.AddOrUpdate(item);
                        }

                        foreach (var item in complianceResponse.TDSHouseholdMembers)
                        {
                            var data = App.Database.GetTableRows("ComplianceTDSMembers", "ProfileDSDetailID", item.ProfileDSDetailID);
                            if(data.Count == 0)
                            {
                                App.Database.AddOrUpdate(item);
                            }
                        }
                    }

                    if (complianceResponse.TDSPLWHouseholds != null)
                    {
                        foreach (var item in complianceResponse.TDSPLWHouseholds)
                        {
                            App.Database.AddOrUpdate(item);
                        }

                        foreach (var item in complianceResponse.TDSPLWHouseholdMembers)
                        {
                            string _plw = item.PLW;
                            if (_plw.Equals("P"))
                            {
                                item.BabyName = "N/A";
                                item.BabySex = "N/A";
                            }

                            var data = App.Database.GetTableRows("ComplianceTDSPLWMembers", "ProfileTDSPLWID", item.ProfileTDSPLWID);
                            if (data.Count == 0)
                            {
                                App.Database.AddOrUpdate(item);
                            }
                        }
                    }

                    if (complianceResponse.TDSCMCHouseholds != null)
                    {
                        foreach (var item in complianceResponse.TDSCMCHouseholds)
                        {
                            App.Database.AddOrUpdate(item);
                        }

                        foreach (var item in complianceResponse.TDSCMCHouseholdMembers)
                        {
                            var data = App.Database.GetTableRows("ComplianceTDSCMCMembers", "ProfileTDSCMCID", item.ProfileTDSCMCID);
                            if (data.Count == 0)
                            {
                                App.Database.AddOrUpdate(item);
                            }
                        }
                    }

                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Success",
                        Message = "Data downloaded successfully",
                        Cancel = "OK"
                    });
                }
                else
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Unable to Sync",
                        Message = "Please Configure Kebele and/or Reporting Period",
                        Cancel = "OK"
                    });
                }

                
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Sync",
                    Message = "The Sync failed. Please try again later.\n\n" + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageSync = string.Empty;
                SyncDownComplianceText = "Download Data from Server";
                IsBusy = false;
                await Task.FromResult(0);
            }

        }

        private ICommand syncUpMonitoringCommand;
        public ICommand SyncUpMonitoringCommand => syncUpMonitoringCommand ?? (syncUpMonitoringCommand = new Command(async () => await ExecuteSyncUpMonitoringAsync()));
        private async Task ExecuteSyncUpMonitoringAsync()
        {
            AppSettings = GetAppSettings();

            if (IsBusy)
            {
                return;
            }

            MessageSync = "Uploading data to the server... ...";
            SyncUpMonitoringText = "Uploading Data . . .";
            ModuleName = "Monitoring";

            try
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Sync Failed",
                        Message = "Uh Oh, It looks like you have gone offline. \n" +
                                  "Please check your internet connection and try again.",
                        Cancel = "OK"
                    });
                    return;
                }
                IsBusy = true;

                var device = new UserDevice()
                {
                    ModuleName = ModuleName,
                    AppBuild = CrossDeviceInfo.Current.AppBuild,
                    AppVersion = CrossDeviceInfo.Current.AppVersion,
                    DeviceName = CrossDeviceInfo.Current.DeviceName,
                    DeviceId = CrossDeviceInfo.Current.Id,
                    DeviceManufacturer = CrossDeviceInfo.Current.Manufacturer,
                    DeviceModel = CrossDeviceInfo.Current.Model,
                    Version = CrossDeviceInfo.Current.Version,
                    IsDevice = CrossDeviceInfo.Current.IsDevice,
                    VersionNumber = CrossDeviceInfo.Current.VersionNumber.ToString()
                };



                if (AppSettings["Location"] != null && AppSettings["ReportingPeriod"] != null)
                {
                    var errorMessage = "";
                    var localMonitoringData = App.Database.GetMonitoringTableRowsPendingSyncRows<Monitoring>("Monitoring");

                    if (localMonitoringData.Any())
                    {
                        int i = 1;
                        foreach (var item in localMonitoringData)
                        {
                            device.DataId = Guid.Parse(item.ProfileDetailID);
                            var monitoringCaptureVm = new MonitoringCaptureVm()
                            {
                                IndividualProfileID = Guid.Parse(item.ProfileDetailID),
                                KebeleID = item.KebeleID,
                                ReportingPeriodID = item.ReportingPeriodID,
                                CompletedDate = item.CompletedDate,
                                NonComplianceReason = item.NonComplianceReasonID,
                                ActionResponse = item.ActionResponseID,
                                HouseholdType = item.Household,
                                SocialWorker = item.SocialWorker,
                                CreatedById = item.MonitoringBy,
                                UserDevice = device
                            };

                            MessageMonitoringSync = $"Processing Record Ref.#{i} out of {localMonitoringData.Count} ... ";
                            i++;

                            var feedback = await client.PostHouseholdsReadyForMonitoring(monitoringCaptureVm);
                            if (feedback.StatusId == null || feedback.StatusId == -1)
                            {
                                errorMessage = $"{feedback.Description}";
                            }
                            else
                            {
                                item.Status = "SYNCED";
                                App.Database.AddOrUpdate(item);
                            }
                        }
                    }


                    if (errorMessage == "")
                    {
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                        {
                            Title = "Success",
                            Message = "Sync Successful",
                            Cancel = "OK"
                        });
                    }
                    else
                    {
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(
                            MessageKeys.Error,
                            new MessagingServiceAlert
                            {
                                Title = "Error",
                                Message = $"Sync Error \n" + errorMessage,
                                Cancel = "OK"
                            });
                        IsBusy = false;
                    }
                }
                else
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Unable to Sync",
                        Message = "Please Configure Kebele and/or Reporting Period",
                        Cancel = "OK"
                    });
                }


            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Sync",
                    Message = "The Sync failed. Please try again later.\n\n" + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageSync = string.Empty;
                MessageMonitoringSync = string.Empty;
                SyncUpMonitoringText = "Upload Data to Server";
                IsBusy = false;
                await Task.FromResult(0);
            }
        }

        private ICommand syncDownMonitoringCommand;
        public ICommand SyncDownMonitoringCommand => syncDownMonitoringCommand ?? (syncDownMonitoringCommand = new Command(async () => await ExecuteSyncDownMonitoringAsync()));
        private async Task ExecuteSyncDownMonitoringAsync()
        {
            AppSettings = GetAppSettings();

            if (IsBusy)
            {
                return;
            }

            MonitoringResponse monitoringResponse = null;
            MessageSync = "Pulling data from the server... ...";
            SyncDownMonitoringText = "Downloading Data . . .";

            try
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Sync Failed",
                        Message = "Uh Oh, It looks like you have gone offline. \n" +
                                  "Please check your internet connection and try again.",
                        Cancel = "OK"
                    });
                    return;
                }
                IsBusy = true;

                if (AppSettings["Location"] != null && AppSettings["ReportingPeriod"] != null)
                {

                    QueryParameter queryParameter = new QueryParameter
                    {
                        KebeleID = AppSettings["Location"],
                        ReportingPeriodID = AppSettings["ReportingPeriod"]
                    };

                    monitoringResponse = await client.GetHouseholdsReadyForMonitoring(queryParameter);

                    if (monitoringResponse.TDS1Households != null)
                    {
                        foreach (var item in monitoringResponse.TDS1Households)
                        {
                            App.Database.AddOrUpdate(item);
                        }
                        foreach (var item in monitoringResponse.TDS1HouseholdMembers)
                        {
                            App.Database.AddOrUpdate(item);
                        }

                    }

                    if (monitoringResponse.TDS2Households != null)
                    {
                        foreach (var item in monitoringResponse.TDS2Households)
                        {
                            App.Database.AddOrUpdate(item);
                        }
                        foreach (var item in monitoringResponse.TDS2HouseholdMembers)
                        {
                            App.Database.AddOrUpdate(item);
                        }

                    }

                    if (monitoringResponse.PLWHouseholds != null)
                    {
                        foreach (var item in monitoringResponse.PLWHouseholds)
                        {
                            App.Database.AddOrUpdate(item);
                        }
                        foreach (var item in monitoringResponse.PLWHouseholdMembers)
                        {
                            App.Database.AddOrUpdate(item);
                        }

                    }

                    if (monitoringResponse.CMCHouseholds != null)
                    {
                        foreach (var item in monitoringResponse.CMCHouseholds)
                        {
                            App.Database.AddOrUpdate(item);
                        }
                        foreach (var item in monitoringResponse.CMCHouseholdMembers)
                        {
                            App.Database.AddOrUpdate(item);
                        }

                    }

                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Success",
                        Message = "Data downloaded successfully",
                        Cancel = "OK"
                    });
                }
                else
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Unable to Sync",
                        Message = "Please Configure Kebele and/or Reporting Period",
                        Cancel = "OK"
                    });
                }
            }
            catch (Exception)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Sync",
                    Message = "The Sync failed. Please try again later.",
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageSync = string.Empty;
                SyncDownMonitoringText = "Download Data from Server";
                IsBusy = false;
                await Task.FromResult(0);
            }

        }


        private ICommand syncUpRetargetingCommand;
        public ICommand SyncUpRetargetingCommand => syncUpRetargetingCommand ?? (syncUpRetargetingCommand = new Command(async () => await ExecuteSyncUpRetargetingAsync()));
        private async Task ExecuteSyncUpRetargetingAsync()
        {
            AppSettings = GetAppSettings();

            if (IsBusy)
            {
                return;
            }

            MessageSync = "Uploading data to the server... ...";
            SyncUpRetargetingText = "Uploading Data . . .";
            ModuleName = "Retargeting";

            try
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Sync Failed",
                        Message = "Uh Oh, It looks like you have gone offline. \n" +
                                  "Please check your internet connection and try again.",
                        Cancel = "OK"
                    });
                    return;
                }
                IsBusy = true;

                var device = new UserDevice()
                {
                    ModuleName = ModuleName,
                    AppBuild = CrossDeviceInfo.Current.AppBuild,
                    AppVersion = CrossDeviceInfo.Current.AppVersion,
                    DeviceName = CrossDeviceInfo.Current.DeviceName,
                    DeviceId = CrossDeviceInfo.Current.Id,
                    DeviceManufacturer = CrossDeviceInfo.Current.Manufacturer,
                    DeviceModel = CrossDeviceInfo.Current.Model,
                    Version = CrossDeviceInfo.Current.Version,
                    IsDevice = CrossDeviceInfo.Current.IsDevice,
                    VersionNumber = CrossDeviceInfo.Current.VersionNumber.ToString()
                };
                

                if (AppSettings["Location"] != null && AppSettings["FiscalYear"] != null)
                {
                    var errorMessage = "";
                    var localRetargetingData = App.Database.GetRetargetingTableRowsPendingSyncRows<Retargeting>("Retargeting");

                    if (localRetargetingData.Any())
                    {
                        int i = 1;
                        foreach (var item in localRetargetingData)
                        {
                            device.DataId = Guid.Parse(item.RetargetingDetailID);
                            var retargetingCaptureVm = new RetargetingCaptureVm()
                            {
                                RetargetingHeaderID = item.RetargetingHeaderID,
                                RetargetingDetailID = item.RetargetingDetailID,
                                SocialWorker = item.SocialWorker,
                                CCCMember = item.CCCMember,
                                Status = item.StatusId,
                                Pregnant = item.Pregnant,
                                Lactating = item.Lactating,
                                Handicapped = item.Handicapped,
                                ChronicallyIll = item.ChronicallyIll,
                                NutritionalStatus = item.NutritionalStatus,
                                ChildUnderTSForCMAM = item.ChildUnderTSForCMAM,
                                EnrolledInSchool = item.EnrolledInSchool,
                                SchoolName = item.SchoolName,
                                Grade = item.Grade,
                                DateUpdated = item.CreatedOn,
                                IsUpdated = true,
                                CreatedBy = item.RetargetingBy,
                                UserDevice = device
                            };

                            MessageMonitoringSync = $"Processing Record Ref.#{i} out of {localRetargetingData.Count} ... ";
                            i++;

                            var feedback = await client.PostHouseholdsReadyForRetargeting(retargetingCaptureVm);
                            if (feedback.StatusId == null || feedback.StatusId == -1)
                            {
                                errorMessage = $"{feedback.Description}";
                            }
                            else
                            {
                                item.Status = "SYNCED";
                                App.Database.AddOrUpdate(item);
                            }
                        }
                    }


                    if (errorMessage == "")
                    {
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                        {
                            Title = "Success",
                            Message = "Sync Successful",
                            Cancel = "OK"
                        });
                    }
                    else
                    {
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(
                            MessageKeys.Error,
                            new MessagingServiceAlert
                            {
                                Title = "Error",
                                Message = $"Sync Error \n" + errorMessage,
                                Cancel = "OK"
                            });
                        IsBusy = false;
                    }
                }
                else
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Unable to Sync",
                        Message = "Please Configure Kebele and/or FiscalYear",
                        Cancel = "OK"
                    });
                }


            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Sync",
                    Message = "The Sync failed. Please try again later.\n\n" + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageSync = string.Empty;
                MessageRetargetingSync = string.Empty;
                SyncUpRetargetingText = "Upload Data to Server";
                IsBusy = false;
                await Task.FromResult(0);
            }

        }

        private ICommand syncDownRetargetingCommand;
        public ICommand SyncDownRetargetingCommand => syncDownRetargetingCommand ?? (syncDownRetargetingCommand = new Command(async () => await ExecuteSyncDownRetargetingAsync()));
        private async Task ExecuteSyncDownRetargetingAsync()
        {
            AppSettings = GetAppSettings();

            if (IsBusy)
            {
                return;
            }

            RetargetingResponse retargetingResponse = null;
            MessageSync = "Pulling data from the server... ...";
            SyncDownRetargetingText = "Downloading Data . . .";

            try
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Sync Failed",
                        Message = "Uh Oh, It looks like you have gone offline. \n" +
                                  "Please check your internet connection and try again.",
                        Cancel = "OK"
                    });
                    return;
                }
                IsBusy = true;

                if (AppSettings["Location"] != null && AppSettings["FiscalYear"] != null)
                {
                    QueryParameter2 queryParameter = new QueryParameter2
                    {
                        KebeleID = AppSettings["Location"],
                        FiscalYear = AppSettings["FiscalYear"]
                    };

                    retargetingResponse = await client.GetHouseholdsReadyForRetargeting(queryParameter);

                    if (retargetingResponse != null)
                    {

                        if (retargetingResponse.RetargetingHouseholds != null)
                        {

                            foreach (var item in retargetingResponse.RetargetingHouseholds)
                            {
                                App.Database.AddOrUpdate(item);
                            }

                            foreach (var item in retargetingResponse.RetargetingHouseholdMembers)
                            {
                                App.Database.AddOrUpdate(item);
                            }
                        }


                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                        {
                            Title = "Success",
                            Message = "Data downloaded successfully",
                            Cancel = "OK"
                        });
                    }
                }
                else
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Unable to Sync",
                        Message = "Please Configure Kebele and/or FiscalYear",
                        Cancel = "OK"
                    });
                }



            }
            catch (Exception)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Sync",
                    Message = "The Sync failed. Please try again later.",
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageSync = string.Empty;
                SyncDownRetargetingText = "Download Data from Server";
                IsBusy = false;
                await Task.FromResult(0);
            }

        }

        private ICommand syncUpISNPCommand;
        public ICommand SyncUpISNPCommand => syncUpISNPCommand ?? (syncUpISNPCommand = new Command(async () => await ExecuteSyncUpISNPAsync()));
        private async Task ExecuteSyncUpISNPAsync()
        {
            AppSettings = GetAppSettings();

            if (IsBusy)
            {
                return;
            }

            MessageSync = "Uploading data to the server... ...";
            SyncUpMonitoringText = "Uploading Data . . .";
            ModuleName = "ISNP";

            try
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Sync Failed",
                        Message = "Uh Oh, It looks like you have gone offline. \n" +
                                  "Please check your internet connection and try again.",
                        Cancel = "OK"
                    });
                    return;
                }
                IsBusy = true;

                var device = new UserDevice()
                {
                    ModuleName = ModuleName,
                    AppBuild = CrossDeviceInfo.Current.AppBuild,
                    AppVersion = CrossDeviceInfo.Current.AppVersion,
                    DeviceName = CrossDeviceInfo.Current.DeviceName,
                    DeviceId = CrossDeviceInfo.Current.Id,
                    DeviceManufacturer = CrossDeviceInfo.Current.Manufacturer,
                    DeviceModel = CrossDeviceInfo.Current.Model,
                    Version = CrossDeviceInfo.Current.Version,
                    IsDevice = CrossDeviceInfo.Current.IsDevice,
                    VersionNumber = CrossDeviceInfo.Current.VersionNumber.ToString()
                };

                var errorMessage = "";
                var localCPCaseHeaderData = App.Database.GetISNPTableRowsPendingSyncRows<ComplianceCPCaseHeader>("ComplianceCPCaseHeader");
                var localCPCaseDetailData = App.Database.GetISNPTableRowsPendingSyncRows<ComplianceCPCaseDetail>("ComplianceCPCaseDetail");
                var localCaseVisitHeaderData = App.Database.GetISNPTableRowsPendingSyncRows<ComplianceCPCaseVisitHeader>("ComplianceCPCaseVisitHeader");
                var localCPCaseVisitDetailData = App.Database.GetISNPTableRowsPendingSyncRows<ComplianceCPCaseVisitDetail>("ComplianceCPCaseVisitDetail");


                var complianceCPCaseHeaderCaptureVm = new ComplianceCPCaseHeaderCaptureVm();
                var complianceCPCaseDetailCaptureVm = new ComplianceCPCaseDetailCaptureVm();
                var complianceCPCaseDetailsListCaptureVm = new List<ComplianceCPCaseDetailCaptureVm>();
                var complianceCPCaseVisitHeaderCaptureVm = new ComplianceCPCaseVisitHeaderCaptureVm();
                var complianceCPCaseVisitDetailCaptureVm = new ComplianceCPCaseVisitDetailCaptureVm();
                var complianceCPCaseVisitDetailsListCaptureVm = new List<ComplianceCPCaseVisitDetailCaptureVm>();

                if (localCPCaseHeaderData.Any())
                {
                    int i = 1;
                    foreach (var item in localCPCaseHeaderData)
                    {
                        device.DataId = item.ChildProtectionHeaderID;

                        complianceCPCaseHeaderCaptureVm = new ComplianceCPCaseHeaderCaptureVm
                        {
                            ChildProtectionHeaderID = item.ChildProtectionHeaderID,
                            ChildProtectionId = item.ChildProtectionId,
                            CaseManagementReferral = item.CaseManagementReferral,
                            CaseStatusId = item.CaseStatusId,
                            ClientTypeID = item.ClientTypeID,
                            CollectionDate = item.CollectionDate,
                            SocialWorker = item.SocialWorker,
                            FiscalYear = item.FiscalYear,
                            Remarks = item.Remarks,
                            ChildDetailID = item.ChildDetailID,
                            CreatedBy = item.CreatedBy,
                            CreatedOn = item.CreatedOn,

                        };

                        var caseDetails = localCPCaseDetailData.Where(x => x.ChildProtectionHeaderID == item.ChildProtectionHeaderID);
                        foreach(var caseDetail in caseDetails)
                        {
                            complianceCPCaseDetailCaptureVm = new ComplianceCPCaseDetailCaptureVm()
                            {
                                ChildProtectionDetailID = caseDetail.ChildProtectionDetailID,
                                ChildProtectionHeaderID = caseDetail.ChildProtectionHeaderID,
                                RiskId = caseDetail.RiskId,
                                ServiceId = caseDetail.ServiceId,
                                ProviderId = caseDetail.ProviderId
                            };

                            complianceCPCaseDetailsListCaptureVm.Add(complianceCPCaseDetailCaptureVm);
                        }
                        

                        var isnpCaseVm = new ISNPCaseVm()
                        {
                            CPCaseHeaderInfo = complianceCPCaseHeaderCaptureVm,
                            CPCaseDetailInfo = complianceCPCaseDetailsListCaptureVm,
                            CPCaseVisitHeaderInfo = null,
                            CPCaseVisitDetailInfo = null,
                            UserDevice = device
                        };

                        MessageISNPSync = $"Processing Case Ref.#{i} out of {localCPCaseHeaderData.Count} ... ";
                        i++;

                        var feedback = await client.PostChildProtectionHousehold(isnpCaseVm);
                        if (feedback.StatusId == null || feedback.StatusId == -1)
                        {
                            errorMessage = $"{feedback.Description}";
                        }
                        else
                        {
                            item.Status = "SYNCED";
                            App.Database.AddOrUpdate(item);
                        }
                    }
                }

                if(localCaseVisitHeaderData.Any())
                {
                    int i = 1;
                    foreach (var item in localCaseVisitHeaderData)
                    {
                        device.DataId = item.ChildProtectionVisitHeaderId;

                        complianceCPCaseVisitHeaderCaptureVm = new ComplianceCPCaseVisitHeaderCaptureVm()
                        {

                            ChildProtectionVisitHeaderId=item.ChildProtectionVisitHeaderId,
                            ChildProtectionHeaderID = item.ChildProtectionHeaderID,
                            VisitDate = item.VisitDate,
                            FiscalYear = item.FiscalYear,
                            SocialWorker = item.SocialWorker,
                            CreatedBy = item.CreatedBy,
                            CreatedOn = item.CreatedOn
                        };

                        var caseVisitDetails = localCPCaseVisitDetailData.Where(x => x.ChildProtectionVisitHeaderId == item.ChildProtectionVisitHeaderId);
                        foreach (var caseVisitDetail in caseVisitDetails)
                        {
                            complianceCPCaseVisitDetailCaptureVm = new ComplianceCPCaseVisitDetailCaptureVm()
                            {
                                ChildProtectionVisitHeaderId = caseVisitDetail.ChildProtectionVisitHeaderId,
                                ChildProtectionDetailID = caseVisitDetail.ChildProtectionDetailID,
                                ServiceStatusID = caseVisitDetail.IsServiceAccessed,
                                ServiceAccessDate = caseVisitDetail.DateServiceAccessed,
                                ServiceNotAccessedReasonID = caseVisitDetail.ServiceNotAccessedReason
                            };

                            complianceCPCaseVisitDetailsListCaptureVm.Add(complianceCPCaseVisitDetailCaptureVm);
                        }


                        var isnpCaseVm = new ISNPCaseVm()
                        {
                            CPCaseHeaderInfo = null,
                            CPCaseDetailInfo = null,
                            CPCaseVisitHeaderInfo = complianceCPCaseVisitHeaderCaptureVm,
                            CPCaseVisitDetailInfo = complianceCPCaseVisitDetailsListCaptureVm,
                            UserDevice = device
                        };

                        MessageISNPSync = $"Processing FollowUp Ref.#{i} out of {localCaseVisitHeaderData.Count} ... ";
                        i++;
                        var feedback = await client.PostChildProtectionHousehold(isnpCaseVm);
                        if (feedback.StatusId == null || feedback.StatusId == -1)
                        {
                            errorMessage = $"{feedback.Description}";
                        }
                        else
                        {
                            item.Status = "SYNCED";
                            App.Database.AddOrUpdate(item);
                        }
                    }
                }


                if (errorMessage == "")
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Success",
                        Message = "Sync Successful",
                        Cancel = "OK"
                    });
                }
                else
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(
                        MessageKeys.Error,
                        new MessagingServiceAlert
                        {
                            Title = "Error",
                            Message = $"Sync Error \n" + errorMessage,
                            Cancel = "OK"
                        });
                    IsBusy = false;
                }


            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Sync",
                    Message = "The Sync failed. Please try again later.\n\n" + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageSync = string.Empty;
                MessageISNPSync = string.Empty;
                SyncUpMonitoringText = "Upload Data to Server";
                IsBusy = false;
                await Task.FromResult(0);
            }
        }

        private ICommand syncDownISNPCommand;
        public ICommand SyncDownISNPCommand => syncDownISNPCommand ?? (syncDownISNPCommand = new Command(async () => await ExecuteSyncDownISNPAsync()));
        private async Task ExecuteSyncDownISNPAsync()
        {
            AppSettings = GetAppSettings();

            if (IsBusy)
            {
                return;
            }

            ISNPResponse isnpResponse = null;
            MessageSync = "Pulling data from the server... ...";
            SyncDownComplianceText = "Downloading Data . . .";


            try
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Sync Failed",
                        Message = "Uh Oh, It looks like you have gone offline. \n" +
                                  "Please check your internet connection and try again.",
                        Cancel = "OK"
                    });
                    return;
                }
                IsBusy = true;

                if (AppSettings["Location"] != null && AppSettings["ReportingPeriod"] != null)
                {

                    QueryParameter queryParameter = new QueryParameter
                    {
                        KebeleID = AppSettings["Location"],
                        ReportingPeriodID = AppSettings["ReportingPeriod"]
                    };

                    isnpResponse = await client.GetHouseholdsReadyForChildProtection(queryParameter);

                    if (isnpResponse.CPHouseholds != null)
                    {
                        foreach (var item in isnpResponse.CPHouseholds)
                        {
                            App.Database.AddOrUpdate(item);
                        }

                        foreach (var item in isnpResponse.CPHouseholdMembers)
                        {
                            App.Database.AddOrUpdate(item);
                        }
                    }

                    if (isnpResponse.CPCases != null)
                    {
                        foreach (var item in isnpResponse.CPCases)
                        {
                            var risk = (SystemCodeDetail)App.Database.GetTableRow("SystemCodeDetail", "Value", (item.RiskId).ToString(), "SystemCode", "RiskTypeOptions");
                            var service = (SystemCodeDetail)App.Database.GetTableRow("SystemCodeDetail", "Value", (item.ServiceId).ToString(), "SystemCode", "ChildProtectionServiceOptions");
                            var provider = (SystemCodeDetail)App.Database.GetTableRow("SystemCodeDetail", "Value", (item.ProviderId).ToString(), "SystemCode", "ChildProtectionServiceProviderOptions");

                            item.RiskName = risk.Name;
                            item.ServiceName = service.Name;
                            item.ProviderName = provider.Name;

                            App.Database.AddOrUpdate(item);
                        }
                    }

                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Success",
                        Message = "Data downloaded successfully",
                        Cancel = "OK"
                    });
                }
                else
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Unable to Sync",
                        Message = "Please Configure Kebele and/or Reporting Period",
                        Cancel = "OK"
                    });
                }


            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Sync",
                    Message = "The Sync failed. Please try again later.\n\n" + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageSync = string.Empty;
                SyncDownComplianceText = "Download Data from Server";
                IsBusy = false;
                await Task.FromResult(0);
            }
        }

        #endregion Sync
    }
}